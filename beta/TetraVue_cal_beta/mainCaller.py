from alpha_calibration_functions import *
def main():

    # Welcome to the one button tetravue beta camera calibration processing script
    # Assumes data acquired and properly placed - see the accompanying procedure sheet
    # 1. Registration images - reflect.tiff, transmit.tiff
    # 2. Lens cal images
    # 3. Wall images
    # 4. Ramp images
    # SET THE BELOW FILE LOCATIONS & TYPE OF BINNING DESIRED


    main_loc = 'C:\Users\\3dlabrat\\Dropbox\\SHARED FILES\\calibration_testing' # location of the camera data <------------------------------
    CMU_files_loc = 'C:\Users\\3dlabrat\PycharmProjects\Calibration Pipeline (TV_BETA)\\beta\lenscalibration'  # location of CMU scripts <-------------------------------
    binsTYPE = np.asarray([3.0]) # <---------------------- floats, creates LUTs for each binning amount 1/1.0,1/3.0, etc



    #-----------------------------------------------------------------------------------------------------------------------
    #-----------------------------------------------------------------------------------------------------------------------
    #-----------------------------------------------------------------------------------------------------------------------


    main_loc = main_loc.replace('\\', '//')
    CMU_files_loc = CMU_files_loc.replace('//', '\\')

    ramp_loc = main_loc + '/ramp1'  # location of ramp files, the folder containing each ramp
    wall_loc = main_loc + '/wall1/rw0'   # location of wall files, the folder containing rw0
    ramp_folders = [f for f in os.listdir(ramp_loc) if not f.startswith('.') if f.startswith('rw') if not f.endswith('.png')]
    BG_loc = ramp_loc + '/BG'
    lensCal_loc = main_loc + '/lensCal1' # location of the lens cal data
    lensCalOut_loc = lensCal_loc + '_out' # output for CMU
    wallRange_loc = lensCalOut_loc + '/wall' # location of the wall range data in the CMU
    cal_out_loc =  main_loc + '/calibration_out' # holds all the files needed to run a pipeline
    register_loc = main_loc + '/registration' # lcoation of the registration matrix


    print 'MAIN DIRECTORY: ', main_loc
    print 'calibration data source directory: ', main_loc
    print 'ramps: ', ramp_folders
    print 'creating LUTs for ' + str(len(ramp_folders)) + ' ramps and ' + str(binsTYPE.size) + ' binning modes'

    for binning in binsTYPE:
        cal_out_loc = main_loc + '/calibration_out'+'_bin(X'+str(binning)+')'
        lensCalOut_loc = lensCal_loc + '_out' +'(binX'+str(binning)+')' # output for CMU

        mainprog(binning, cal_out_loc,lensCalOut_loc,CMU_files_loc,main_loc,ramp_loc,ramp_folders,BG_loc,lensCal_loc,wallRange_loc,register_loc,wall_loc)

if __name__ == "__main__":
    main()