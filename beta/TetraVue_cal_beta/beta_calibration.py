from os.path import join as pjoin
import os
import numpy as np
from beta.TetraVue_cal_beta.alpha_calibration_functions import *
import sys


def create_dir_safe(folder):
    if not os.path.exists(folder):
        os.makedirs(folder)
        print('creating directory: ' + folder)
    return


def main():
    #  __   ___ ___          __               __   __       ___    __
    # |__) |__   |   /\     /  `  /\  |    | |__) |__)  /\   |  | /  \ |\ |
    # |__) |___  |  /~~\    \__, /~~\ |___ | |__) |  \ /~~\  |  | \__/ | \| VERSION 1.0
    # LV
    version = 1.0

    # Welcome to the one button tetravue beta camera calibration processing script
    # Assumes data acquired and properly placed - see the accompanying procedure sheet
    # 1. Registration images - reflect.tiff, transmit.tiff
    # 2. Lens cal images
    # 3. Wall images
    # 4. Ramp images

    # given a SN

    # go into confluence to find traveler for it

    # extract SNs (lens, sensors, camera)

    # look for folder with camera SN

    # confirm sensors and lens calibrated (look for sensor cal and place LUT in the right place)

    # check if all ramp and lens data exists

    # put in the right folder

    # run calibration for each camera for each ramp with proper settings, placing LUTs in one place,
    # and cal summaries in another

    # update the traveler with cal location, camera data folder, cal date, calibration date

    # -----------------------------------------------------------------------------------------------------------------------
    # -----------------------------------------------------------------------------------------------------------------------
    # -----------------------------------------------------------------------------------------------------------------------

    # main_loc = main_loc.replace('\\', '//')
    camera_SN = 'Camera 1'  # <----------------------
    cal_PN = '001'  # <----------------------
    c_single = 'RIGHT'
    binsTYPE = np.asarray(
        [3.0])  # <---------------------- floats, creates LUTs for each binning amount 1/1.0,1/3.0, etc

    production_dir = '/Users/hanscastorp/Downloads/crap'  # <---------------------- directory
    CMU_files_loc = pjoin(os.getcwd(),
                          'lenscalibration')  # <---------------------- CMU scripts location, usually within current
    camera_dir = pjoin(production_dir, camera_SN)
    useful_shit = np.load(pjoin(camera_dir, 'system_details.npy'))
    print(useful_shit)
    main_loc = pjoin(camera_dir, 'cal 3D ARCHIVE', c_single, cal_PN)

    ramp_loc = pjoin(main_loc, 'ramp1')  # location of ramp files, the folder containing each ramp
    wall_loc = pjoin(main_loc, 'wall1/rw0')  # location of wall files, the folder containing rw0
    ramp_folders = [f for f in os.listdir(ramp_loc) if not f.startswith('.') if f.startswith('rw') if
                    not f.endswith('.png')]
    BG_loc = pjoin(ramp_loc, 'BG')
    lensCal_loc = pjoin(main_loc, 'lensCal1')  # location of the lens cal data
    lensCalOut_loc = pjoin(lensCal_loc, '_out')  # output for CMU
    wallRange_loc = pjoin(lensCalOut_loc, 'wall')  # location of the wall range data in the CMU
    register_loc = pjoin(main_loc, 'registration')  # lcoation of the registration matrix
    F1_loc = pjoin(camera_dir, 'cal 3D CURRENT LUTs', c_single, 'function1_polyvals_wno_bno.lut')


    # holds all the files needed to run, and is just the latest calibration with F1 files as well
    cal_out_loc_current = pjoin(camera_dir, 'cal 3D CURRENT', c_single)

    print('MAIN DIRECTORY: ', main_loc)
    print('calibration data source directory: ', main_loc)
    print('ramps: ', ramp_folders)
    print('creating LUTs for ' + str(len(ramp_folders)) + ' ramps and ' + str(binsTYPE.size) + ' binning modes')

    # now, we will consider running calibration on particular cal directory, and first check that requisite files exist
    if not os.path.exists(BG_loc) or not os.path.exists(ramp_loc + '/' + ramp_folders[0]):
        print('ERROR: missing ramp data in ' + BG_loc)
        sys.exit()
    if not os.path.exists(wall_loc):
        print('ERROR: missing ramp data in ' + wall_loc)
        sys.exit()
    if not os.path.exists(lensCal_loc):
        print('ERROR: missing lens calibration data in ' + lensCal_loc)
        sys.exit()
    else:
        print('Lens Cal data found!')

    if not os.path.isfile(F1_loc):
        print('ERROR: missing F1 LUT in ' + F1_loc)
        sys.exit()
    else:
        print('F1 LUT found!')
    # create cal out folder for pipeline, if one already exists --> create another


    print('Data folders contain requisite directories.')

    if 1 == 1:

        for binning in binsTYPE:
            cal_out_loc = pjoin(camera_dir, 'cal 3D ARCHIVE', c_single, cal_PN,
                                'calibration_out'+ '_bin(X' + str(binning) + ')')  # holds all the files for a given cal
            lensCalOut_loc = lensCal_loc + '_out' + '(binX' + str(binning) + ')'  # output for CMU

            if not os.path.exists(cal_out_loc):
                create_dir_safe(cal_out_loc)
                print('creating calibration output directory for pipeline: ', cal_out_loc)
            else:
                num = 1
                cal_out_loc0 = cal_out_loc
                while os.path.exists(cal_out_loc):
                    cal_out_loc = cal_out_loc0 + str(num)
                    num += 1
                create_dir_safe(cal_out_loc)
                print('WARNING: calibration output directory exists, creating new folder', cal_out_loc)

            mainprog(binning, cal_out_loc, lensCalOut_loc, CMU_files_loc, main_loc, ramp_loc, ramp_folders, BG_loc,
                     lensCal_loc, wallRange_loc, register_loc, wall_loc)

if __name__ == "__main__":
    main()
