import pdfkit
from lxml import etree
from bs4 import BeautifulSoup
import pandas as pd
import shutil
import os
import pickle
import re

import ast
from os.path import join as pjoin
import numpy as np


def create_dir_safe(folder):
    if not os.path.exists(folder):
        os.makedirs(folder)
        print('creating directory: ' + folder)
    return


# pages to care about 'Traveler - Camera 1','Traveler - Camera 2','Camera Travelers','testspage2!',

def extract_table(table, cols=None):  # from a html soup - e.g. soup.find_all('table')
    rows = table.find_all('tr')
    if cols == None:
        cols = int(str(rows[0]).split('colspan=\"')[1].split("\" data")[0])

    new_table = pd.DataFrame(columns=range(0, cols), index=range(0, len(rows)))
    row_marker = 0
    for row in table.find_all('tr'):
        column_marker = 0
        columns = row.find_all('td')
        for column in columns:
            new_table.iat[row_marker, column_marker] = column.get_text()
            column_marker += 1
        row_marker += 1
    return new_table


def get_info(pid, expand, usr='lorenzo.venneri@tetravue.com', pwd='Theseus1824',
             site='https://tetravue.atlassian.net/wiki/rest/api/content/'):
    c_command = 'curl -u' + usr + ':' + pwd + ' ' \
                + site + pid + '?expand=' + expand + ' | python ' \
                                                     '-mjson.tool'
    a = os.popen(c_command).read()
    return json.loads(a)


def update_page(pid, new_title, space, new_body, version, usr='lorenzo.venneri@tetravue.com', pwd='Theseus1824',
                site='https://tetravue.atlassian.net/wiki/rest/api/content/'):
    c_command = 'curl -u ' + usr + ':' + pwd + ' -X PUT -H \'Content-Type: application/json\' -d\'{\"id\":\"' + pid + \
                '\",\"type\":"page",\"title\":\"' \
                + new_title + '","space\":{\"key\":\"' + space + '\"},\"body\":{\"storage\":{\"value\":\"' + new_body \
                + '\",\"representation\":\"storage\"}}, "version":{"number":' + str(
        version) + '}}\' ' \
                + site + pid + ' | python -mjson.tool'
    return os.popen(c_command).read()


def create_page(new_title, space, new_body, usr='lorenzo.venneri@tetravue.com', pwd='Theseus1824',
                site='https://tetravue.atlassian.net/wiki/rest/api/content/'):
    # # create new page
    c_command = 'curl -u ' + usr + ':' + pwd + ' -X POST -H \'Content-Type: application/json\' -d\'{\"type\":"page",' \
                                               '\"title":"' \
                + new_title + '","space\":{\"key\":\"' + space + '\"},\"body\":{\"storage\":{\"value\":\"' + new_body \
                + '\",\"representation\":\"storage\"}}}\' ' \
                + site + ' | python -mjson.tool'
    return os.popen(c_command).read()


def upload_ATCH(pid, file, file_comment, usr='lorenzo.venneri@tetravue.com', pwd='Theseus1824',
                site='https://tetravue.atlassian.net/wiki/rest/api/content/'):
    #     curl -v -S -u admin:admin -X POST -H "X-Atlassian-Token: no-check" -F "file=@myfile.txt" -F
    # "comment=this is my file" "http://localhost:8080/confluence/rest/api/content/3604482/child/attachment"
    # | python -mjson.tool
    c_command = 'curl -v -S -u ' + usr + ':' + pwd + ' -X POST -H \"X-Atlassian-Token: no-check\"  -F "' + file + '" -d\'{\"type\":"page",' \
                                                                                                                  '-F "' + file_comment + '" "' + site + pid + '" | python -mjson.tool'
    return os.popen(c_command).read()


def download_ATCH(pid, usr='lorenzo.venneri@tetravue.com', pwd='Theseus1824',
                  site='https://tetravue.atlassian.net/wiki/rest/api/content/'):
    # curl -u admin:admin "http://localhost:8080/confluence/rest/api/content/1998856/child/attachment" | python -m
    # json.tool
    c_command = 'curl -u ' + usr + ':' + pwd + ' "' + site + pid + '/child/attachment"' + ' | python -mjson.tool'
    result = os.popen(c_command).read()

    if 1 == 0:
        url = result['results']["_links"]["self"]
        r = requests.get(url, allow_redirects=True)
        open('thing.ico', 'wb').write(r.content)
    return


import requests, json


def printResponse(r):
    print('{} {}\n'.format(json.dumps(r.json(), sort_keys=True, indent=4, separators=(',', ': ')), r))


def comment(pid, comment='<p>trd</p>', usr='lorenzo.venneri@tetravue.com', pwd='Theseus1824',
            site='https://tetravue.atlassian.net/wiki/rest/api/content/'):
    r = requests.get(site,
                     params={'id': pid},
                     auth=(usr, pwd))
    printResponse(r)
    parentPage = r.json()['results'][0]
    pageData = {'type': 'comment', 'container': parentPage,
                'body': {'storage': {'value': comment, 'representation': 'storage'}}}
    r = requests.post(site,
                      data=json.dumps(pageData),
                      auth=(usr, pwd),
                      headers=({'Content-Type': 'application/json'}))
    printResponse(r)


def main(camera_page_id='293240840',
         production_dir='/Users/hanscastorp/Downloads/crap',
         sensor_cal_dir='/Users/hanscastorp/Dropbox/SHARED FILES/PTCs/PTC Beta Sensor Characterization Done'
         ):
    '''

    :param camera_page_id: confluence page id that holds travelers
    :param production_dir: server location of production directory
    :return: updates things, and creates folders if they don't exist
    '''

    # camer_dir directory is inside production_dir, production_dir contains all cameras, camer_dir is a camera

    # make camera simulator folder
    camera_dir = pjoin(production_dir, 'Camera SIMULATOR')
    current_cal_dir = pjoin(camera_dir, 'cal 3D CURRENT LUTs')
    create_dir_safe(camera_dir)
    create_dir_safe(current_cal_dir)
    create_dir_safe(pjoin(camera_dir, '3D data'))
    create_dir_safe(pjoin(camera_dir, 'intnernal testing'))

    camera_dir = pjoin(production_dir, 'Camera CHECKLUTS')
    current_cal_dir = pjoin(camera_dir, 'cal 3D CURRENT LUTs')
    create_dir_safe(camera_dir)
    create_dir_safe(current_cal_dir)
    create_dir_safe(pjoin(camera_dir, '3D data'))
    create_dir_safe(pjoin(camera_dir, 'intnernal testing'))

    # find all cameras in the Camera Travelers page
    result = get_info(camera_page_id, 'children.page,version.number,id,title,body.storage,space')

    for camera in result['children']['page']['results'][0:1]:  # BADSHIT 0:1
        temp_id = '293830673'  # get_info(camera['id'] # BADSHIT
        result = get_info(temp_id, 'children.page,version.number,id,title,body.storage,space')

        # print(result['children']['page']['results'])
        # print(result['version']['number'])
        # print(result['id'])
        # print(result['title'])
        # print(result['space']['key'])
        # print(result['body']['storage']['value'])

        # print('__________')
        html_bod = result['body']['storage']['value']
        soup = BeautifulSoup(html_bod)
        # print(soup.prettify())
        # print(soup.get_text())

        table_camSN = extract_table(soup.find_all('table')[0], 2)
        table_calibration = extract_table(soup.find_all('table')[1])
        table_RASS = extract_table(soup.find_all('table')[3])
        table_LASS = extract_table(soup.find_all('table')[4])
        useful_shit = {}
        useful_shit['calibration_dir'] = table_calibration[table_calibration[:][0] == 'Calibration'][8].item()
        useful_shit['calibration_PN'] = table_calibration[table_calibration[:][0] == 'Calibration'][1].item()
        useful_shit['firmware_version'] = table_calibration[table_calibration[:][0] == 'Firmware'][1].item()
        useful_shit['cam_PN'] = table_camSN[table_camSN[:][0] == 'TV Part Number:'][1].item()
        useful_shit['cam_SN'] = table_camSN[table_camSN[:][0] == 'Serial Number:'][1].item()
        useful_shit['RIGHT_sensor_SN'] = table_RASS[table_RASS[:][0] == 'CMOS Sensor, Onyx 1.3M'][6].item()
        useful_shit['LEFT_sensor_SN'] = table_LASS[table_LASS[:][0] == 'CMOS Sensor, Onyx 1.3M'][6].item()
        useful_shit['RIGHT_lens_SN'] = table_RASS[table_RASS[:][0] == 'Assy, RX Lens'][6].item()
        useful_shit['LEFT_lens_SN'] = table_LASS[table_LASS[:][0] == 'Assy, RX Lens'][6].item()

        # running this for all cameras, beforehand, to make right folders available
        camera_dir = pjoin(production_dir, useful_shit['cam_SN'])
        current_cal_dir = pjoin(camera_dir, 'cal 3D CURRENT LUTs')
        create_dir_safe(camera_dir)
        create_dir_safe(pjoin(camera_dir, 'cal 3D ARCHIVE'))
        create_dir_safe(current_cal_dir)
        create_dir_safe(pjoin(camera_dir, '3D data'))
        create_dir_safe(pjoin(camera_dir, 'intnernal testing'))

        duo_cams = ['RIGHT', 'LEFT']

        soup = BeautifulSoup(html_bod, "html.parser")
        sensors = soup.find_all('td', string=('CMOS Sensor, Onyx 1.3M'))

        for i, c_single in enumerate(duo_cams):
            create_dir_safe(pjoin(camera_dir, 'cal 3D ARCHIVE', c_single, '001'))
            create_dir_safe(pjoin(camera_dir, 'cal 3D ARCHIVE', c_single, '002'))
            create_dir_safe(pjoin(current_cal_dir, c_single))
            create_dir_safe(pjoin(current_cal_dir, c_single))

            c_dirs = [f for f in os.listdir(pjoin(camera_dir, 'cal 3D ARCHIVE', c_single)) if not f.startswith('.')]
            empty_dir = 0

            for c_dir in c_dirs:
                if len(os.listdir(pjoin(camera_dir, 'cal 3D ARCHIVE', c_single, c_dir))) > 2:
                    empty_dir += 1
                # always have two empty directries available for someone to put in new calibration data
            for i in range(empty_dir):
                name = str((int(c_dir) + i + 1)).zfill(3)
                create_dir_safe(pjoin(camera_dir, 'cal 3D ARCHIVE', c_single, name))

            if 1 == 1:
                F1_official_loc = pjoin(current_cal_dir, c_single, 'function1_polyvals_wno_bno.lut')
                BMP_official_loc = pjoin(current_cal_dir, c_single, 'badpixelmap_wno_bno.lut')
                if not (os.path.isfile(F1_official_loc)):  # check if F1 has already been uploaded, otherwise grab it
                    print('No Function 1 LUTs, searching and importing for ' + 'sn ' + useful_shit[
                        c_single + '_sensor_SN'])
                    F1_my_loc = pjoin(sensor_cal_dir, 'sn ' + useful_shit[c_single + '_sensor_SN'], 'PTC_OUTPUT',
                                      'function1_polyvals_wno_bno.lut')
                    # BPM_my_loc = pjoin(sensor_cal_dir,'sn '+useful_shit[c_single+'_sensor_SN'],'PTC_OUTPUT',
                    # 'badpixelmap_wno_bno.lut')

                    shutil.copyfile(F1_my_loc, F1_official_loc)
                    # shutil.copyfile(BPM_my_loc,BMP_official_loc)
                else:
                    print('F1 already in place - for ' + 'sn ' + useful_shit[c_single + '_sensor_SN'])

                for f in range(8):
                    sensors[i] = sensors[i].find_next('td')
                    print(sensors[i])
                # update the traveler with file locations
                sensors[i].clear('p')
                snsr_data_loc = pjoin('/Volumes/ENGINEERING/PROJECTS/BETA/TestData/PTC Data/PTC Beta Sensor Characterization',
                    'sn ' + useful_shit[c_single + '_sensor_SN']) # location on server of the sensor data

                sensors[i].insert(1, BeautifulSoup("<p>" + snsr_data_loc + "</p>", "html.parser"))
                print(sensors[i])

        # edit the calibration directory in table as well
        cal_row = soup.find_all('td', string=('Calibration'))[0]
        cal_row_PN = cal_row.find_next('td').find_next('td')
        cal_row_BY = cal_row_PN.find_next('td').find_next('td')
        cal_row_DATE = cal_row_BY.find_next('td').find_next('td')
        cal_row_TRAV = cal_row_DATE.find_next('td').find_next('td')
        cal_row_PN.clear('p')
        cal_row_BY.clear('p')
        cal_row_DATE.clear('p')
        cal_row_TRAV.clear('p')
        cal_row_PN.insert(1, BeautifulSoup("<p> Not calibrated. </p>", "html.parser"))
        cal_row_BY.insert(1, BeautifulSoup("<p> - </p>", "html.parser"))
        cal_row_DATE.insert(1, BeautifulSoup("<p> - </p>", "html.parser"))
        cal_row_TRAV.insert(1, BeautifulSoup("<p> " + current_cal_dir + " </p>", "html.parser"))

        html_bod_new = str(soup).replace('\\', '\\\\').replace('"', '\\"')

        update_page(pid=result['id'], new_title=result['title'], space=result['space']['key'],
                    new_body=html_bod_new,
                    version=result['version']['number'] + 1)

        pdfkit.from_string(html_bod, pjoin(camera_dir,
                                           'current_traveler.pdf'))  # save html table as pdf # pdfkit.from_url also
        np.save(pjoin(camera_dir, 'system_details.npy'), useful_shit)


if __name__ == "__main__":
    main()
