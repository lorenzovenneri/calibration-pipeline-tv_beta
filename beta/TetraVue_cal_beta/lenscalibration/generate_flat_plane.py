#!/usr/bin/python
#
# CHANGES:
# * if a single argument is used with -f, it is used as a base for pattern '\*int.dat'

import os,sys
import glob
import cv2
from camera_model import *
from multiprocessing import Pool

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D as ax3d

from misc_utils.mutils import *
from opencv_calibrate import *

def hnorm(x):
  y=x.copy()
  y[:-1,:]/=np.tile(y[-1,:],(y.shape[0]-1,1))
  return y

def homog(x):
  return np.vstack((x,np.ones((1,x.shape[1]))))

def applyEuclidean(R,T,X):
  return np.dot(R,X)+np.tile(T,(1,X.shape[1]))
  
  
class DetectFlatPlane:
  def __init__(self,campath,verbose=False):
    self.cam=PinHoleCamera.load(campath)
    self.psize=readCSV(os.path.join(campath,'psize.csv'),int)
    self.square=np.loadtxt(os.path.join(campath,'square.dat'))
    self.verbose=verbose

  def genObjPoints(self):
    op = np.zeros( (np.prod(self.psize), 3), np.float32 )
    op[:,:2] = np.indices(self.psize).T.reshape(-1, 2)
    op *= self.square
    return op

  def saveAndShow(self,opath=None,fn=None):
    if opath!=None:
      plt.savefig(os.path.join(opath,fn))
    #end
    if self.verbose:
      mng = plt.get_current_fig_manager()
      mng.resize(*mng.window.maxsize())      
      plt.show()
    #end

  def showMap(self,img,xim,title,fn,opath=None):
    plt.clf()
    plt.subplot(121)
    plt.imshow(img)
    plt.subplot(122)
    plt.imshow(xim,cmap='hsv')
    plt.tight_layout()
    plt.colorbar(shrink=0.6)
    plt.title(title)
    self.saveAndShow(opath,fn)


  def detect(self,im,opath=None):

    # rectify the images
    #im2=self.cam.undistort(im)

    # detect the corners
    (rv,cnr)=cv2.findChessboardCorners(im,self.psize)
    if not rv:
      print 'Could not find corners'
      return False
    #end

    # subpix refinement
    term = ( cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_COUNT, 30, 0.1 )
    cv2.cornerSubPix(im, cnr, (5, 5), (-1, -1), term)

    # save off for debugging
    imc=cv2.cvtColor(im,cv2.COLOR_GRAY2BGR)
    cv2.drawChessboardCorners(imc,self.psize,cnr,rv)

    # just use PnP for now. Easier than pulling values from H
    Xp=self.genObjPoints()
    xc=cnr.reshape(-1,2)
    
    # find the R, T for the target. Could extract
    rv,rvec,tvec=cv2.solvePnP(Xp,xc,self.cam.K,self.cam.distort,flags=cv2.CV_ITERATIVE)    
    if not rv:
      print 'Could not solve for pose'
      return False
    #end

    # get the rotation
    R,J=cv2.Rodrigues(rvec)

    # print 'R=',R
    # print 'T',tvec
    # print '|T|',np.linalg.norm(tvec)

    # form the homography from checkerboard to normalized image
    H=np.hstack((R[:,:2],tvec))
    Hinv=np.linalg.inv(H)

    # now build up an image of pixel locations and project to the wall
    vv,uu=np.mgrid[0:im.shape[0],0:im.shape[1]]
    uv=np.vstack((uu.ravel(),vv.ravel()))
    uv=uv.T.reshape((1,uu.size,2)).astype(np.float32)
    xx=self.cam.undistortPoints(uv).squeeze().T

    # Now apply inverse homography to get to the chessboard frame
    YYp=hnorm(np.dot(Hinv,homog(xx)))

    # test
    # print 'max |YYp[2]|',np.abs(YYp[2,:]).max()

    # clear out the Z-coordinate to put it on the chessboard surface
    YYp[2,:]=0

    # now apply the Rigid transform to get back to the camera frame
    YYc=np.dot(R,YYp)+np.tile(tvec,(1,YYp.shape[1]))

    # now calculate the range and projected range
    Xim=YYc[0,:].reshape(im.shape)
    Yim=YYc[1,:].reshape(im.shape)
    Zim=YYc[2,:].reshape(im.shape)
    Rim=np.sqrt((YYc**2).sum(axis=0)).reshape(im.shape)

    # generate the angle images
    #xz_angle=np.arctan(YYc[0,:]/YYc[2,:]).reshape(im.shape)

    n=np.sqrt(YYc[0,:]**2+YYc[2,:]**2)
    #elev_angle=np.arctan(YYc[1,:]/n).reshape(im.shape)

    # bodo's angles
    #a_angle=np.arctan(YYc[0,:]/YYc[2,:]).reshape(im.shape)
    #b_angle=np.arctan(YYc[1,:]/YYc[2,:]).reshape(im.shape)

    # save output
    if opath!=None:
      testMakeDir(opath)

      # print 'Saving to',opath

      with open(os.path.join(opath,'summary.txt'),'w') as f:
        f.write('Flat Plane information\n')
        f.write('Square size=%s pattern=%s\n'%(self.square,self.psize))
        f.write('K=%s\n'%(self.cam.K))
        f.write('distort=%s\n'%(self.cam.distort))
        f.close()
      #end

      np.savetxt(os.path.join(opath,'x_image.dat.gz'),Xim,delimiter=" ")
      np.savetxt(os.path.join(opath,'y_image.dat.gz'),Yim,delimiter=" ")
      np.savetxt(os.path.join(opath,'z_image.dat.gz'),Zim,delimiter=" ")
      np.savetxt(os.path.join(opath,'range_image.dat.gz'),Rim,delimiter=" ")

      #np.savetxt(os.path.join(opath,'xz_angle_image.dat.gz'),xz_angle,delimiter=" ")
      #np.savetxt(os.path.join(opath,'elev_angle_image.dat.gz'),elev_angle,delimiter=" ")

      #np.savetxt(os.path.join(opath,'a_angle_image.dat.gz'),a_angle,delimiter=" ")
      #np.savetxt(os.path.join(opath,'b_angle_image.dat.gz'),b_angle,delimiter=" ")

      cv2.imwrite(os.path.join(opath,'corners.png'),imc)
    #end

    self.showMap(imc,Rim,'Range image','range_image_plot.png',opath)
    self.showMap(imc,Xim,'X image','x_image_plot.png',opath)
    self.showMap(imc,Yim,'Y image','y_image_plot.png',opath)
    self.showMap(imc,Zim,'Z image','z_image_plot.png',opath)

    #self.showMap(imc,np.rad2deg(xz_angle),'XZ Angle (deg)','xz_angle_image_plot.png',opath)
    #self.showMap(imc,np.rad2deg(elev_angle),'Elev Angle (deg)','elev_angle_image_plot.png',opath)
    #self.showMap(imc,np.rad2deg(a_angle),'A Angle (deg)','a_angle_image_plot.png',opath)
    #self.showMap(imc,np.rad2deg(b_angle),'B Angle (deg)','b_angle_image_plot.png',opath)
            
    # get undistorted corner points for plotting
    xc=self.cam.undistortPoints(cnr).squeeze().T

    # project our target into the camera frame
    Xp=Xp.T
    Xc=np.dot(R,Xp)+np.tile(tvec,(1,Xp.shape[1]))
      
    fig = plt.figure(1)
    fig.clf()
    ax = fig.add_subplot(111, projection='3d')
    ax.plot(YYc[0,::20],YYc[1,::20],YYc[2,::20],'b.',markersize=1)
    plt.hold(True)
    ax.plot(Xc[0,:],Xc[1,:],Xc[2,:],'go')
    ax.plot([0],[0],[0],'r.')
    ax.set_xlabel('X')
    ax.set_ylabel('Y')
    plt.title('Camera Frame 3D')
    self.saveAndShow(opath,'camera_frame_plot.png')



if __name__=='__main__':
  import argparse
  from datafile import *

  p = argparse.ArgumentParser(description='Load raw file and display.')
  p.add_argument('-f',dest='files',nargs='+',required=True,metavar='FILES',
                 help='Files to process')
  #p.add_argument('-s',dest='square',default=105.0,type=float,
  #               help='Square size [def=105.0]')
  #p.add_argument('-p',dest='psize',default=(5,3),nargs=2,type=int,
  #               help='pattern size [def=5 3]')
  p.add_argument('-c',metavar='PATH',dest='campath',required=True,
                 help='Camera path')
  #p.add_argument('-o',metavar='PATH',dest='output',default=None,
  #               help='Output path')
  #p.add_argument('-d',dest='debug',default=False,action='store_true',
  #               help='Debug')
  p.add_argument('-v',dest='verbose',default=False,action='store_true',
                 help='Verbose')

  args = p.parse_args()

  dfp=DetectFlatPlane(args.campath,verbose=args.verbose)

  df=DataFile()

  # Windows doesn't do nice pattern matching like Linux, so do it here
  if len(args.files) == 1:
    files = glob.glob(args.files[0] + "\\*int.dat")
  else:
    files = args.files

  for f in files: # TODO parallelize this
    df.clear()
    df.loadIntensity(f)

    fbase=os.path.splitext(os.path.basename(f))[0]

    opath=os.path.join(args.campath,'wall',fbase)

    im=df.scaleTo8bit()

    # detect it...
    # print "Running wall for ",fbase,"=>",opath
    dfp.detect(im,opath)
  #end
  

  


