#!/usr/bin/python

import os,sys
import numpy as np


from ply_io import *
from camera_model import *
from misc_utils import *
from datafile import *
from range_filter import *
from range_to_ply import *

# Pointless, but useless to document the use of cumsum for 
# integral image of arbitrary array.
def integral(array):
    return array.cumsum(0).cumsum(1)
  
def average(array, w):
    i = integral(array)

    size = np.shape(array)
    # convert to array because arrays are mutable, but tuples are not.
    paddedsize = np.array(size)
    paddedsize[0] += w
    paddedsize[1] += w

    print "Shape pa: " , paddedsize
    pa = np.zeros(paddedsize)
    pa[:-w,:-w] = i
    pww = pa[w:,w:]
    p0w = pa[:-w,w:]
    pw0 = pa[w:,:-w]
    p00 = pa[:-w,:-w]
    ave = pww - p0w - pw0 + p00
    ave[-w:,:] = 0.0
    ave[:,-w:] = 0.0
    ave /= (w*w)
    return ave

class Normals:
    def __init__(self,cam):
        self.cam = cam
        self.num = cam.imsize[0] * cam.imsize[1]

        # Get the rays for each pixel.  Rays are not unit length.  
        # They are unit z-value.
        self.rays = self.cam.rays
        if False:
            # Normalize them.
            n = np.sqrt((self.rays**2).sum(axis=1))
            self.rays[:,0]/=n
            self.rays[:,1]/=n
            self.rays[:,2]/=n
        #end
        print self.rays
        
        # Precompute hatR, cosPhi, ones
        self.hatR = self.computeHatR(self.rays)
        self.cosPhiInv = (1.0/self.computeCosPhi(self.rays))
        self.one = np.ones(self.num).reshape(-1,1)
    #end

    # rays is array of 3d vectors
    def computeHatR(self, rays):
        d = np.sqrt((rays**2).sum(axis=1))
        x = rays[:,0]
        y = rays[:,1]
        z = rays[:,2]

        # This seems convoluted, but I can't think of a better way.
        costhetamask = ...

        # Compute elements of rotation matrix w/o using trig functions
        cosphi = (y / d).reshape(-1,1)
        sinphi = np.sqrt(1 - cosphi**2)
        tantheta = (x / z)
        denom = np.sqrt(1 + tantheta**2)
        sintheta = (tantheta / denom).reshape(-1,1)
        costheta = 1 / denom

        r1 = np.tile(np.array([[1,0,0],[0,1,0],[0,0,0]]), (self.num,1,1,1))
        

        zxy = np.array([[0, 1, 0], [0, 0, 1], [1, 0, 0]])
        # Compute cos(theta), sin(theta), cos(phi), sin(phi)
        costheta = 
        # Construct two rotation matrices.
        # multiply
        # hatR = np.dot(zxy, np.dot(R1, R2))

        hatR = np.tile(np.eye(3), (self.num,1,1,1))
        print "hatR shape = " , np.shape(hatR)
        return hatR
    #end

    def computeCosPhi(self, rays):
        # TBD
        return np.ones(self.num).reshape(-1,1)
    #end


    # Compute derivative from range image.  Range image must already have 
    # been smoothed/blurred appropriately.  No smoothing is done in this 
    # method.
    # 
    # With the placeholders above, the result should be vectors [1,1/r,1/r]

    def getBySRI(self, range):
        # Ick, make 0's into 1.  Build this into the mask and respect the 
        # mask here.
        zeros = range==0.0
        range[zeros] = 1

        # Compute dr/dtheta, dr/dphi using sobel or prewitt operator
        # We use Sobel because it exists, but does the math in the 
        # paper rely on the Prewitt operator?
        drdt = cv2.Sobel(range, -1, 1, 0).reshape((-1,1))
        drdp = cv2.Sobel(range, -1, 0, 1).reshape((-1,1))
        
        # Build vectors with components
        #   1, (1/(r cos(phi)) dr/dtheta, 1/r dr/dphi
        rinv = (1.0/range).reshape((-1,1))

        print "rinv shape = ", np.shape(rinv)
        print "cosPhiInv shape = ", np.shape(self.cosPhiInv)
        print "hatR shape = " , np.shape(self.hatR)
        print "drdt shape = " , np.shape(drdt)
        print "one shape = " , np.shape(self.one)

        #comp1 = rinv * self.cosPhiInv * drdt
        #comp2 = rinv * drdp
        comp1 = rinv * self.cosPhiInv
        comp2 = rinv
        
        vect = np.hstack((self.one, comp1, comp2))
        normals = np.array(map(np.dot, self.hatR, vect))

        print "normals shape = " , np.shape(normals)
        return normals
    # end

    # Compute matrix of outer products from image rays.
    #
    # XXX: This has issues.  The np.linalg.inv is raising an exception for a
    # singular matrix.
    def computeOuterMatrix(self, rays):
        print "Computing outer products"
        o = map(np.outer, rays, rays)
        print "Inverting outer products"
        i = map(np.linalg.inv, o)
        print "Done"
        # Resize to original image size
        w = self.cam.imsize[0]
        h = self.cam.imsize[1]
        return np.reshape(i, (w, h, 3, 3))
    #end
    
    #def fastApproxLeastSquares(self, range, width):
    # Compute scaled vectors
    # Integral image of scaled vectors
    # Compute average scaled vector hat{b} for each pixel
    # Normal is hat{M}^-1[i,j] * hat{b}[i,j]

    def byEigenvector(self, range, width):
        size = np.shape(range)

        p = mapToRays(cam, range).reshape((size[0],size[1],3))
        print "Shape p: ", np.shape(p)

        sum = average(p,width) * (width * width)
        print "Shape sum: ", np.shape(sum)
        #print n_ave

        pp = map(np.outer, p.reshape((-1,1,3)), p.reshape((-1,1,3)))
        print "Shape pp: ", np.shape(pp)
        print "pp[0] = ", pp[0]

        ppinv = map(np.linalg.inv, pp)
    
        n = ppinv * sum
    
    
        # Normalize to length 1.
        #l = np.sqrt((n_ave**2).sum(axis=1))
        #n_ave[:,0]/=l
        #n_ave[:,1]/=l
        #n_ave[:,2]/=l

        return n_ave,n
        #return n
    #end

if __name__=='__main__':
  import argparse

  parser = argparse.ArgumentParser(description='Compute normals from ranges.')
  parser.add_argument('-f',metavar='files',dest='files',required=True,
                      nargs='+',help='Files to process')
  parser.add_argument('-c',metavar='PATH',dest='camera',required=True,
                      help='Path to camera configuration')
  parser.add_argument('-o',metavar='PATH',dest='output',required=True,
                      help='Output path to store ply files')
  parser.add_argument('-t',metavar='threshold',dest='threshold',
                      default=0.05,type=float,help='Filtering threshold')
  parser.add_argument('-a',dest='use_ascii',action='store_true',default=False,
                      help='Use ASCII output [def=Binary]')
  parser.add_argument('-bw',metavar='bilateral width',dest='bilateral_width',
                      default=8,type=int,
                      help='Window width for bilateral filtering.')
  parser.add_argument('-brs',metavar='bilateral range sigma',
                      dest='bilateral_rsigma', default=10,type=float,
                      help='Range sigma for bilateral filtering.')
  parser.add_argument('-bps',metavar='bilateral pixel sigma',
                      dest='bilateral_psigma', default=10,type=float,
                      help='Pixel sigma for bilateral filtering.')

  args = parser.parse_args()

  df=DataFile()
  cam=PinHoleCamera.load(args.camera)
  normals=Normals(cam)
  rfilt=RangeFilter(args.threshold)

  # Make output directory if necessary
  if not os.path.isdir(args.output):
    print 'Creating output directory',args.output
    os.makedirs(args.output)
  #end


  for f in args.files:
    
    print "Processing frame: ", f

    # Create filenames we need by parsing input filename.
    # We assume the frame number is the substring up to the first '_'.
    # We assume the timestamp is the number between the second and third '_'
    # characters.
    fbase=os.path.basename(f)
    parts = fbase.split('_')
    framenum = parts[0]
    timestamp = parts[2]
    nfn = os.path.join(args.output,'pointCloud'+framenum+'-normal.ply')

    df.loadSmart(f)
    fbase=os.path.basename(f)

    df.filterRange()
    mask=df.getMask()

    range = df.getRange()
    
    smooth = rfilt.bilateralFilterSmoother(range, args.bilateral_width, 
                                           args.bilateral_rsigma,
                                           args.bilateral_psigma)

    n = normals.getBySRI(smooth)

    plySavePoints(nfn, n.astype('float32'), ascii=args.use_ascii)

#end
