#!/usr/bin/python

import os,sys
import numpy as np


from ply_io import *
from camera_model import *
from misc_utils import *
from datafile import *
from range_filter import *

  
def mapToRays(cam,range):
  
  # scale the rays based on the range
  r=np.reshape(range,(-1,1))
  rays=cam.rays*np.hstack((r,r,r))
  
  #R=np.array([[0,0,1],[-1,0,0],[0,-1,0]]).T
  
  #rr=rays[mask[::10],:]
  #return np.dot(rays[mask,:],R)
  return rays
    
  

if __name__=='__main__':
  import argparse

  parser = argparse.ArgumentParser(description='Show 3D points.')
  parser.add_argument('-f',metavar='files',dest='files',required=True,
                      nargs='+',help='Files to process')
  parser.add_argument('-c',metavar='PATH',dest='camera',required=True,
                      help='Path to camera configuration')
  parser.add_argument('-o',metavar='PATH',dest='output',required=True,
                      help='Output path to store ply files')
  parser.add_argument('-t',metavar='threshold',dest='threshold',default=0.05,type=float,
                      help='Filtering threshold')
  parser.add_argument('-nocolor',dest='use_color',action='store_false',default=True,
                      help='Disable color output [def=use color]')
  parser.add_argument('-a',dest='use_ascii',action='store_true',default=False,
                      help='Use ASCII output [def=Binary]')

  args = parser.parse_args()

  df=DataFile()
  cam=PinHoleCamera.load(args.camera)
  
  rfilt=RangeFilter(args.threshold)

  if not os.path.isdir(args.output):
    print 'Creating output directory',args.output
    os.makedirs(args.output)
  #end

  for f in args.files:
    
    df.loadSmart(f)
    fbase=os.path.basename(f)

    df.filterRange()
    mask=df.getMask()

    if True:
      range = df.getRange()
      # Median Blur + edge filtering
      #smooth = rfilt.medianBlurSmoother(range, 5)
      #mask,g = rfilt.simpleEdgeFilter(range, mask)
      #mask = np.logical_and(mask, rfilt.fillInEdgeFilter(range))

	  # Gaussian smoothing + edge filtering
      #smooth = rfilt.fillInAverage(range, 5, mask)
      #smooth = rfilt.gaussianBlurSmoother(range, 15)
      #mask,g = rfilt.simpleEdgeFilter(smooth, mask)

      # No smoothing with outliers removed by truncation
      #mask = rfilt.updateMask(mask, range, minimum=0.0, maximum=10.0)
      #smooth = range

      # Bilateral filtering + edge filtering + outlier removal by truncation
      smooth = rfilt.bilateralFilterSmoother(range, 15, 20, 10)
      mask,g = rfilt.simpleEdgeFilter(smooth, mask)
      mask = rfilt.updateMask(mask, smooth, minimum=1.5, maximum=10)

      pts = mapToRays(cam, smooth)
      m = mask.ravel()
    else:
      # The original way, just edge filtering.
      mask,g=rfilt.simpleEdgeFilter(df.getRange(),mask)
      m=mask.ravel()
      # convert to 3D points
      pts=mapToRays(cam,df.getRange())
    #end

    # setup the colors
    gim = df.scaleTo8bitMinMax(-5000.0, 25000.0).reshape((-1,1))
    
    cols=np.hstack((gim,gim,gim))

    ofn=os.path.join(args.output,fbase+'.ply')
    print 'Saving to ',ofn
    if args.use_color:
      plySavePoints(ofn,pts[m,:].astype('float32'),
                    color=cols[m,:],ascii=args.use_ascii)
    else:
      plySavePoints(ofn,pts[m,:].astype('float32'),ascii=args.use_ascii)
    #end
  #end
