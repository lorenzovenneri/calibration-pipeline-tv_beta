import numpy as np

# misc utils
def skew(vec):
  return np.array([[0,      -vec[2],  vec[1]],
                   [vec[2],       0, -vec[0]],
                   [-vec[1], vec[0],      0 ]])

def invSkew(a):
  #return np.array([-a[1,2],a[0,2],-a[0,1]]).reshape((-1,1))
  return np.array([-a[1,2],a[0,2],-a[0,1]])

class AngleAxis:
  def __init__(self,angle,axis):
    self.angle=angle
    self.axis=axis/np.linalg.norm(axis)

  def toCayley(self):
    return Cayley(self.axis*np.tan(self.angle/2.0))

  @staticmethod
  def fromR(R):
    a=np.arccos(0.5*(np.trace(R)-1.0))
    w=0.5/(np.sin(a))*np.array([R[2,1]-R[1,2],
                                R[0,2]-R[2,0],
                                R[1,0]-R[0,1]])
    return AngleAxis(angle=a,axis=w)
    

  def toR(self):
    # rodrigues formula
    wx=skew(self.axis)
    wx2=np.dot(wx,wx)

    # note this is to set it up as R not R'
    return np.eye(3)-wx*np.sin(self.angle)+wx2*(1-np.cos(self.angle))

  # rotate the vectors. This is the same as y=R^T x
  def rotv(self,vecs):
    # rodrigues rotation

    wn=self.axis.reshape((-1,1))
    wx=skew(self.axis)
    c=np.cos(self.angle)
    s=np.sin(self.angle)

    if len(vecs.shape)==1:
      vr=vecs*c+np.dot(wx,vecs)*s+(wn*np.dot(wn.T,vecs)*(1.0-c)).squeeze()
    else:
      N=vecs.shape[1]
      vr=vecs*c+np.dot(wx,vecs)*s
      vr+=np.tile(wn,(1,N))*np.dot(wn.T,vecs)*(1.0-c)
    #end
    return vr

  # rotate the vectors. This is the same as y=R^T x
  def rot(self,vecs):
    return AngleAxis(angle=-self.angle,axis=self.axis).rotv(vecs)

  def __str__(self):
    return '[%4.2f deg %s]'%(np.rad2deg(self.angle),self.axis)

class Cayley:
  def __init__(self,w):
    self.w=w
    
  def toR(self):
    A=skew(self.w)
    return np.dot(np.eye(3)-A,np.linalg.inv(np.eye(3)+A))

  # van golub method
  @staticmethod
  def fromR(R):
    Q=np.dot(np.linalg.inv(np.eye(3)+R),np.eye(3)-R)
    return Cayley(invSkew(Q))

  def toAngleAxis(self):
    return AngleAxis(angle=self.getAngle(),axis=self.w)

  def getAngle(self):
    return 2.0*np.arctan(np.linalg.norm(self.w))

  def __str__(self):
    return 'w=%s (a=%3.2f deg, wn=%s)'%(self.w,np.rad2deg(self.getAngle()),
                                     self.w/np.linalg.norm(self.w))


if __name__=='__main__':

  print 'Testing rotations'

  vec=np.array([1,2,3])
  M=skew(vec)
  print 'vec=',vec
  print 'cross(vec)=',M
  print 'test cross(vec)=',M+M.T
  print 'uncross(M)=',invSkew(M)
  print '\n'

  v=np.random.rand(3,4)

  print '\n\n==========\nTesting Angle Axis'
  aa=AngleAxis(axis=[0,0,1],angle=np.deg2rad(90.0))
  print 'aa=',aa
  print 'R=',aa.toR()
  print 'det(R)=',np.linalg.det(aa.toR())
  print 'identity=',AngleAxis.fromR(aa.toR())

  x=np.array([1,0,0])
  y=np.dot(aa.toR(),x)
  print '\nRotations of vectors\n'
  print 'x=%s -> Rx=y=%s'%(x,y)

  y=aa.rotv(x)
  print 'x=%s -> rotv(x)=%s'%(x,y)
  y=aa.rot(x)
  print 'x=%s -> rot(x)=%s'%(x,y)

  print '\n'
  vr=aa.rotv(v)
  print 'v=\n',v
  print 'rotv(v)=\n',vr
  print 'test=\n',np.linalg.norm(np.diag(np.dot(v[:2,:].T,vr[:2,:])))

  print '\n\n==========\nTesting Cayley'
  
  c=aa.toCayley()
  print 'cayley=',c
  print ' (as aa):',c.toAngleAxis()

  print '\nRotation Matrices'
  print 'R=',c.toR()
  print 'det(R)=',np.linalg.det(c.toR())
  print 'cf R=',aa.toR()

  print '\nRotating a vector'
  y=np.dot(c.toR(),x)
  print 'x=%s -> Rx=y=%s'%(x,y)

  # why is this transposed???
  print '\nRotation matrix text'
  print 'Rtest=',np.linalg.norm(c.toR()-aa.toR())
  print 'sanity=',Cayley.fromR(c.toR())
  
  
  
  
