import numpy as np
from gutils import *


# note -- does no normalization. Should do that
# assumes points are in column format

class Plane3D:
  def __init__(self,plane):
    self.plane=plane

  @staticmethod
  def fromVecPoint(normal,p0):
    a=np.linalg.norm(normal)
    n=normal/a
    d= -np.dot(n,p0)
    return Plane3D(np.hstack((n.squeeze(),d)))

  @staticmethod
  def exactFit(x):
    n=np.cross(x[:,1]-x[:,0],x[:,2]-x[:,0])
    return Plane3D.fromVecPoint(n,x[:,0])

  @staticmethod
  def lsqFit(X):
    # check we have enough points
    if X.shape[1]<3:
      print 'Not enough points!!!'
      raise
    #end

    # estimate the plane
    # ax+by+cr+d = 0
    A=homog(X).T
    [u,s,v]=np.linalg.svd(A,full_matrices=False)

    #print 's=',s
    #print 'v=',v

    # solution is the last one -- matrix output is already transposed
    plane=Plane3D(plane=v[-1,:])

    # plane fit score is given by singular values
    # TODO: should we use the trace?
    return (plane,s[-1]/np.sum(s))

  def distance(self,X):
    # assumes normalized vector
    return (np.dot(self.plane[:3],X)+self.plane[3])

  # project the points onto the plane. Basically take off the
  # component perpendicular to the plane. Assumes normalized vector
  def projectToPlane(self,X):
    return X-np.outer(self.plane[:3],self.distance(X))

  def asVec(self):
    return self.plane
  
  def getParts(self):
    n=self.plane[:3]

    print 'n',n
    a=np.linalg.norm(n)
    return (n/a,-self.plane[3]/a)

  def normalize(self):
    self.plane/=np.linalg.norm(self.plane)
    return self

  def norm(self):
    return Plane3D(self.plane/np.linalg.norm(self.plane))
    
  def normalizeVec(self):
    self.plane/=np.linalg.norm(self.plane[:3])
    return self

  def normVec(self):
    return Plane3D(self.plane/np.linalg.norm(self.plane[:3]))

  # rays that start from the origin
  def originRayIntersectDepth(self,rays):
    return -self.plane[3]/np.dot(self.plane[:3],rays)

  # rays that start from p0 with direction ray
  def rayIntersectDepth(self,rays,p0):
    n=self.plane[:3]
    return (-np.dot(n,p0)-self.plane[3])/np.dot(n,rays)

  def __str__(self):
    return str(self.plane)


# Cauchy distribution...not yet used
class Cauchy:
  def __init__(self,x0=0.0,gamma=1.0):
    self.x0=x0
    self.gamma=gamma
    self.gamma2=gamma**2
    self.logK=-np.log(np.PI)+np.log(gamma)

  def logLikelihood(self,x):
    return self.logK+np.log((x-self.x0)**2+self.gamma2)

  def likelihood(self,x):
    return self.gamma/((x-self.x0)**2+self.gamma2)/np.PI
    
# temporarily here
class RansacPlane3D:
  def __init__(self,max_iterations=200,min_iterations=10,thresh=0.1):
    self.max_iterations=max_iterations
    self.min_iterations=min_iterations
    self.thresh=thresh

  def estimate(self,X):
    npts=X.shape[1]
    inliers=np.ones(npts)

    best_plane=None
    best_score=0
    
    outlier_ratio=0.5
    n=0
    nmax=self.max_iterations
    while n<nmax and npts>3:
      # pick our minimal set
      #ind=np.random.choice(npts,size=3,replace=False)

      found=False
      while not found:
        ind=np.random.randint(npts,size=3)
        found=(ind[0]!=ind[1] and ind[0]!=ind[2])
      #end

      # estimate the plane exactly
      plane=Plane3D.exactFit(X[:,ind])

      # normalize plane to get consistent distances...
      plane.normalize()

      # test inliers
      score=np.sum(np.abs(plane.distance(X))<self.thresh)

      #print 'n',n,'score',score

      if score>best_score:
        best_plane=plane
        best_score=score

        # re-estimate number of iterations
        err=1.0-float(score)/float(npts)
        if err>0.0:
          nt=np.ceil(np.log(0.01)/np.log(1.0-(1.0-err)**3))
          nmax=np.minimum(self.max_iterations,nt)
          nmax=np.maximum(nmax,self.min_iterations)
        else:
          nmax=n
        #end

        #print 'e=',err,'nmax=',nmax,'n',n
      #end

      n+=1
    #end

    if best_plane==None:
      return (False,None,0.0,None)
    #end
    
    # re-estimate the plane using the inliers
    inliers=np.abs(best_plane.distance(X))<self.thresh

    #print 'max iterations',nmax,'n',n
    #print 'best plane norm',best_plane.norm()
    #print 'reestimate from ',np.sum(inliers),'inliers'
    
    plane,qual=Plane3D.lsqFit(X[:,inliers])


    return (True,plane,np.sum(inliers),inliers)

    

if __name__=='__main__':

  from rotations import *
  import matplotlib.pyplot as plt
  from mpl_toolkits.mplot3d import Axes3D as ax3d
  import argparse

  p = argparse.ArgumentParser(description='Test plane fit.')
  p.add_argument('-v',dest='verbose',default=False,action='store_true',
                 help='Verbose')
  args = p.parse_args()

  # generate a random plane
  n=np.random.rand(3)
  n/=np.linalg.norm(n)
  p0=np.random.rand(3)

  # generate some random points in 3D
  N=100
  X=np.random.rand(3,N)

  # push them onto the surface, with some gaussian noise
  sigma=0.005
  d=np.dot(n,X)-np.dot(n,p0)
  
  Y=X-np.outer(d+np.random.rand(d.size)*sigma,n).T

  # add some gross outliers
  m=np.random.rand(N)<0.2
  good=np.logical_not(m)
  Y[:,m]=np.random.rand(3,np.sum(m))

  ptrue=Plane3D.fromVecPoint(n,p0)

  # now estimate the surface
  ransac=RansacPlane3D(thresh=0.01)

  rv,plane,score,inliers=ransac.estimate(Y)
  if not rv:
    print 'RANSAC FAILED!!!'
  #end

  err=min(np.linalg.norm(ptrue.norm().asVec()-plane.asVec()),
          np.linalg.norm(ptrue.norm().asVec()+plane.asVec()))

  if err>0.1:
    print 'ERROR RANSAC seemed to fail....'
    print 'Error from truth: ',err
    print 'Plane true',ptrue.norm()
    print 'Estimate',plane.norm()
    raise Exception('Plane: Failed to estimate correctly')
  #end

  if args.verbose:
    print 'true max dist to plane',np.abs(np.dot(n,Y)-np.dot(n,p0)).max()
    print 'est max dist to plane',np.abs(plane.distance(Y)).max()
  #end

  if args.verbose:
    print 'RANSAC Score=',score
    print 'Plane',plane

    fig = plt.figure(0)
    ax = fig.add_subplot(111, projection='3d')
    ax.plot(Y[0,:],Y[1,:],Y[2,:],'b.')
    plt.hold(True)
    ax.plot(Y[0,m],Y[1,m],Y[2,m],'r.')
    pp=p0+n
    ax.plot([p0[0],pp[0]],[p0[1],pp[1]],[p0[2],pp[2]],'g-')

    ax.plot(Y[0,inliers],Y[1,inliers],Y[2,inliers],'mo')
  
    ne,de=plane.getParts()
    pe=de*ne
    pe2=pe+ne
    ax.plot([pe[0],pe2[0]],[pe[1],pe2[1]],[pe[2],pe2[2]],'k-')  

    plt.axis('equal')
    plt.ioff()
    plt.show()
  #end
  
  
  print 'OK...'
  


  

               
