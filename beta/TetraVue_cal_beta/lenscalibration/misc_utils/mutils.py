import os

def testMakeDir(p):
  if not os.path.isdir(p):
    print 'Making directory:',p
    os.makedirs(p)
  else:
    print 'path:',p,'exists'
  #end
  return p
