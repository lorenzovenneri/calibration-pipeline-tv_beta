import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D as ax3d


def plot3_quick(x):
  fig = plt.figure(0)
  ax = fig.add_subplot(111, projection='3d')
  ax.plot(x[0,:],x[1,:],x[2,:],'b.')
  plt.axis('equal')
  ax.set_xlabel('x')
  ax.set_ylabel('y')
  ax.set_zlabel('z')
  plt.ioff()
  plt.show()

def imshow_quick(x):
  plt.clf()
  plt.imshow(x)
  plt.ioff()
  plt.show()

def plot_quick(x):
  plt.clf()
  plt.plot(x)
  plt.ioff()
  plt.show()

def plot_show():
  plt.ioff()
  plt.show()
  

def plot_maximize():
  mng = plt.get_current_fig_manager()
  mng.resize(*mng.window.maxsize())      
  #plt.show()
  plt.draw()
