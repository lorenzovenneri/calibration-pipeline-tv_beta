import numpy as np

def bound(a,lo,hi):
  return np.minimum(np.maximum(a,lo),hi)

def linearMapToColors(val,cm):
  return cm.cmap[linearMapToColorIndex(val,cm),:]

def linearMapToColorIndex(val,cm):

  valid=np.isfinite(val)
  vhi=val[valid].max()
  vlo=val[valid].min()

  vv=(val-vlo)/(vhi-vlo)*float(cm.nrCols())

  vi=np.floor(((val-vlo)/(vhi-vlo))*float(cm.nrCols())).astype(np.int)
  vi[np.logical_not(valid)]=0
  
  return np.minimum(vi,cm.nrCols()-1)


class JetColormap:
  def __init__(self,N=256):

    self.N=N
    x=np.linspace(0,1,N)
    r = bound(np.minimum(4*x - 1.5,-4*x + 4.5),0.0,1.0)
    g = bound(np.minimum(4*x - 0.5,-4*x + 3.5),0.0,1.0)
    b = bound(np.minimum(4*x + 0.5,-4*x + 2.5),0.0,1.0)

    self.cmap=np.dstack((r,g,b)).squeeze()

  def nrCols(self):
    return self.cmap.shape[0]
  
  def colormap(self):
    return self.cmap
  
  def image(self,a):
    if a.dtype.kind in 'bfc':
      b=a.astype('int')
      c=self.cmap[b.ravel(),:]
    else:
      c=self.cmap[a.ravel(),:]
    #end
    return c.reshape((a.shape[0],a.shape[1],3))
    

  def imagesc(self,a):
    amin=a.ravel().min()
    amax=a.ravel().max()

    ai=((a.astype('float32')-amin)/(amax-amin)*float(self.N-1)).astype('int')
    c=self.cmap[ai.ravel(),:]
    return c.reshape((ai.shape[0],ai.shape[1],3))
       
  def imagesc255(self,a):
    return (self.imagesc(a)*255).astype('int')


if __name__=='__main__':
  import matplotlib.pyplot as plt

  h=300
  w=400
  
  a=np.arange(h*w).reshape((h,w))

  cm=JetColormap()

  im=cm.imagesc(a)

  plt.clf()
  plt.imshow(im)
  plt.ion()
  plt.show()
  
    
