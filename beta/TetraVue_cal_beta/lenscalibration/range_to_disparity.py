#!/usr/bin/python

import os,sys
import numpy as np


from ply_io import *
from camera_model import *
from misc_utils import *
from datafile import *
from range_filter import *
import numpy.ma as ma
  
def mapToRays(cam,range):
  
  # scale the rays based on the range
  r=np.reshape(range,(-1,1))
  rays=cam.rays*np.hstack((r,r,r))
  
  #R=np.array([[0,0,1],[-1,0,0],[0,-1,0]]).T
  
  #rr=rays[mask[::10],:]
  #return np.dot(rays[mask,:],R)
  return rays

def rangeToDisparity(df, focalLength):
    msk = df.getMask()
    # Compute disparity as f/z for all valid pixels
    zvals = df.getRange()
    # Eliminate NaN
    isn = np.isnan(zvals)
    badvals = ~msk | isn
    zvals[badvals] = 1.0
    invz = 1/zvals
    invz[badvals] = 0.0
    dvals = focalLength * invz
    return dvals
  
# Leave the masking and range computation to the caller
def rangeToDisparity2(range, focalLength):
  zvals = range.copy()
  isn = np.isnan(zvals)
  zvals[zvals==0.0] = 1.0
  zvals[isn] = 1.0
  invz = 1/zvals
  invz[isn] = 0.0
  dvals = focalLength * invz
  return dvals

if __name__=='__main__':
  import argparse

  parser = argparse.ArgumentParser(description='Show 3D points.')
  parser.add_argument('-f',metavar='files',dest='files',required=True,
                      nargs='+',help='Files to process')
  parser.add_argument('-c',metavar='PATH',dest='camera',required=True,
                      help='Path to camera configuration')
  parser.add_argument('-o',metavar='PATH',dest='output',required=True,
                      help='Output path to store ply files')
  parser.add_argument('-t',metavar='threshold',dest='threshold',default=0.05,
                      type=float,help='Filtering threshold')

  args = parser.parse_args()

  df=DataFile()
  cam=PinHoleCamera.load(args.camera)
  focalLength = cam.K[0,0]

  rfilt=RangeFilter(args.threshold)

  if not os.path.isdir(args.output):
    print 'Creating output directory',args.output
    os.makedirs(args.output)
  #end

  for f in args.files:
    print "File: ", f

    df.loadSmart(f)
    framenum = os.path.basename(f).split('_')[0]
    dfn = os.path.join(args.output,'image'+framenum+'-disparity.pgm')

    df.filterRange()
    mask = df.getMask()

    # Uncomment this to apply the filtering.  For this purpose, we've 
    # found no filtering to be best.
    #msk=df.getMask()
    #msk,g=rfilt.simpleEdgeFilter(df.getRange(),msk)

    focalLength = cam.K[0,0]
    range = df.getRange()
    dvals = rangeToDisparity2(range,focalLength)
    dvals[~mask] = 0.0
    # For TMAP, disparity images are 16-bit images containing
    # (short)(disparity * 16)
    dvals = dvals * 16.0
    cv2.imwrite(dfn,dvals.astype('uint16'));

    # Create more user-friendly range images, for verification.
    # This just scales the output range to use the full 0-255.
    if (1):
      dmin = -5000.0
      dmax = 25000.0
      dvals[dvals>dmax] = dmax
      dvals[dvals<dmin] = dmin
      dvals = (dvals-dmin) * 255.0/(dmax-dmin)
      ofn = os.path.join(args.output, 'image'+framenum+'-dispIm.pgm')
      cv2.imwrite(ofn, dvals.astype('uint8'))
    #end

  #end
