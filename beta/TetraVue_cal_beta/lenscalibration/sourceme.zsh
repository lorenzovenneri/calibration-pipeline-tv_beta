export BASE=`pwd`/..
if [ -z $EXT ]; then
    export EXT=$BASE/../external
fi

export PYTHONPATH=$BASE/bin.linux64/src:$EXT/lib/python2.7/dist-packages:$PYTHONPATH

if [ -d $EXT/lib/python2.7/site-packages ]; then
    echo "adding scite-packages"
    export PYTHONPATH=$EXT/lib/python2.7/site-packages:$PYTHONPATH
fi
export LD_LIBRARY_PATH=$EXT/lib:$LD_LIBRARY_PATH



