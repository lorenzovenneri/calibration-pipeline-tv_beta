import numpy as np
import os,sys,csv
import cv2

# pin hole camera model
class PinHoleCamera:
  def __init__(self,K,distort,imsize):
    self.K=K
    self.distort=distort
    self.imsize=tuple(imsize)

    self.w=imsize[0]
    self.h=imsize[1]

    #print "imsize:",self.imsize

    # initialize undistortion maps
    R=np.eye(3,dtype=np.float32)
    #(map1,map2)=cv2.initUndistortRectifyMap(K,distort,R,K,tuple(imsize),cv2.CV_16SC2)

    # maps for fast undistortion and rectification
    (mx,my)=cv2.initUndistortRectifyMap(K,distort,R,K,self.imsize,cv2.CV_16SC2)
    self.mapx=mx
    self.mapy=my
    self.calcUndistortRays()

  @staticmethod
  def load(path):
    try:
      K=np.loadtxt(os.path.join(path,'kmat.dat'))
      distort=np.loadtxt(os.path.join(path,'distort.dat'))

      with open(os.path.join(path,'imsize.csv'),'rb') as csvfile:
        r = csv.reader(csvfile,delimiter=',')
        s=r.next()
        imsize=(int(s[0]),int(s[1]))
      #end
      return PinHoleCamera(K,distort,imsize)
    except:
      raise

  def undistort(self,img):
    return cv2.remap(img,self.mapx,self.mapy,cv2.INTER_LINEAR)

  def projectPoints(self,world_pts,R,T):
    return cv2.projectPoints(world_pts,R,T,self.K,self.distort)

  def undistortPoints(self,pts):
    return cv2.undistortPoints(pts,self.K,self.distort)

  def getRays(self):
    return self.rays

  def getRayImage(self):
    return self.ray_im
    
  def calcUndistortRays(self):
    h=self.imsize[1]
    w=self.imsize[0]
    
    [yy,xx]=np.mgrid[0:h,0:w]
    pts=np.vstack((xx.ravel(),yy.ravel()))
    pts=pts.T.reshape((1,xx.size,2)).astype(np.float32)

    #print "pts.size=",pts.shape
    rays=cv2.undistortPoints(pts,self.K,self.distort).squeeze()
    self.rays=np.hstack((rays,np.ones((rays.shape[0],1))))

    # For some reason the data looks more correct without this normalization,
    # even though this should be the right thing to do.  So perhaps the range
    # data already take this into account?  They could have decided that the 
    # range data should just be the z-value for the 3D points.
    if False:
      # normalize them
      n=np.sqrt((self.rays**2).sum(axis=1))

      self.rays[:,0]/=n
      self.rays[:,1]/=n
      self.rays[:,2]/=n
    #end false
    # get them in an image format
    self.ray_im=np.reshape(self.rays,(h,w,3))
