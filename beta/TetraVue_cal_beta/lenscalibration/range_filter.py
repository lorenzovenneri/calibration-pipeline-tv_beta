#!/usr/bin/python

import numpy as np
import cv2

def average(range, w):
  # integral pads with 0's on top and left.
  i = cv2.integral(range)
  i = i[1:,1:]
  # Which is faster, that, or i = range.cumsum(0).cumsum(1) ?

  size = np.shape(range)
  paddedsize = size[0]+w,size[1]+w

  pi = np.zeros(paddedsize)
  pi[:-w,:-w] = i
  pww = pi[w:,w:]
  p0w = pi[:-w,w:] 
  pw0 = pi[w:,:-w] 
  p00 = pi[:-w,:-w]
  ave = pww - p0w - pw0 + p00
  # Pad rows have bogus values
  ave[-w:,:] = 0.0
  ave[:,-w:] = 0.0
  ave /= (w * w)
  return ave

class RangeFilter:
  def __init__(self,thresh=0.5,debug=False):
    self.thresh=thresh
    self.debug=debug

  def updateMask(self, mask, range, 
                 nonan=True, noinf=True, minimum=-float("inf"), 
                 maximum = float("inf")):
    if nonan:   mask = np.logical_and(mask, np.logical_not(np.isnan(range)))
    if noinf:   mask = np.logical_and(mask, np.isfinite(range))
    mask = np.logical_and(mask, range>minimum)
    mask = np.logical_and(mask, range<maximum)

    return mask

  # Should really apply this using the Spherical Range Image trick to
  # estimate surface normals
  def simpleEdgeFilter(self,range,mask):
    #rim=cv2.GaussianBlur(range,(0,0),5.0)
    rim=cv2.medianBlur(range,3)
    dx=cv2.Sobel(rim,-1,1,0)
    dy=cv2.Sobel(rim,-1,0,1)
    
    g=dx**2+dy**2

    return (np.logical_and(mask,g<self.thresh**2),g)

  # Compute the average *for all pixels* but only taking average of 
  # valid pixels.  If there is a w by w region with no valid pixels
  # the result is 0.
  #
  # This only works because we use 0 as a special value that doesn't 
  # change the sum of the valid pixels, but does change the 
  # sum of the valid mask pixels.
  # 
  # Because we use integral images to compute the result, you get an
  # empty border of w pixels on the bottom and right of the image.
  # 
  # Test this with, e.g. 
  #   rfilt = RangeFilter()
  #   testimage = (np.random.random(np.shape(rim))>0.1).astype('float')
  #   testmask = testimage==1
  #   filledin = rfilt.fillInBlur(testimage, 3, testmask)


  def fillInAverage(self, range, w, goodmask):
    mask = self.updateMask(goodmask, range, minimum=0, maximum=10)
    bad = np.logical_not(mask)
    r2 = range.copy()
    r2[bad] = 0

    blur = average(r2, w)
    maskblur = average(mask.astype('float'), w)
    nonzero = maskblur != 0
    blur[nonzero] /= maskblur[nonzero]
    return blur

  def fillInEdgeFilter(self,range):
    # Exclude egregious outliers
    toolarge = range>10.0
    toosmall = range<0.0
    mask = np.logical_and(np.logical_not(toolarge), np.logical_not(toosmall))

    # What we really want to do is fill in masked pixels based on 
    # neighboring unmasked pixels.  This is an approximation that
    # can be written in just a few lines.
    # This copy doesn't appear to be necessary, but it seems like the right
    # thing to do.
    r2 = range.copy();
    r2[np.logical_not(mask)] = 0

    blur1 = cv2.medianBlur(r2, 5)
    r2[np.logical_not(mask)] = blur1[np.logical_not(mask)]
    blur2 = cv2.medianBlur(r2, 5)

    dx = cv2.Sobel(blur2,-1,1,0)
    dy = cv2.Sobel(blur2,-1,0,1)
    
    g = dx**2+dy**2

    return g<self.thresh**2

  # For some reason cv2 doesn't let us use widths > 5.  we should 
  # write our own.
  def medianBlurSmoother(self, range, width):
    r2 = range.copy();
    mask = (np.ones(np.shape(range)) == 1)
    # Exclude egregious outliers
    mask = self.updateMask(mask, r2, minimum=0, maximum=10)
    bad = np.logical_not(mask)

    # What we really want to do is fill in masked pixels based on 
    # neighboring unmasked pixels.  This is an approximation that
    # can be written in just a few lines.
    r2[bad] = 0

    blur1 = cv2.medianBlur(r2, width)
    r2[mask] = blur1[mask]
    blur2 = cv2.medianBlur(r2, width)
    return blur2;

  def bilateralFilterSmoother(self, range, width, sigma_range, sigma_space,
                              minimum=-float("inf"),  maximum = float("inf")):
    r2 = range.copy();
    mask = (np.ones(np.shape(range)) == 1)
    # Exclude egregious outliers
    mask = self.updateMask(mask, r2, minimum=minimum, maximum=maximum)

    # Give bad pixels the fill-in-average.
    fillInAve = self.fillInAverage(r2, width, mask)
    bad = np.logical_not(mask)
    r2[bad] = fillInAve[bad]

    blur = cv2.bilateralFilter(r2, width, sigma_range, sigma_space)
    
    return blur

  def gaussianBlurSmoother(self, range, width):
    
    r2 = range.copy();
    mask = (np.ones(np.shape(range)) == 1)
    # Exclude egregious outliers
    mask = self.updateMask(mask, r2, minimum=0, maximum=10)
    bad = np.logical_not(mask)

    # What we really want to do is fill in masked pixels based on 
    # neighboring unmasked pixels.  This is an approximation that
    # can be written in just a few lines.
    r2[bad] = 0

    #blur1 = cv2.medianBlur(r2, width)
    #r2[mask] = blur1[mask]
    blur2 = cv2.GaussianBlur(r2, (0,0), width)
    return blur2;

  #def foo(self, range):
  #  m2 = fillInEdgeFilter(range)
  #  blurred = medianBlurFilter(range, 5)
  #  blurred[np.logical_not(m2)] = 0
  #  return blurred

def rangeTo8bit(rim):
  m=np.logical_and(rim>0,np.isfinite(rim))

  maxv=rim[m].max()
  minv=rim[m].min()

  print "maxv, minv = %s, %s\n" % (maxv, minv)

  rv=(255.0*(rim-minv)/(maxv-minv+1)).astype('uint8')
  rv[np.logical_not(m)]=0
  return rv

def r8(rim):
  smallenough = rim<=10.0
  bigenough = rim>=0.0
  notnan = np.logical_not(np.isnan(rim))
  finite = np.isfinite(rim)
  m=np.logical_and(
    np.logical_and(
      np.logical_and(bigenough, smallenough), finite), notnan)

  rv=(255.0*rim/10.0).astype('uint8')
  rv[np.logical_not(m)]=0
  return rv

if __name__=='__main__':
  import os,sys
  import argparse
  import matplotlib.pyplot as plt
  from datafile import *

  p = argparse.ArgumentParser(description='Load raw file and display.')
  p.add_argument('-f',dest='files',required=True,nargs='+',
                 help='File to load')
  p.add_argument('-t',dest='threshold',default=0.5,type=float,
                 help='Threshold to use')
  p.add_argument('-w',metavar='msec',dest='delay',default=0,
                      type=int,help='Wait time between frames')
  p.add_argument('-s',dest='sigma_range',default=5,type=float,
                 help='Sigma for bilateral filtering')
  p.add_argument('-p',dest='sigma_pixel',default=5,type=float,
                 help='Sigma for bilateral filtering')
  args = p.parse_args()

  df=DataFile()
  rfilt=RangeFilter(thresh=args.threshold,debug=True)

  if False:


    for f in args.files:
      df.loadSmart(f)

      rim=df.getRange()
      mask=df.getMask()

      mask2,g=rfilt.simpleEdgeFilter(rim,mask)

    
      plt.clf()
      plt.subplot(221)
      plt.imshow(im,cmap='gray')
      plt.hold(True)
      plt.imshow(mask2,alpha=0.25)
      plt.title('Color')
      plt.axis('off')

      plt.subplot(222)
      plt.imshow(rim)
      plt.hold(True)
      plt.imshow(mask2,alpha=0.25)
      plt.title('Range')
      plt.axis('off')

      plt.subplot(223)
      plt.title('Slope')
      plt.imshow(g)
      plt.axis('off')

      plt.subplot(224)
      plt.imshow(mask)
      plt.title('Raw Mask')
      plt.axis('off')

      plt.tight_layout()
      plt.ioff()
      plt.show()
    #end
  elif False:
    cv2.namedWindow('Range')
    cv2.namedWindow('Filtered')

    for f in args.files:
      df.loadSmart(f)

      df.filterRange()
      rim=df.getRange()

      mask=df.getMask()

      # rim is black if shown here.
      # cv2.imshow('Range',rangeTo8bit(rim))

      mask2,g=rfilt.fillInEdgeFilter(rim,mask)
      r2=rim.copy()
      r2[np.logical_not(mask2)]=0

      cv2.imshow('Range',rangeTo8bit(rim))
      cv2.imshow('Filtered',rangeTo8bit(r2))

      cv2.waitKey(args.delay)
    #end
  elif False:
    cv2.namedWindow('Range')

    for f in args.files:
      df.loadSmart(f)
      df.filterRange()
      rim=df.getRange()
      cv2.imshow('Range',r8(rim))
      cv2.waitKey(args.delay)
    #end
    
  else:
    cv2.namedWindow('Range')
    cv2.namedWindow('Fill In Average')
    cv2.namedWindow('Median Blur')

    for f in args.files:
      df.loadSmart(f)
      df.filterRange()
      rim=df.getRange()
      mask=df.getMask()

      ave = rfilt.fillInAverage(rim, 3, mask)
      med = rfilt.medianBlurSmoother(rim, 5)
      bil = rfilt.bilateralFilterSmoother(rim, 5, 
                                          args.sigma_range, args.sigma_pixel)

      cv2.imshow('Range',r8(rim))
      cv2.imshow('Fill In Average',r8(ave))
      cv2.imshow('Median Blur',r8(med))
      cv2.imshow('Bilateral', r8(bil))

      cv2.waitKey(args.delay)
    #end

  #end
