import os
import shutil
import sys
import numpy as np
import fnmatch
import os
import matplotlib as mpl
import matplotlib.cm as cm
from matplotlib import pyplot as plt
import matplotlib.colors as colors
from mpl_toolkits.mplot3d import Axes3D
import cv2
import time
import math
import matplotlib.pyplot as plt
import struct
import subprocess
import gc
import multiprocessing
from PIL import Image

def importdats(image_file_LOC):
    image_file = open(image_file_LOC)

    image_data = image_file.read()
    # read dimensions which are LE at 12-15 (height) and 16-19 (width) float32
    height = struct.unpack('<L', image_data[0:4])[0]
    width = struct.unpack('<L', image_data[4:8])[0]
    # image data starts after header which is 256 bytes and is LE float32
    image_file.seek(8, os.SEEK_SET)
    actual_data = np.fromfile(image_file, '<f')
    actual_data = np.asarray(actual_data)
    image = (np.reshape(actual_data, (height, width)))

    image_file.close()
    return image

def importLUTall(lut_loc,manypoints):
    lut_file = open(lut_loc)
    lut_data = lut_file.read()
    # HEADER 12 bytes; read dimensions which are LE at 12-15 (height) and 16-19 (width) float32
    heightLUT = struct.unpack('<I', lut_data[0:4])[0]
    widthLUT = struct.unpack('<I', lut_data[4:8])[0]
    if manypoints>0:
        numpoints = struct.unpack('<I', lut_data[8:12])[0]
        lut_file.seek(12, os.SEEK_SET)

    else:
        numpoints = 1
        lut_file.seek(8, os.SEEK_SET)

    # image data starts after header which is 256 bytes and is LE float32
    actual_data = np.fromfile(lut_file, '<f')
    actual_data = np.asarray(actual_data)
    print 'importing LUT: ' + lut_loc + '   with height:width:numpoints - ' \
          + str(heightLUT) + ':' + str(widthLUT) + ':' + str(numpoints) + ' | and total size - ' + str(actual_data.size)
    thisLUT = np.zeros((heightLUT, widthLUT, numpoints))
    start = 0

    for row in range(heightLUT):
        for col in range(widthLUT):
            for i in range(numpoints):
                thisLUT[row, col, i] = actual_data[start]
                start += 1
    lut_file.close()
    print 'complete LUT ', thisLUT.shape
    return thisLUT[:, :, :].squeeze()

def importXFORMS(lut_loc):
    lut_file = open(lut_loc)

    # HEADER 12 bytes; read dimensions which are LE at 12-15 (height) and 16-19 (width) float32
    thisLUT = np.zeros((4,4))
    actual_data = np.fromfile(lut_file, '<f')
    actual_data = np.asarray(actual_data)
    start = 0
    for row in range(4):
        for col in range(4):
            thisLUT[row, col] = actual_data[start]
            start += 1
    lut_file.close()
    return thisLUT[:, :].squeeze()

def exportXFORMS(lut2save, lut_loc):
    #### HEADER
    dims = lut2save.shape
    # height
    image_file = open(lut_loc, 'wb')
    for row in range(dims[0]):
        for col in range(dims[1]):

                myfmt = '<f'
                bin3 = struct.pack(myfmt, np.float32(lut2save[row, col]))
                image_file.write(bin3)

    image_file.close()
    return

def exportLUT(lut2save, lut_loc):
    #### HEADER
    dims = lut2save.shape
    # height
    image_file = open(lut_loc, 'wb')
    myfmt = '<I'
    bin = struct.pack(myfmt, dims[0])
    # width
    image_file.write(bin)
    myfmt = '<I'
    bin1 = struct.pack(myfmt, dims[1])
    image_file.write(bin1)
    # number points
    if len(dims) > 2:
        myfmt = '<I'
        bin2 = struct.pack(myfmt, dims[2])
        image_file.write(bin2)

    for row in range(dims[0]):
        for col in range(dims[1]):
            if len(dims) >2:
                for i in range(dims[2]):
                    myfmt = '<f'
                    bin3 = struct.pack(myfmt, np.float32(lut2save[row, col, i]))
                    image_file.write(bin3)
            else:
                myfmt = '<f'
                bin3 = struct.pack(myfmt, np.float32(lut2save[row, col]))
                image_file.write(bin3)

    image_file.close()
    return

def writeCMUdats(int_image,inputfilename):
    # CREATE INT FOR CMU PLANEFIT
    int_image = np.rot90(int_image)
    int_image = int_image.astype('>f')
    h, w = int_image.shape
    image_file = open(inputfilename, 'wb')
    image_file.write(struct.pack('>i4', h))
    image_file.write(struct.pack('>i4', w))
    int_image.tofile(image_file, '', '>f')
    image_file.close()
    return

def importCMUtxt(location):
    data = np.asarray(
        np.loadtxt((location), delimiter=' '))
    return data

def import_rng(filename):
    image_file = open(filename)
    image_data = image_file.read()
    # read dimensions which are LE at 12-15 (height) and 16-19 (width) float32
    height = struct.unpack('<L', image_data[13:17])[0]
    width = struct.unpack('<L', image_data[17:21])[0]
    # image data starts after header which is 256 bytes and is LE float32
    image_file.seek(256, os.SEEK_SET)
    actual_data = np.fromfile(image_file, '<f')
    # take to numpy array
    actual_data = np.asarray(actual_data)
    image = np.reshape(actual_data, (height, width))
    image_file.close()
    return image

def import_int(filename):
    image_file = open(filename)
    image_data = image_file.read()
    # read dimensions which are LE at 12-15 (height) and 16-19 (width) float32
    height = struct.unpack('<L', image_data[13:17])[0]
    width = struct.unpack('<L', image_data[17:21])[0]
    # image data starts after header which is 256 bytes and is LE float32
    image_file.seek(256, os.SEEK_SET)
    actual_data = np.fromfile(image_file, '<H')  # or <H if actual int data, or <f if .dat
    # take to numpy array
    actual_data = np.asarray(actual_data)
    image = np.reshape(actual_data, (height, width))
    image_file.close()
    return image

def import_cams(filename):
    image1 = []
    if filename.endswith('c1.bur') or filename.endswith('c2.bur') or filename.endswith('rng.bur'):
        image1 = import_rng(filename)
    elif filename.endswith('int.bur'):
        image1 = import_int(filename)
    elif filename.endswith('.dat'):
        image1 = importdats(filename)
    elif filename.endswith('.tif'):
        image1 = np.float32(Image.open(filename))
    else:
        print 'ERROR: data import failed datatype not supported at: ',filename
        sys.exit()
    return np.float32(image1)

def create_dir(folder):
    if not os.path.exists(folder):
        os.makedirs(folder)
        print 'creating directory: ' + folder
    else:
        shutil.rmtree(folder)
        os.makedirs(folder)
        print 'overwriting directory: ' + folder
    return

def register(first_im, second_im, saveloc):
    c1 = np.flipud(import_cams(first_im))
    c2 = import_cams(second_im)

    im1_gray = c2
    im2_gray = c1

    # Find size of image1
    sz = c1.shape

    # Define the motion model
    warp_mode = cv2.MOTION_EUCLIDEAN  # cv2.MOTION_EUCLIDEAN

    # Define 2x3 or 3x3 matrices and initialize the matrix to identity
    if warp_mode == cv2.MOTION_HOMOGRAPHY:
        warp_matrix0 = np.eye(3, 3, dtype=np.float32)
    else:
        warp_matrix0 = np.eye(2, 3, dtype=np.float32)

    # Specify the number of iterations.
    number_of_iterations = 100;

    # Specify the threshold of the increment
    # in the correlation coefficient between two iterations
    termination_eps = 1e-10;

    # Define termination criteria
    criteria = (cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, number_of_iterations, termination_eps)

    # Run the ECC algorithm. The results are stored in warp_matrix.
    (cc, warp_matrix0) = cv2.findTransformECC(im1_gray, im2_gray, warp_matrix0, warp_mode, criteria)
    print warp_matrix0
    if warp_mode == cv2.MOTION_HOMOGRAPHY:
        # Use warpPerspective for Homography
        c1al = cv2.warpPerspective(c1, warp_matrix0, (sz[1], sz[0]),
                                   flags=cv2.INTER_LINEAR + cv2.WARP_INVERSE_MAP)
    else:
        # Use warpAffine for Translation, Euclidean and Affine
        c1al = cv2.warpAffine(c1, warp_matrix0, (sz[1], sz[0]), flags=cv2.INTER_LINEAR + cv2.WARP_INVERSE_MAP)

    # write the matrix in 4X4 matrix
    warp_matrix = np.zeros((4, 4))
    warp_matrix[3, 3] = 1.0
    warp_matrix[2, 2] = 1.0
    warp_matrix[0, 0] = warp_matrix0[0, 0]
    warp_matrix[0, 1] = warp_matrix0[0, 1]
    warp_matrix[1, 0] = warp_matrix0[1, 0]
    warp_matrix[1, 1] = warp_matrix0[1, 1]
    warp_matrix[3, 0] = -warp_matrix0[0, 2]
    warp_matrix[3, 1] = -warp_matrix0[1, 2]

    if 1 == 1:
        fig = plt.figure(figsize=(10, 6))
        fig.patch.set_facecolor('white')
        plt.subplot(2, 1, 1)
        plt.imshow(c1 + c2)

        plt.subplot(2, 1, 2)
        plt.imshow(c1+c2)
        plt.savefig(saveloc)

    return warp_matrix.transpose()

if 1==1:
    loc1 =  'C:/Users/3dlabrat/Desktop/testing for LUTS/calibration_out_bin(X1.0)5/rw0/'
    loc1 =  'C:/Users/3dlabrat/Desktop/testing for LUTS/calibration_out_bin(X1.0)5/'

    loc2 = 'C:/Users/3dlabrat/Desktop/z005fEB15/cal_rw0/'
    LUT_T = 'angles.dat'
    numpoints = 0
    LUT1 = importLUTall(loc1+LUT_T,numpoints)
    print LUT1.shape
    # LUT1 = LUT1[:,:,17]
    # for i in range(LUT1.shape[2]):
    #     LUT1[:, :,i] = np.rot90(LUT1[:,:,i],2)

    LUT2 = importLUTall(loc2+LUT_T,numpoints)
    # LUT2 = LUT2[:,:,17]

    fig = plt.figure(figsize=(22, 7))
    fig.patch.set_facecolor('white')
    plt.subplot(131)
    plt.imshow(LUT1)
    plt.colorbar()
    plt.subplot(132)
    plt.imshow(LUT2)
    plt.colorbar()
    plt.subplot(133)
    plt.imshow(LUT1-LUT2)
    plt.colorbar()


    mean_dif = np.mean(np.abs(LUT1-LUT2))
    std_dif = np.std(np.abs(LUT1-LUT2))
    print mean_dif
    print std_dif

# 0.0788678623515
# 0.165885984279
#
#
# loc1 = 'C:/Users/3dlabrat/Desktop/sn007TESTNING/calibration_out_bin(X1.0)/'
# # loc1 =  'C:/Users/3dlabrat/Desktop/testing for LUTS/calibration_out1/'
#
# LUT_T = 'range_image.dat'
# numpoints = 0
# LUT1 = importLUTall(loc1+LUT_T,numpoints)
#
# fig = plt.figure(figsize=(22, 7))
# fig.patch.set_facecolor('white')
# plt.subplot(111)
# plt.imshow(LUT1)
# plt.colorbar()


plt.show()
