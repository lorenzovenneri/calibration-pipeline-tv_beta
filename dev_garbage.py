import os
import shutil
import sys
from matplotlib.patches import Rectangle
import numpy as np
import fnmatch
import os
import matplotlib as mpl
import matplotlib.cm as cm
from matplotlib import pyplot as plt
import matplotlib.colors as colors
from mpl_toolkits.mplot3d import Axes3D
import cv2
import time
import math
import matplotlib.pyplot as plt
import struct
from multiprocessing import Pool
import subprocess
import gc
import multiprocessing
from PIL import Image
from os.path import join as pjoin
def importdats(image_file_LOC):
    """
    Import of typical LabView 2D image format with 8 byte header indicating dimensions.
    :param image_file_LOC:
    :return:
    """
    image_file = open(image_file_LOC, 'rb')

    image_data = image_file.read()
    # read dimensions which are LE at 12-15 (height) and 16-19 (width) float32
    height = struct.unpack('<L', image_data[0:4])[0]
    width = struct.unpack('<L', image_data[4:8])[0]
    # image data starts after header which is 256 bytes and is LE float32
    image_file.seek(8, os.SEEK_SET)
    actual_data = np.fromfile(image_file, '<f')
    actual_data = np.asarray(actual_data)
    image = (np.reshape(actual_data, (height, width)))

    image_file.close()
    return np.float32(image)




im = importdats('/Users/hanscastorp/Desktop/100049_7.070065E-6_mean.dat')
print(im.shape)
importdats('/Users/hanscastorp/Downloads/crap/Camera 1/cal 3D ARCHIVE/RIGHT/001/ramp1/BG/bg_cam1.dat')
print(im.shape)
