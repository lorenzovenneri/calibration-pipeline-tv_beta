import os
import shutil
import sys
from matplotlib.patches import Rectangle
import numpy as np
import fnmatch
import os
import matplotlib as mpl
import matplotlib.cm as cm
from matplotlib import pyplot as plt
import matplotlib.colors as colors
from mpl_toolkits.mplot3d import Axes3D
import cv2
import time
import math
import scipy
import matplotlib.pyplot as plt
import struct
from PIL import Image
from scipy.interpolate import CubicSpline
from filteringOPT import *
import multiprocessing
from ply_io import *
from scipy import misc
import timeit

def importdats(image_file_LOC):
    image_file = open(image_file_LOC)

    image_data = image_file.read()
    # read dimensions which are LE at 12-15 (height) and 16-19 (width) float32
    height = struct.unpack('<L', image_data[0:4])[0]
    width = struct.unpack('<L', image_data[4:8])[0]
    # image data starts after header which is 256 bytes and is LE float32
    image_file.seek(8, os.SEEK_SET)
    actual_data = np.fromfile(image_file, '<f')
    actual_data = np.asarray(actual_data)
    image = (np.reshape(actual_data, (height, width)))

    image_file.close()
    return image


def importLUT(lut_loc,manypoints):
    lut_file = open(lut_loc)
    lut_data = lut_file.read()
    # HEADER 12 bytes; read dimensions which are LE at 12-15 (height) and 16-19 (width) float32
    heightLUT = struct.unpack('<I', lut_data[0:4])[0]
    widthLUT = struct.unpack('<I', lut_data[4:8])[0]
    if manypoints>0:
        numpoints = struct.unpack('<I', lut_data[8:12])[0]
        lut_file.seek(12, os.SEEK_SET)

    else:
        numpoints = 1
        lut_file.seek(8, os.SEEK_SET)

    # image data starts after header which is 256 bytes and is LE float32
    actual_data = np.fromfile(lut_file, '<f')
    actual_data = np.asarray(actual_data)
    print 'importing LUT: ' + lut_loc + '   with height:width:numpoints - ' \
          + str(heightLUT) + ':' + str(widthLUT) + ':' + str(numpoints) + ' | and total size - ' + str(actual_data.size)
    thisLUT = np.zeros((heightLUT, widthLUT, numpoints))
    start = 0

    for row in range(heightLUT):
        for col in range(widthLUT):
            for i in range(numpoints):
                thisLUT[row, col, i] = actual_data[start]
                start += 1
    lut_file.close()
    print 'complete LUT ', thisLUT.shape
    return thisLUT[:, :, 2:22].squeeze()


def importLUTall(lut_loc,manypoints):
    lut_file = open(lut_loc)
    lut_data = lut_file.read()
    # HEADER 12 bytes; read dimensions which are LE at 12-15 (height) and 16-19 (width) float32
    heightLUT = struct.unpack('<I', lut_data[0:4])[0]
    widthLUT = struct.unpack('<I', lut_data[4:8])[0]
    if manypoints>0:
        numpoints = struct.unpack('<I', lut_data[8:12])[0]
        lut_file.seek(12, os.SEEK_SET)

    else:
        numpoints = 1
        lut_file.seek(8, os.SEEK_SET)

    # image data starts after header which is 256 bytes and is LE float32
    actual_data = np.fromfile(lut_file, '<f')
    actual_data = np.asarray(actual_data)
    print 'importing LUT: ' + lut_loc + '   with height:width:numpoints - ' \
          + str(heightLUT) + ':' + str(widthLUT) + ':' + str(numpoints) + ' | and total size - ' + str(actual_data.size)
    thisLUT = np.zeros((heightLUT, widthLUT, numpoints))
    start = 0

    for row in range(heightLUT):
        for col in range(widthLUT):
            for i in range(numpoints):
                thisLUT[row, col, i] = actual_data[start]
                start += 1
    lut_file.close()
    print 'complete LUT ', thisLUT.shape
    return thisLUT[:, :, :].squeeze()


def exportLUT(lut2save, lut_loc):
    #### HEADER
    height, width, numpoints = lut2save.shape
    # height
    image_file = open(lut_loc, 'wb')
    myfmt = '<I'
    bin = struct.pack(myfmt, height)
    # width
    image_file.write(bin)
    myfmt = '<I'
    bin1 = struct.pack(myfmt, width)
    # number points
    image_file.write(bin1)
    myfmt = '<I'
    bin2 = struct.pack(myfmt, numpoints)
    image_file.write(bin2)

    for row in range(height):
        for col in range(width):
            for i in range(numpoints):
                myfmt = '<f'
                bin3 = struct.pack(myfmt, np.float32(lut2save[row, col, i]))
                image_file.write(bin3)

        image_file.close()
    return

def import_rng(filename):
    image_file = open(filename)
    image_data = image_file.read()
    # read dimensions which are LE at 12-15 (height) and 16-19 (width) float32
    height = struct.unpack('<L', image_data[13:17])[0]
    width = struct.unpack('<L', image_data[17:21])[0]
    # image data starts after header which is 256 bytes and is LE float32
    image_file.seek(256, os.SEEK_SET)
    actual_data = np.fromfile(image_file, '<f')
    # take to numpy array
    actual_data = np.asarray(actual_data)
    image = np.reshape(actual_data, (height, width))
    image_file.close()
    return image


def import_int(filename):
    image_file = open(filename)
    image_data = image_file.read()
    # read dimensions which are LE at 12-15 (height) and 16-19 (width) float32
    height = struct.unpack('<L', image_data[13:17])[0]
    width = struct.unpack('<L', image_data[17:21])[0]
    # image data starts after header which is 256 bytes and is LE float32
    image_file.seek(256, os.SEEK_SET)
    actual_data = np.fromfile(image_file, '<H')  # or <H if actual int data, or <f if .dat
    # take to numpy array
    actual_data = np.asarray(actual_data)
    image = np.reshape(actual_data, (height, width))
    image_file.close()
    return image


def importdats(image_file_LOC):
    image_file = open(image_file_LOC)

    image_data = image_file.read()
    # read dimensions which are LE at 12-15 (height) and 16-19 (width) float32
    height = struct.unpack('<L', image_data[0:4])[0]
    width = struct.unpack('<L', image_data[4:8])[0]
    # image data starts after header which is 256 bytes and is LE float32
    image_file.seek(8, os.SEEK_SET)
    actual_data = np.fromfile(image_file, '<f')
    actual_data = np.asarray(actual_data)
    image = (np.reshape(actual_data, (height, width)))

    image_file.close()
    return image


def import_cams(filename):
    if filename.endswith('c1.bur') or filename.endswith('c2.bur') or filename.endswith('rng.bur'):
        image = import_rng(filename)
    elif filename.endswith('int.bur'):
        image = import_int(filename)
    elif filename.endswith('.dat'):
        image = importdats(filename)
    elif filename.endswith('.tif'):
        image = np.float32(Image.open(filename))
    else:
        print 'Data import failed. Camera datatype not supported.'
    image = np.float32(image)
    return image


def create_dir(folder):
    if not os.path.exists(folder):
        os.makedirs(folder)
        print 'creating directory: ' + folder
    else:
        shutil.rmtree(folder)
        os.makedirs(folder)
        print 'overwriting directory: ' + folder
    return


def register(first_im, second_im):
    c1 = np.flipud(import_cams(first_im))
    c2 = import_cams(second_im)

    im1_gray = c2
    im2_gray = c1

    # Find size of image1
    sz = c1.shape

    # Define the motion model
    warp_mode = cv2.MOTION_EUCLIDEAN  # cv2.MOTION_EUCLIDEAN

    # Define 2x3 or 3x3 matrices and initialize the matrix to identity
    if warp_mode == cv2.MOTION_HOMOGRAPHY:
        warp_matrix0 = np.eye(3, 3, dtype=np.float32)
    else:
        warp_matrix0 = np.eye(2, 3, dtype=np.float32)

    # Specify the number of iterations.
    number_of_iterations = 100;

    # Specify the threshold of the increment
    # in the correlation coefficient between two iterations
    termination_eps = 1e-10;

    # Define termination criteria
    criteria = (cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, number_of_iterations, termination_eps)

    # Run the ECC algorithm. The results are stored in warp_matrix.
    (cc, warp_matrix0) = cv2.findTransformECC(im1_gray, im2_gray, warp_matrix0, warp_mode, criteria)
    print warp_matrix0
    if warp_mode == cv2.MOTION_HOMOGRAPHY:
        # Use warpPerspective for Homography
        c1al = cv2.warpPerspective(c1, warp_matrix0, (sz[1], sz[0]),
                                   flags=cv2.INTER_LINEAR + cv2.WARP_INVERSE_MAP)
    else:
        # Use warpAffine for Translation, Euclidean and Affine
        c1al = cv2.warpAffine(c1, warp_matrix0, (sz[1], sz[0]), flags=cv2.INTER_LINEAR + cv2.WARP_INVERSE_MAP)

    # write the matrix in 4X4 matrix
    warp_matrix = np.zeros((4, 4))
    warp_matrix[3, 3] = 1.0
    warp_matrix[2, 2] = 1.0
    warp_matrix[0, 0] = warp_matrix0[0, 0]
    warp_matrix[0, 1] = warp_matrix0[0, 1]
    warp_matrix[1, 0] = warp_matrix0[1, 0]
    warp_matrix[1, 1] = warp_matrix0[1, 1]
    warp_matrix[3, 0] = -warp_matrix0[0, 2]
    warp_matrix[3, 1] = -warp_matrix0[1, 2]

    if 1 == 0:
        fig = plt.figure(figsize=(10, 6))
        fig.patch.set_facecolor('white')
        plt.subplot(2, 1, 1)
        plt.imshow(c1 + c2)

        plt.subplot(2, 1, 2)
        plt.imshow(c1al + c2)
        plt.show()

    return warp_matrix.transpose()


def savingPLYs(z_coord,a_angle,b_angle,intensity,location):
    print z_coord.shape
    coords  = np.hstack((np.reshape(np.multiply(z_coord, a_angle),(z_coord.size,1)),
                         np.reshape(np.multiply(z_coord, b_angle),(z_coord.size,1)),
                         np.reshape(z_coord,(z_coord.size,1))))
    intensity = np.reshape(scipy.misc.bytescale(intensity, cmin=None, cmax=None, high=255, low=0),(z_coord.size,1)).astype('int8')
    cols = np.hstack((intensity, intensity, intensity))
    plySavePoints(location, coords.astype('float32'),
                  color=cols, ascii=False)
    return


# need a config file
filterRNG = 1
filterC1C2 = 0

# define locations of data assuming substrucutre is respected

data_loc = '/Users/lvenneri/Desktop/development/ArriData/shots'


calibration_loc = '/Users/lvenneri/Desktop/development/ARRI2 (cal to use)/cal_rw0_3'
binning = 0
if binning == 1:
    calibration_loc = calibration_loc+'(3x3)'

# check that ramp is descending, if not, rename c1 and c2

# Registration matrices
# Opencv or imagej


# print 'registering...'
# transform_c1 = register((register_loc + '/c1.bur'), (register_loc + '/c2.bur'))
#
# print transform_c1
# ARRI
transform_c1 = np.asarray([[0.99955039, 0.01172138, 0.0, 0.0], [-0.01251095, 1.00059674, 0.0, 0.0],
                           [0.0, 0.0, 1.0, 0.0], [17.95689912, -96.96577326, 0.0, 1.0]]).transpose()

# ZEBRA07
# transform_c1 = np.asarray([[0.99952161, -0.00541130, 0.0, 0.0], [ 0.00610531, 0.99967819, 0.0, 0.0],
#                            [0.0, 0.0, 1.0, 0.0], [22.76989555, 1.97380638, 0.0, 1.0]]).transpose()

# import LUTs

rampLUT = importLUT(calibration_loc + '/ramp.dat',1)
# rampLUT = np.minimum.accumulate(rampLUT,2) # forces monotonic
rampLUT = rampLUT[:, :, ::-1]
timesLUT = importLUT(calibration_loc + '/times.dat',1)
timesLUT = timesLUT[:, :, ::-1]
aminLUT = importLUTall(calibration_loc + '/amin.dat',0)
amaxLUT = importLUTall(calibration_loc + '/amax.dat',0)
normLUT = importLUTall(calibration_loc + '/normintensity.dat',0)
anglesLUT = importLUTall(calibration_loc + '/angles.dat',0)
a_angleLUT = importLUTall(calibration_loc + '/a_angle.dat',0)
b_angleLUT = importLUTall(calibration_loc + '/b_angle.dat',0)

########################## filtering parameters
kernel_m = 7
kernel_b = 19
iter_m = 1
iter_b = 3
sigma_z0 = .3
bilateral_velocity = .5
x = 0
z = 0
bin_Q = 1
refl = .1
minI = 1  # minimum intensity threshold for valid data
amin = 0.25  # min threshold on the ratio (use fixed here)
amax = .95  # max threshold on the ratio (use fixed here)
###########################

currentTime = []

# the slice are numbered
xs = np.linspace(0.0, float(rampLUT.shape[2] - 1), float(rampLUT.shape[2]))

cpu_count = multiprocessing.cpu_count()
print 'parellized pipeline...?'
print 'CPU headcount: ', cpu_count

data_folders = [f for f in os.listdir(data_loc + '/Framelogs') if not f.startswith('.')]
print 'folders to process: '
print data_folders
for test_pot_loc in data_folders:
    print 'processing: ' + test_pot_loc
    test_pot_loc = data_loc + '/Framelogs' + '/' + test_pot_loc
    # REAL DATA
    # import  the rng and int image
    c1_loc = [f for f in os.listdir(test_pot_loc) if
              (f.endswith('c1.bur') or f.endswith('cam1.dat')) and not f.startswith('b')]
    c2_loc = [f for f in os.listdir(test_pot_loc) if
              (f.endswith('c2.bur') or f.endswith('cam2.dat')) and not f.startswith('b')]
    c1back_loc = [f for f in os.listdir(test_pot_loc) if
                  (f.endswith('c1.bur') or f.endswith('cam1.dat')) and f.startswith('b')]
    c2back_loc = [f for f in os.listdir(test_pot_loc) if
                  (f.endswith('c2.bur') or f.endswith('cam2.dat')) and f.startswith('b')]
    c1b = import_cams(test_pot_loc + '/' + c1back_loc[0])
    c2b = import_cams(test_pot_loc + '/' + c2back_loc[0])
    count = 0.0

    create_dir(test_pot_loc + '/intensity')
    create_dir(test_pot_loc + '/ratio')
    create_dir(test_pot_loc + '/range')
    create_dir(test_pot_loc + '/ply')

    # create_dir(test_pot_loc + '/ratinfront')
    # create_dir(test_pot_loc + '/ratinbehind')

    image_count = 1
    for f in range(len(c1_loc)):
        start = timeit.timeit()
        print 'currently processing: ', test_pot_loc, '  ', np.float32(f) / np.float32(len(c1_loc)) * 100.0, '%'
        c1 = import_cams((test_pot_loc + '/' + c1_loc[f]))
        c2 = import_cams((test_pot_loc + '/' + c2_loc[f]))

        # Subtract background
        c1old = np.flipud(c1 - c1b)
        c2old = c2 - c2b

        # Histogram

        # Banding and scattering

        # Linearity

        # Black-white correction

        # Register images3
        M = transform_c1[0:3, 0:3]
        rows, cols = c1old.shape
        # sz = c1old.shape
        c1_perspold = cv2.warpPerspective(c1old, M, (cols, rows))
        Mtold = np.float32([[1, 0, transform_c1[0, 3]], [0, 1, transform_c1[1, 3]]])
        c1old = cv2.warpAffine(c1_perspold, Mtold, (cols, rows))
        # find ratio and use LUTs

        if c1_loc[f].endswith('.bur'):         # naming structure -- 002690_10138.00_1486696356947_c1
            timing_offset = np.float32(c1_loc[f][7:15])
        elif c1_loc[f].endswith('.dat'):
            timing_offset = np.float32(c1_loc[f][0:9])

        # timing_offset = np.float32(c1_loc[f][0:9])

        # timing_offset = 10100
        # c1old[:,:] = 100.0
        # c2old[:, :] = 100.0


        intensity = c1old + c2old
        ratio = np.divide(c1old, (c1old + c2old))

        range_image = np.zeros((rows, cols))
        ratio_infront = np.zeros((rows, cols))
        ratio_behind= np.zeros((rows, cols))
        # for each pixel, find range
        # can multicore this step (hold LUTs)
        if 1==1:
            spline = 0
            for i in range(rows):
                # multip core?
                # out1, out2, out3 = zip(*pool.map(calc_stuff, range(0, range(cols), 1)))
                for j in range(cols):
                    ## the two LUTs have 24 matched points per pixel
                    ## use ratio in the ramp LUT to get the position over the 24 points

                    # print rampLUT[i, j, :]
                    pixramp = rampLUT[i, j, :]
                    timramp = timesLUT[i, j, :]
                    # deal with registration NANs and non monotonic;
                    # if the registration causes the section to be black, this will be 0 in ramp
                    if pixramp[0] == 0  or c1old[i, j] < minI or \
                                    c2old[i, j] < minI or ratio[i, j] < aminLUT[i,j] or ratio[i, j] > amaxLUT[i,j]: #or not np.all(np.diff(pixramp) > 0):
                        range_image[i, j] = np.nan
                    else:
                        if spline == 1:
                            splinefitRAMP = CubicSpline(pixramp, xs)
                            x_from_ramp = splinefitRAMP(ratio[i, j])

                            ## find the time of flight by interpolating in timesLUT and subtract the offset (in the name) which is
                            ## the opposite of what was done in generating the LUTS
                            splinefitTIMES = CubicSpline(xs, timramp)
                            TOF = timing_offset - splinefitTIMES(x_from_ramp)
                            range_image[i, j] = TOF * 0.299792458 / 2.0

                        else:

                            firstGr8 = np.argmax(pixramp > ratio[i, j])
                            x_from_ramp = (ratio[i, j] - pixramp[firstGr8 - 1]) / (
                            pixramp[firstGr8] - pixramp[firstGr8 - 1])
                            ratio_infront[i,j] =pixramp[firstGr8 - 1]
                            ratio_behind[i,j] =pixramp[firstGr8]

                            TOF = (timing_offset - (
                            (timramp[firstGr8] - timramp[firstGr8 - 1]) * x_from_ramp + timramp[firstGr8 - 1]))

                            range_image[i, j] = TOF * 0.299792458 / 2.0

                            ## have time of flight *c/2 = range

                            # print rampLUT[i, j, :]
                            # print timesLUT[i, j, :]
                            # print ratio[i, j], x_from_ramp, TOF, range_image[i,j]
                            # print timing_offset

                            # print 'new'
                            # print pixramp
                            # print firstGr8, firstGr8 - 1, x_from_ramp
                            # print pixramp[firstGr8], pixramp[firstGr8 - 1], ratio[i, j]
                            # print timramp[firstGr8], timramp[firstGr8 - 1],  (
                            #     (timramp[firstGr8] - timramp[firstGr8 - 1]) * x_from_ramp + timramp[firstGr8 - 1])
                            # print TOF, range_image[i, j]
            count += 1

        # use the lens calibration angle files to correct lens distorion and obtain rectilinear coord
        range_image = np.multiply(range_image, np.cos((anglesLUT)))  # cos((angles)) to take to rectilinear

        if filterRNG == 1:
            print 'filtering range...'
            # nan killer
            # range_image = cv2.resize(range_image, (0, 0), fx=1.0 / 3.0, fy=1.0 / 3.0, interpolation=cv2.INTER_AREA)
            range_image = nan_killer(range_image,3)

            # filter range with bilateral etc
            range_image = med_n_bi_adapt(range_image, kernel_m, kernel_b, iter_m, iter_b, sigma_z0, bilateral_velocity, x,
                                         z, bin_Q, refl, c1_loc)




        # saving tifs
        print 'saving tifs...'
        im = Image.fromarray(intensity)
        im.save((test_pot_loc + '/intensity/' + c1_loc[f] + 'intensity.tif'))
        im = Image.fromarray(ratio)
        im.save((test_pot_loc + '/ratio/' + c1_loc[f] + 'ratio.tif'))
        im = Image.fromarray(range_image)
        im.save((test_pot_loc + '/range/' + c1_loc[f] + 'range.tif'))

        # ratio_infront[ratio_infront[:,:]==0] = np.nan
        # im = Image.fromarray(c1old)
        # im.save((test_pot_loc + '/ratinfront/' + c1_loc[f] + 'rat.tif'))
        # ratio_behind[ratio_behind[:,:]==0] = np.nan
        # im = Image.fromarray(c2old)
        # im.save((test_pot_loc + '/ratinbehind/' + c1_loc[f] + 'rat.tif'))


        # save PLYs
        print 'saving plys'
        savingPLYs(range_image, a_angleLUT, b_angleLUT, intensity, test_pot_loc + '/ply'+'/'+str(image_count).zfill(6)
            +'.ply')

        image_count +=1

        end = timeit.timeit()
        currentTime.append(end - start)

        print 'average processing time per image ', np.mean(currentTime)

        if 1 == 0:
            fig = plt.figure(figsize=(10, 6))
            fig.patch.set_facecolor('white')
            plt.subplot(1, 1, 1)
            plt.imshow(range_image)
            plt.clim([0, 15])

        plt.show()
