import os
import shutil
import sys
from matplotlib.patches import Rectangle
import numpy as np
import fnmatch
import os
import matplotlib as mpl
import matplotlib.cm as cm
from matplotlib import pyplot as plt
import matplotlib.colors as colors
from mpl_toolkits.mplot3d import Axes3D
import cv2
import time
import math
import matplotlib.pyplot as plt
import struct
from multiprocessing import Pool
import subprocess
import gc
import multiprocessing
from PIL import Image

def importdats(image_file_LOC):
    image_file = open(image_file_LOC)

    image_data = image_file.read()
    # read dimensions which are LE at 12-15 (height) and 16-19 (width) float32
    height = struct.unpack('<L', image_data[0:4])[0]
    width = struct.unpack('<L', image_data[4:8])[0]
    # image data starts after header which is 256 bytes and is LE float32
    image_file.seek(8, os.SEEK_SET)
    actual_data = np.fromfile(image_file, '<f')
    actual_data = np.asarray(actual_data)
    image = (np.reshape(actual_data, (height, width)))

    image_file.close()
    return image

def importLUTall(lut_loc,manypoints):
    lut_file1 = open(lut_loc)
    lut_data = lut_file1.read()
    # HEADER 12 bytes; read dimensions which are LE at 12-15 (height) and 16-19 (width) float32
    heightLUT = struct.unpack('<I', lut_data[0:4])[0]
    widthLUT = struct.unpack('<I', lut_data[4:8])[0]
    if manypoints>0:
        numpoints = struct.unpack('<I', lut_data[8:12])[0]
        lut_file1.seek(12, os.SEEK_SET)
        # image data starts after header which is 256 bytes and is LE float32
        actual_data = np.fromfile(lut_file1, '<f')

        actual_data = np.asarray(actual_data)
        print 'importing LUT: ' + lut_loc + '   with height:width:numpoints - ' \
              + str(heightLUT) + ':' + str(widthLUT) + ':' + str(numpoints) + ' | and total size - ' + str(
            actual_data.size)
        thisLUT = np.zeros((heightLUT, widthLUT, numpoints))
        start = 0
        for row in range(heightLUT):
            for col in range(widthLUT):
                for i in range(numpoints):
                    thisLUT[row, col, i] = actual_data[start]
                    start += 1
        thisLUT =thisLUT[:,:,:].squeeze()
    else:
        numpoints = 1
        lut_file1.seek(8, os.SEEK_SET)
        actual_data = np.fromfile(lut_file1, '<f')
        actual_data = np.asarray(actual_data)
        thisLUT = (np.reshape(actual_data, (heightLUT, widthLUT)))
        print thisLUT

    lut_file1.close()
    print 'complete LUT ', thisLUT.shape

    return thisLUT

def importXFORMS(lut_loc):
    lut_file = open(lut_loc)

    # HEADER 12 bytes; read dimensions which are LE at 12-15 (height) and 16-19 (width) float32
    thisLUT = np.zeros((4,4))
    actual_data = np.fromfile(lut_file, '<f')
    actual_data = np.asarray(actual_data)
    start = 0
    for row in range(4):
        for col in range(4):
            thisLUT[row, col] = actual_data[start]
            start += 1
    lut_file.close()
    return thisLUT[:, :].squeeze()

def exportXFORMS(lut2save, lut_loc):
    #### HEADER
    lut2save = lut2save.transpose()
    dims = lut2save.shape
    # height
    image_file = open(lut_loc, 'wb')
    for row in range(dims[0]):
        for col in range(dims[1]):

                myfmt = '<f'
                bin3 = struct.pack(myfmt, np.float32(lut2save[row, col]))
                image_file.write(bin3)

    image_file.close()
    return

def exportLUT(lut2save, lut_loc):
    #### HEADER
    dims = lut2save.shape
    # height
    image_file = open(lut_loc, 'wb')
    myfmt = '<I'
    bin = struct.pack(myfmt, dims[0])
    # width
    image_file.write(bin)
    myfmt = '<I'
    bin1 = struct.pack(myfmt, dims[1])
    image_file.write(bin1)
    # number points
    if len(dims) > 2:
        myfmt = '<I'
        bin2 = struct.pack(myfmt, dims[2])
        image_file.write(bin2)
    for row in range(dims[0]):
        for col in range(dims[1]):
            if len(dims) >2:
                for i in range(dims[2]):
                    myfmt = '<f'
                    bin3 = struct.pack(myfmt, np.float32(lut2save[row, col, i]))
                    image_file.write(bin3)
            else:
                myfmt = '<f'
                bin3 = struct.pack(myfmt, np.float32(lut2save[row, col]))
                image_file.write(bin3)
    image_file.close()
    return

def export_rng(filename, image):
    image_file = open(filename,'wb')
    image_data = np.float32(np.reshape(image,(image.size)))
    dims = np.asarray(image.shape)
    header = np.zeros(13)
    myfmt = 'x' * len(header)
    bin = struct.pack(myfmt)
    image_file.write(bin)

    dims1 = dims[0]
    dims2 = dims[1]

    myfmt = '=L'
    bin = struct.pack(myfmt,dims1)
    image_file.write(bin)
    myfmt = '=L'
    bin2 = struct.pack(myfmt,dims2)
    image_file.write(bin2)

    header = np.zeros(256-13-8)
    myfmt = 'x' * len(header)
    bin = struct.pack(myfmt)
    image_file.write(bin)

    # header = np.zeros(3)
    # myfmt = 'b' * len(header)
    # bin = struct.pack(myfmt, *header)
    # image_file.write(bin)

    myfmt = 'f' * len(image_data)
    bin = struct.pack(myfmt, *image_data)
    image_file.write(bin)
    image_file.close()

    return image

def writeCMUdats(int_image,inputfilename):
    # CREATE INT FOR CMU PLANEFIT
    int_image = np.rot90(int_image,-1)
    int_image = int_image.astype('>f')
    h, w = int_image.shape
    image_file = open(inputfilename, 'wb')
    image_file.write(struct.pack('>i4', h))
    image_file.write(struct.pack('>i4', w))
    int_image.tofile(image_file, '', '>f')
    image_file.close()
    return

def importCMUtxt(location):
    data = np.asarray(
        np.loadtxt((location), delimiter=' '))
    return data

def import_rng(filename):
    image_file = open(filename)
    image_data = image_file.read()
    # read dimensions which are LE at 12-15 (height) and 16-19 (width) float32
    height = struct.unpack('<L', image_data[13:17])[0]
    width = struct.unpack('<L', image_data[17:21])[0]
    # image data starts after header which is 256 bytes and is LE float32
    image_file.seek(256, os.SEEK_SET)
    actual_data = np.fromfile(image_file, '<f')
    # take to numpy array
    actual_data = np.asarray(actual_data)
    image = np.reshape(actual_data, (height, width))
    image_file.close()
    return image

def import_int(filename):
    image_file = open(filename)
    image_data = image_file.read()
    # read dimensions which are LE at 12-15 (height) and 16-19 (width) float32
    height = struct.unpack('<L', image_data[13:17])[0]
    width = struct.unpack('<L', image_data[17:21])[0]
    # image data starts after header which is 256 bytes and is LE float32
    image_file.seek(256, os.SEEK_SET)
    actual_data = np.fromfile(image_file, '<H')  # or <H if actual int data, or <f if .dat
    # take to numpy array
    actual_data = np.asarray(actual_data)
    image = np.reshape(actual_data, (height, width))
    image_file.close()
    return image

def import_cams(filename):
    image1 = []
    if filename.endswith('c1.bur') or filename.endswith('c2.bur') or filename.endswith('rng.bur'):
        image1 = import_rng(filename)
    elif filename.endswith('int.bur'):
        image1 = import_int(filename)
    elif filename.endswith('.dat'):
        image1 = importdats(filename)
    elif filename.endswith('.tif'):
        image1 = np.float32(Image.open(filename))
    else:
        print 'ERROR: data import failed datatype not supported at: ',filename
        sys.exit()
    dims = image1.shape
    # force crop the image to be divisible by three.
    image1 = image1[0:dims[0]-dims[0]%3, 0:dims[1]-dims[1]%3]

    return np.float32(image1)

def create_dir(folder):
    if not os.path.exists(folder):
        os.makedirs(folder)
        print 'creating directory: ' + folder
    else:
        shutil.rmtree(folder)
        os.makedirs(folder)
        print 'overwriting directory: ' + folder
    return


def register(first_im, second_im, saveloc):
    c1 = np.flipud(import_cams(first_im))
    c2 = import_cams(second_im)

    im1_gray = c2
    im2_gray = c1

    # Find size of image1
    sz = c1.shape

    # Define the motion model
    warp_mode = cv2.MOTION_EUCLIDEAN  # cv2.MOTION_EUCLIDEAN

    # Define 2x3 or 3x3 matrices and initialize the matrix to identity
    if warp_mode == cv2.MOTION_HOMOGRAPHY:
        warp_matrix0 = np.eye(3, 3, dtype=np.float32)
    else:
        warp_matrix0 = np.eye(2, 3, dtype=np.float32)

    # Specify the number of iterations.
    number_of_iterations = 1000;

    # Specify the threshold of the increment
    # in the correlation coefficient between two iterations
    termination_eps = 1e-10;

    # Define termination criteria
    criteria = (cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, number_of_iterations, termination_eps)

    # Run the ECC algorithm. The results are stored in warp_matrix.
    (cc, warp_matrix0) = cv2.findTransformECC(im1_gray, im2_gray, warp_matrix0, warp_mode, criteria)
    print warp_matrix0
    if warp_mode == cv2.MOTION_HOMOGRAPHY:
        # Use warpPerspective for Homography
        c1al = cv2.warpPerspective(c1, warp_matrix0, (sz[1], sz[0]),
                                   flags=cv2.INTER_LINEAR + cv2.WARP_INVERSE_MAP)
    else:
        # Use warpAffine for Translation, Euclidean and Affine
        c1al = cv2.warpAffine(c1, warp_matrix0, (sz[1], sz[0]), flags=cv2.INTER_LINEAR + cv2.WARP_INVERSE_MAP)

    # write the matrix in 4X4 matrix
    warp_matrix = np.zeros((4, 4))
    warp_matrix[3, 3] = 1.0
    warp_matrix[2, 2] = 1.0
    warp_matrix[0, 0] = warp_matrix0[0, 0]
    warp_matrix[0, 1] = warp_matrix0[0, 1]
    warp_matrix[1, 0] = warp_matrix0[1, 0]
    warp_matrix[1, 1] = warp_matrix0[1, 1]
    warp_matrix[3, 0] = -warp_matrix0[0, 2]
    warp_matrix[3, 1] = -warp_matrix0[1, 2]

    if 1 == 0:
        fig = plt.figure(figsize=(10, 6))
        fig.patch.set_facecolor('white')
        plt.subplot(2, 1, 1)
        plt.imshow(np.divide(c1,c1 + c2),interpolation='none')

        plt.subplot(2, 1, 2)
        plt.imshow(c1+c2,interpolation='none')
        plt.savefig(saveloc)

    return warp_matrix.transpose()

def registerCheck(first_im, second_im, saveloc,transform_c1):
    c1 = np.flipud(import_cams(first_im))
    c1old = c1
    c2 = import_cams(second_im)
    M = transform_c1[0:3, 0:3]
    rows, cols = c1old.shape
    c1_perspold = cv2.warpPerspective(c1old, M, (cols, rows))
    Mtold = np.float32([[1, 0, transform_c1[0, 3]], [0, 1, transform_c1[1, 3]]])
    c1old = cv2.warpAffine(c1_perspold, Mtold, (cols, rows))
    if 1 == 1:
        imm = np.zeros((c1.shape[0],c1.shape[1],3))
        imm[:,:,0] = c1/np.max(c1)
        imm[:,:,1] = c2/np.max(c2)
        fig = plt.figure(figsize=(10, 6))
        fig.patch.set_facecolor('white')
        plt.subplot(2, 1, 1)
        plt.imshow(c1 + c2,interpolation='none')
        plt.title('Unregistered')
        plt.colorbar()

        plt.subplot(2, 1, 2)
        imm[:,:,0] = c1old/np.max(c1old)

        plt.imshow(c1old+c2,interpolation='none')
        plt.colorbar()
        plt.title('Registered')
        plt.savefig(saveloc)

    return


def image_snatcher(f, c1_loc, c2_loc,test_pot_loc,transform_c1,c1b,c2b,register_ramp,binning):
    print 'working... ', int(np.float32(f) / np.float32(len(c1_loc)) * 100.0), '%', c1_loc[f]
    c1 = import_cams((test_pot_loc + '/' + c1_loc[f]))
    c2 = import_cams((test_pot_loc + '/' + c2_loc[f]))

    if 1 == 0:  # saves bur files of the ramp for quick checks
        export_rng((storeRamp_loc + '/burs' + '/' + c1_loc[f] + '-c1.bur'), c1)
        export_rng((storeRamp_loc + '/burs' + '/' + c2_loc[f] + '-c2.bur'), c2)

    if register_ramp == 1:

        # Subtract background
        c1old = np.flipud(c1 - c1b)
        c2old = c2 - c2b

        # Histogram

        # Banding and scattering

        # Linearity

        # Black-white correction

        # Register images3
        M = transform_c1[0:3, 0:3]
        rows, cols = c1old.shape
        c1_perspold = cv2.warpPerspective(c1old, M, (cols, rows))
        Mtold = np.float32([[1, 0, transform_c1[0, 3]], [0, 1, transform_c1[1, 3]]])
        c1old = cv2.warpAffine(c1_perspold, Mtold, (cols, rows))
        if c1_loc[f].endswith('.bur'):  # naming structure -- 002690_10138.00_1486696356947_c1
            timing_offset = np.float32(c1_loc[f][7:15])
        elif c1_loc[f].endswith('.dat'):
            timing_offset = np.float32(c1_loc[f][0:9])
        else:
            print 'ERROR: incompatible file naming for ramps. offset timing must be as defined by the bur or dat format'
            sys.exit()
    else:
        c1old = c1
        c2old = c2
        timing_offset = np.float32(c1_loc[f][0:9])

    c1old = cv2.resize(c1old, (0, 0), fx=1.0 / binning, fy=1.0 / binning, interpolation=cv2.INTER_AREA)
    c2old = cv2.resize(c2old, (0, 0), fx=1.0 / binning, fy=1.0 / binning, interpolation=cv2.INTER_AREA)
    return c1old,c2old,timing_offset

def mainprog(binning,cal_out_loc, lensCalOut_loc, CMU_files_loc, main_loc, ramp_loc, ramp_folders, BG_loc, lensCal_loc,
             wallRange_loc, register_loc,wall_loc):
    if not os.path.exists(BG_loc) or not os.path.exists(ramp_loc + '/' + ramp_folders[0]):
        print 'ERROR: missing ramp data in ' + BG_loc
        sys.exit()
    if not os.path.exists(wall_loc):
        print 'ERROR: missing ramp data in ' + wall_loc
        sys.exit()
    if not os.path.exists(lensCal_loc):
        print 'ERROR: missing lens calibration data in ' + lensCal_loc
        sys.exit()

    # create cal out folder for pipeline, if one already exists --> create another
    if not os.path.exists(cal_out_loc):
        create_dir(cal_out_loc)
        print 'creating calibration output directory for pipeline: ', cal_out_loc
    else:
        num = 1
        cal_out_loc0 = cal_out_loc
        while os.path.exists(cal_out_loc):
            cal_out_loc = cal_out_loc0 + str(num)
            num += 1
        create_dir(cal_out_loc)
        print 'WARNING: calibration output directory exists, creating new folder', cal_out_loc

    # -----------------------------------------------------------------------------------------------------------------------
    # DESCENDING RAMP
    # check that ramp is descending, if not, indicate error



    # -----------------------------------------------------------------------------------------------------------------------
    # REGISTRATION
    regfiles = [f for f in os.listdir(register_loc) if
                not f.startswith('.') and (f.startswith('c') or f.startswith('C'))]
    if (len(regfiles) < 2):
        print 'ERROR: Missing registration data.'
        sys.exit()
    elif os.path.exists((register_loc + "/xforms.dat")):
        print 'using previously computed registration in: ', (register_loc + "/xforms.dat")
        transform_c1 = importXFORMS((register_loc + "/xforms.dat")).transpose()
    else:
        print 'computing registration --------------------------------------------------------------------------------------'
        imageC1 = [f for f in os.listdir(register_loc) if
                   not f.startswith('.') and (f.find('c1') > -1 or f.find('C1') > -1)]
        imageC2 = [f for f in os.listdir(register_loc) if
                   not f.startswith('.') and (f.find('c2') > -1 or f.find('C2') > -1)]
        print imageC1, imageC2
        transform_c1 = register((register_loc + '/' + imageC1[0]), (register_loc + '/' + imageC2[0]),
                                (register_loc + '/scene.tif'))
        print 'done.'
        # write to a text file
        with open((register_loc + "/registration.txt"), "w") as text_file:
            text_file.write("Transformation Matrix: {0}".format(transform_c1))
        # write to xforms.dat
        exportXFORMS(transform_c1, (register_loc + "/xforms.dat"))
    imageC1 = [f for f in os.listdir(register_loc) if
                   not f.startswith('.') and (f.find('c1') > -1 or f.find('C1') > -1)]
    imageC2 = [f for f in os.listdir(register_loc) if
           not f.startswith('.') and (f.find('c2') > -1 or f.find('C2') > -1)]

    registerCheck((register_loc + '/' + imageC1[0]), (register_loc + '/' + imageC2[0]),
                            (register_loc + '/scene.tif'),transform_c1)

    print 'registration matrix:'
    print transform_c1

    # -----------------------------------------------------------------------------------------------------------------------
    # LENS cal
    # running CMU scripts (at each step, check if step already done, and write a status report for user
    holder2 = [f for f in os.listdir(lensCal_loc) if not f.startswith('.')]

    if os.path.exists(lensCalOut_loc) == 0:
        print 'no prior lens calibration'
        create_dir(lensCalOut_loc)


    holder = [f for f in os.listdir(lensCalOut_loc) if not f.startswith('.')]

    if len(holder2) > 20:

        if len(holder) < 12:
            print 'computing CMU lens calibration -------------------------------------------------------------------------'

            # find lenscal data, and put in the right format for CMU
            filesLENS = [f for f in os.listdir(lensCal_loc) if
                         not f.startswith('.') and (f.endswith('.dat') or f.endswith('.bur'))]
            cmuformatfolder = lensCal_loc + '/CMU_format'+'_(binX'+str(binning)+')'
            create_dir(cmuformatfolder)
            for f in filesLENS:
                image22 = import_cams(lensCal_loc + '/' + f)
                image22 = cv2.resize(image22, (0, 0), fx=1.0 / binning, fy=1.0 / binning, interpolation=cv2.INTER_AREA)
                writeCMUdats(image22, cmuformatfolder + '/' + f)

            doitLENScal = ('C:\Python27\python.exe "' + str(CMU_files_loc + '\opencv_calibrate.py') +
                           '" -f  "' + str(cmuformatfolder.replace('/', '\\') + '\*.dat') +
                           '" -c "' + str(lensCalOut_loc.replace('/', '\\')) + '" -s 75.35 -p 7 4 -wc')

            print doitLENScal
            # angles
            doitANGLES = ('C:\Python27\python.exe "' + str(CMU_files_loc + '\generate_tetravue_angles.py') +
                          '" -c "' + str(lensCalOut_loc.replace('/', '\\')))

            subprocess.call(doitLENScal)
            print 'lens cal done.'

            print 'computing angle files'

            subprocess.call(doitANGLES)

            print 'angle files done. '




        else:

            print 'WARNING: lens calibration previously computed in ' + lensCalOut_loc
            print 'remove directory to redo a lens calibration'
            print 'proceeding with current lens cal'
        a_angle = importCMUtxt(lensCalOut_loc + '/angle_maps/a_angle_image.dat.gz')
        b_angle = importCMUtxt(lensCalOut_loc + '/angle_maps/b_angle_image.dat.gz')

        print 'writing angle LUTs...'
        angles = np.power(np.power(a_angle, 2) + np.power(b_angle, 2), .5)
        # convert LUTs to a mode Arjun can use
        exportLUT(angles, cal_out_loc + '/angles.dat')
        exportLUT(a_angle, cal_out_loc + '/a_angle.dat')
        exportLUT(b_angle, cal_out_loc + '/b_angle.dat')
    else:
        print 'ERROR: insufficient or missing lens calibration data in ' + lensCal_loc
        sys.exit()

    # -----------------------------------------------------------------------------------------------------------------------
    # Wall
    holder = [f for f in os.listdir(wall_loc) if not f.startswith('.') and (f.endswith('.dat') or f.endswith('.bur'))]
    if len(holder) >= 4:
        print 'computing... CMU wall range ---------------------------------------------------------------------------------'
        if 1 == 1:
            # find wall data, and put in the right format for CMU
            # find lenscal data, and put in the right format for CMU
            filesWALL = [f for f in os.listdir(wall_loc) if
                         not f.startswith('.') and (f.endswith('2.dat') or f.endswith('2.bur'))]

            cmuformatfolder = wall_loc + '/CMU_format'+'_(binX'+str(binning)+')'
            create_dir(cmuformatfolder)

            count = 0
            for f in filesWALL:
                image22 = import_cams(wall_loc + '/' + f)
                image22 = cv2.resize(image22, (0, 0), fx=1.0 / binning, fy=1.0 / binning, interpolation=cv2.INTER_AREA)
                writeCMUdats(image22, cmuformatfolder + '/' + f + 'int.dat')
            # wall
            doitWALL = ('C:\Python27\python.exe "' + str(CMU_files_loc + '\generate_flat_plane.py') +
                        '" -c "' + str(lensCalOut_loc.replace('/', '\\')) +
                        '" -f  "' + str(cmuformatfolder.replace('/', '\\')) + '"')
            print doitWALL
            subprocess.call(doitWALL)

        holder = [f for f in os.listdir(lensCalOut_loc + '/wall') if
                  not f.startswith('.') and (f.endswith('int')) and f.find('cam2') > -1]
        range_image_CMU_loc = lensCalOut_loc + '/wall/' + holder[0] + '/range_image.dat.gz'
        # unzip the range file
        # convert files to a mode Arjun can use and save
        range_image = importCMUtxt(range_image_CMU_loc)

        exportLUT(range_image, cal_out_loc + '/range_image.dat')

        print 'wall range done'
    else:
        print 'ERROR - Missing wall data. (Need at least 2 wall images for c2).'
        sys.exit()

    # -----------------------------------------------------------------------------------------------------------------------
    # in here would be where to generate LUTs for linearity if you have that kind of data




    # -----------------------------------------------------------------------------------------------------------------------
    # IMPORT DATA for LUTs import the ramp data
    print 'computing...RAMPs ---------------------------------------------------------------------------------'
    # grab wall data
    range_imageCMU = importLUTall(cal_out_loc + '/range_image.dat', 0) / 1000.0
    range_imageCMU = range_image/1000.0
    # repeat for each ramp

    register_ramp = 1
    for rampnum in ramp_folders:
        storeRamp_loc = cal_out_loc + '/' + rampnum
        create_dir(storeRamp_loc)
        create_dir(storeRamp_loc+ '/burs')

        print 'Importing data for ramp: ', rampnum

        # LUT generation (for non binned and binned, and for each ramp) (maybe some medians here and there)
        # Raw image processing and corrections
        # import images, should be able to hold all of them in memory
        test_pot_loc = ramp_loc + '/' + rampnum

        # REAL DATA
        # import  the rng and int image
        c1_loc = [f for f in os.listdir(test_pot_loc) if
                  (f.endswith('c1.bur') or f.endswith('cam1.dat')) and not f.startswith('b')]
        c2_loc = [f for f in os.listdir(test_pot_loc) if
                  (f.endswith('c2.bur') or f.endswith('cam2.dat')) and not f.startswith('b')]
        c1back_loc = [f for f in os.listdir(BG_loc) if
                      (f.endswith('c1.bur') or f.endswith('cam1.dat')) and f.startswith('b')]
        c2back_loc = [f for f in os.listdir(BG_loc) if
                      (f.endswith('c2.bur') or f.endswith('cam2.dat')) and f.startswith('b')]
        c1b = import_cams(BG_loc + '/' + c1back_loc[0])
        c2b = import_cams(BG_loc + '/' + c2back_loc[0])
        export_rng((storeRamp_loc + '/burs' + '/' + 'backgroundc1.bur'), c1b)
        export_rng((storeRamp_loc + '/burs' + '/' + 'backgroundc2.bur'), c2b)
        gc.collect()
        print c1b.shape

        binnedc1b = cv2.resize(c1b, (0, 0), fx=1.0 / binning, fy=1.0 / binning, interpolation=cv2.INTER_AREA)
        print binnedc1b.shape
        print c1b.shape
        ratios = np.zeros((binnedc1b.shape[0], binnedc1b.shape[1], len(c1_loc)))
        timesLUT = np.zeros((binnedc1b.shape[0], binnedc1b.shape[1], len(c1_loc)))
        print ratios.shape
        offsets = np.zeros(len(c1_loc))
        intensity_mean = np.zeros((len(c1_loc), 2))
        ratios_mean = np.zeros((len(c1_loc), 2))

        pool = Pool(processes=3)
        result_list = []

        for f in range(len(c1_loc)): # TODO parallelize this
            result_list.append(pool.apply_async(image_snatcher, (f, c1_loc, c2_loc,test_pot_loc,transform_c1,c1b,c2b,register_ramp,binning)))

        for ijk, res in enumerate(result_list):
            rtn = res.get(timeout=None)
            c1old = rtn[0]
            c2old = rtn[1]
            timing_offset = rtn[2]

            ratios[:, :, ijk] = np.divide(c1old, c1old + c2old)
            ratios_mean[ijk, 0] = np.mean(ratios[:, :, ijk])
            intensity_mean[ijk, 0] = np.mean(c1old + c2old)
            ratios_mean[ijk, 1] = np.std(ratios[:, :, ijk])
            intensity_mean[ijk, 1] = np.std(c1old + c2old)
            # grab offsets from name


            offsets[ijk] = timing_offset
            # Multiply rangefile by speed of light and 2 = timeR
            TOF = range_imageCMU / 0.299792458 * 2.0

            timesLUT[:, :, ijk] = timing_offset - TOF


        print 'import finished'

        print 'writing ramp and timing LUTs for ramp: ' + rampnum

        # -----------------------------------------------------------------------------------------------------------------------
        # Sort the LUTs according to offset number - increasing offset
        sortedOFF_ind = offsets.argsort() # sort the offset and get indices
        ratios = ratios[:,:,sortedOFF_ind[:]]
        timesLUT = timesLUT[:,:,sortedOFF_ind[:]]
        intensity_mean = intensity_mean[sortedOFF_ind[:]]
        ratios_mean = ratios_mean[sortedOFF_ind[:]]
        offsets = offsets[sortedOFF_ind[:]]


        # Min and max of the ratio for each pixel to amin and amax (use this to force monotonic)
        tempcompare = np.zeros((ratios[:, :, 0].shape[0], ratios[:, :, 0].shape[1], 2))
        tempcompare[:, :, 0] = np.min(ratios, 2)
        tempcompare[:, :, 1] = ratios[:, :, 0]
        amax = np.max(tempcompare, 2)
        tempcompare[:, :, 0] = np.max(ratios, 2)
        tempcompare[:, :, 1] = ratios[:, :, ratios.shape[2] - 1]
        amin = np.min(tempcompare, 2)
        # -----------------------------------------------------------------------------------------------------------------------
        # WRITE LUTS
        # ramp.dat is just the ratio at each offset value (ramp values*(N*M)) pixels
        exportLUT(ratios, storeRamp_loc + '/ramp.dat')
        exportLUT(timesLUT, storeRamp_loc + '/times.dat')
        exportLUT(amax, storeRamp_loc + '/amax.dat')
        exportLUT(amin, storeRamp_loc + '/amin.dat')

        # timeR - offsetvalue  times.dat are the time of flight values  * (N*M)

        # Norm intensity is M*N and is image/Max(image)


        # save some figures
        # ramps at points, some averages, etc
        # times
        if 1 == 1:
            outputplot_loc = storeRamp_loc + '/output_plots'
            create_dir(outputplot_loc)
            fig = plt.figure(figsize=(21, 7))
            fig.patch.set_facecolor('white')
            plt.subplot(131)
            plt.imshow(ratios[:, :, int(ratios.shape[2] * .25)])
            plt.title('slice .25')
            plt.clim([0, 1])
            plt.colorbar()
            plt.subplot(133)
            plt.imshow(ratios[:, :, int(ratios.shape[2] * .75)])
            plt.colorbar()
            plt.title('slice .75')
            plt.clim([0, 1])
            plt.subplot(132)
            plt.imshow(ratios[:, :, int(ratios.shape[2] * .5)])
            plt.colorbar()
            plt.clim([0, 1])
            plt.title('mid ramp (.5)')
            plt.savefig(outputplot_loc + '/ramp_slices.png')
            plt.clf()
            fig = plt.figure(figsize=(21, 7))
            fig.patch.set_facecolor('white')
            plt.subplot(131)
            plt.imshow(timesLUT[:, :, int(timesLUT.shape[2] * .25)])
            plt.colorbar()
            plt.title('slice .25')
            plt.subplot(133)
            plt.imshow(timesLUT[:, :, int(timesLUT.shape[2] * .75)])
            plt.colorbar()
            plt.title('slice .75')
            plt.subplot(132)
            plt.imshow(timesLUT[:, :, int(timesLUT.shape[2] * .5)])
            plt.colorbar()
            plt.title('slice .5')
            plt.savefig(outputplot_loc + '/time_slices.png')
            plt.clf()

            fig = plt.figure(figsize=(7, 7))
            fig.patch.set_facecolor('white')
            plt.imshow(amin)
            plt.title('amin')
            plt.clim([0, 1])
            plt.colorbar()
            plt.savefig(outputplot_loc + '/amin.png')
            plt.clf()
            fig = plt.figure(figsize=(7, 7))
            fig.patch.set_facecolor('white')
            plt.imshow(amax)
            plt.clim([0, 1])
            plt.colorbar()
            plt.title('amax')
            plt.savefig(outputplot_loc + '/amax.png')
            plt.clf()
            fig = plt.figure(figsize=(7, 7))
            fig.patch.set_facecolor('white')
            plt.imshow(range_imageCMU)
            plt.colorbar()
            plt.title('range_imageCMU')
            plt.savefig(outputplot_loc + '/range_imageCMU.png')
            plt.clf()
            fig = plt.figure(figsize=(5, 5))
            fig.patch.set_facecolor('white')
            plt.plot(offsets, ratios_mean[:, 0],
                     color='k')
            plt.title('ratio (mean)')
            plt.savefig(outputplot_loc + '/offset_vs_ratioPIX.png')
            plt.clf()
            fig = plt.figure(figsize=(7, 7))
            fig.patch.set_facecolor('white')
            plt.plot(offsets, intensity_mean[:, 0],
                     color='k')
            plt.title('intensity (mean)')
            plt.savefig(outputplot_loc + '/offset_vs_intensityPIX.png')
            plt.clf()
            # fig = plt.figure(figsize=(7, 7))
            # fig.patch.set_facecolor('white')
            # plt.imshow(angles)
            # plt.colorbar()
            # plt.title('angles')
            # plt.savefig(outputplot_loc + '/angles.png')
            # norm intensity

            # registered imagers

            # wall z-line output from pipeline on self-same data

    return
