import numpy as np
from ply_io import *
import scipy
from scipy import misc


def savingPLYs(z_coord,a_angle,b_angle,intensity,location):

    coords  = np.hstack((np.reshape(np.random.rand(100,100),(z_coord.size,1)),
                         np.reshape(np.random.rand(100,100),(z_coord.size,1)),
                         np.reshape(z_coord,(z_coord.size,1))))
    intensity = np.reshape(scipy.misc.bytescale(intensity, cmin=None, cmax=None, high=255, low=0),(z_coord.size,1)).astype('int8')
    cols = np.hstack((intensity, intensity, intensity))
    plySavePoints(location, coords.astype('float32'),
                  color=cols, ascii=False)
    return



z_coord = np.random.rand(100,100)
a_angle = np.random.rand(100,100)
b_angle = np.random.rand(100,100)
intensity = np.random.rand(100,100)*1000
location = '/Users/lvenneri/Desktop/float32.ply'
savingPLYs(z_coord,a_angle,b_angle,intensity,location)