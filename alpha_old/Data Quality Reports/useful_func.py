import os
import shutil
import sys
from matplotlib.patches import Rectangle
import numpy as np
import fnmatch
import os
import matplotlib as mpl
import matplotlib.cm as cm
from matplotlib import pyplot as plt
import matplotlib.colors as colors
from mpl_toolkits.mplot3d import Axes3D
import cv2
import time
import math
import scipy
import matplotlib.pyplot as plt
import struct
from PIL import Image
from scipy.interpolate import CubicSpline
from filteringOPT import *
import multiprocessing
from ply_io import *
from scipy import misc
import time

def export_rng(filename, image):
    image_file = open(filename,'wb')
    image_data = np.float32(np.reshape(image,(image.size)))
    dims = np.asarray(image.shape)
    header = np.zeros(13)
    myfmt = 'x' * len(header)
    bin = struct.pack(myfmt)
    image_file.write(bin)

    dims1 = dims[0]
    dims2 = dims[1]

    myfmt = '=L'
    bin = struct.pack(myfmt,dims1)
    image_file.write(bin)
    myfmt = '=L'
    bin2 = struct.pack(myfmt,dims2)
    image_file.write(bin2)

    header = np.zeros(256-13-8)
    myfmt = 'x' * len(header)
    bin = struct.pack(myfmt)
    image_file.write(bin)

    # header = np.zeros(3)
    # myfmt = 'b' * len(header)
    # bin = struct.pack(myfmt, *header)
    # image_file.write(bin)

    myfmt = 'f' * len(image_data)
    bin = struct.pack(myfmt, *image_data)
    image_file.write(bin)
    image_file.close()

    return image

def importdats(image_file_LOC):
    image_file = open(image_file_LOC)

    image_data = image_file.read()
    # read dimensions which are LE at 12-15 (height) and 16-19 (width) float32
    height = struct.unpack('<L', image_data[0:4])[0]
    width = struct.unpack('<L', image_data[4:8])[0]
    # image data starts after header which is 256 bytes and is LE float32
    image_file.seek(8, os.SEEK_SET)
    actual_data = np.fromfile(image_file, '<f')
    actual_data = np.asarray(actual_data)
    image = (np.reshape(actual_data, (height, width)))

    image_file.close()
    return image


def importLUT(lut_loc,manypoints):
    lut_file = open(lut_loc)
    lut_data = lut_file.read()
    # HEADER 12 bytes; read dimensions which are LE at 12-15 (height) and 16-19 (width) float32
    heightLUT = struct.unpack('<I', lut_data[0:4])[0]
    widthLUT = struct.unpack('<I', lut_data[4:8])[0]
    if manypoints>0:
        numpoints = struct.unpack('<I', lut_data[8:12])[0]
        lut_file.seek(12, os.SEEK_SET)

    else:
        numpoints = 1
        lut_file.seek(8, os.SEEK_SET)

    # image data starts after header which is 256 bytes and is LE float32
    actual_data = np.fromfile(lut_file, '<f')
    actual_data = np.asarray(actual_data)
    print 'importing LUT: ' + lut_loc + '   with height:width:numpoints - ' \
          + str(heightLUT) + ':' + str(widthLUT) + ':' + str(numpoints) + ' | and total size - ' + str(actual_data.size)
    thisLUT = np.zeros((heightLUT, widthLUT, numpoints))
    start = 0

    for row in range(heightLUT):
        for col in range(widthLUT):
            for i in range(numpoints):
                thisLUT[row, col, i] = actual_data[start]
                start += 1
    lut_file.close()
    print 'complete LUT ', thisLUT.shape
    return thisLUT[:, :, 2:22].squeeze()


def importLUTall(lut_loc,manypoints):
    lut_file = open(lut_loc)
    lut_data = lut_file.read()
    # HEADER 12 bytes; read dimensions which are LE at 12-15 (height) and 16-19 (width) float32
    heightLUT = struct.unpack('<I', lut_data[0:4])[0]
    widthLUT = struct.unpack('<I', lut_data[4:8])[0]
    if manypoints>0:
        numpoints = struct.unpack('<I', lut_data[8:12])[0]
        lut_file.seek(12, os.SEEK_SET)

    else:
        numpoints = 1
        lut_file.seek(8, os.SEEK_SET)

    # image data starts after header which is 256 bytes and is LE float32
    actual_data = np.fromfile(lut_file, '<f')
    actual_data = np.asarray(actual_data)
    print 'importing LUT: ' + lut_loc + '   with height:width:numpoints - ' \
          + str(heightLUT) + ':' + str(widthLUT) + ':' + str(numpoints) + ' | and total size - ' + str(actual_data.size)
    thisLUT = np.zeros((heightLUT, widthLUT, numpoints))
    start = 0

    for row in range(heightLUT):
        for col in range(widthLUT):
            for i in range(numpoints):
                thisLUT[row, col, i] = actual_data[start]
                start += 1
    lut_file.close()
    print 'complete LUT ', thisLUT.shape
    return thisLUT[:, :, :].squeeze()


def exportLUT(lut2save, lut_loc):
    #### HEADER
    height, width, numpoints = lut2save.shape
    # height
    image_file = open(lut_loc, 'wb')
    myfmt = '<I'
    bin = struct.pack(myfmt, height)
    # width
    image_file.write(bin)
    myfmt = '<I'
    bin1 = struct.pack(myfmt, width)
    # number points
    image_file.write(bin1)
    myfmt = '<I'
    bin2 = struct.pack(myfmt, numpoints)
    image_file.write(bin2)

    for row in range(height):
        for col in range(width):
            for i in range(numpoints):
                myfmt = '<f'
                bin3 = struct.pack(myfmt, np.float32(lut2save[row, col, i]))
                image_file.write(bin3)

        image_file.close()


def import_rng(filename):
    image_file = open(filename)
    image_data = image_file.read()
    # read dimensions which are LE at 12-15 (height) and 16-19 (width) float32
    height = struct.unpack('<L', image_data[13:17])[0]
    width = struct.unpack('<L', image_data[17:21])[0]
    # image data starts after header which is 256 bytes and is LE float32
    image_file.seek(256, os.SEEK_SET)
    actual_data = np.fromfile(image_file, '<f')
    # take to numpy array
    actual_data = np.asarray(actual_data)
    image = np.reshape(actual_data, (height, width))
    image_file.close()
    return image


def import_int(filename):
    image_file = open(filename)
    image_data = image_file.read()
    # read dimensions which are LE at 12-15 (height) and 16-19 (width) float32
    height = struct.unpack('<L', image_data[13:17])[0]
    width = struct.unpack('<L', image_data[17:21])[0]
    # image data starts after header which is 256 bytes and is LE float32
    image_file.seek(256, os.SEEK_SET)
    actual_data = np.fromfile(image_file, '<H')  # or <H if actual int data, or <f if .dat
    # take to numpy array
    actual_data = np.asarray(actual_data)
    image = np.reshape(actual_data, (height, width))
    image_file.close()
    return image


def importdats(image_file_LOC):
    image_file = open(image_file_LOC)

    image_data = image_file.read()
    # read dimensions which are LE at 12-15 (height) and 16-19 (width) float32
    height = struct.unpack('<L', image_data[0:4])[0]
    width = struct.unpack('<L', image_data[4:8])[0]
    # image data starts after header which is 256 bytes and is LE float32
    image_file.seek(8, os.SEEK_SET)
    actual_data = np.fromfile(image_file, '<f')
    actual_data = np.asarray(actual_data)
    image = (np.reshape(actual_data, (height, width)))

    image_file.close()
    return image

def writeCMUdats(int_image,inputfilename):
    # CREATE INT FOR CMU PLANEFIT
    int_image = np.rot90(int_image,-1)
    int_image = int_image.astype('>f')
    h, w = int_image.shape
    image_file = open(inputfilename, 'wb')
    image_file.write(struct.pack('>i4', h))
    image_file.write(struct.pack('>i4', w))
    int_image.tofile(image_file, '', '>f')
    image_file.close()
    return

def importCMUtxt(location):
    data = np.asarray(
        np.loadtxt((location), delimiter=' '))
    return data


def import_cams(filename):
    if filename.endswith('c1.bur') or filename.endswith('c2.bur') or filename.endswith('rng.bur'):
        image = import_rng(filename)
    elif filename.endswith('int.bur'):
        image = import_int(filename)
    elif filename.endswith('.dat'):
        image = importdats(filename)
    elif filename.endswith('.tif'):
        image = np.float32(Image.open(filename))
    else:
        print 'Data import failed. Camera datatype not supported.'
    image = np.float32(image)
    return image


def create_dir(folder):
    if not os.path.exists(folder):
        os.makedirs(folder)
        print 'creating directory: ' + folder
    else:
        shutil.rmtree(folder)
        os.makedirs(folder)
        print 'overwriting directory: ' + folder
    return
