

import shutil
import sys
from matplotlib.patches import Rectangle
import numpy as np
import fnmatch
import os
import matplotlib as mpl
import matplotlib.cm as cm
from matplotlib import pyplot as plt
import matplotlib.colors as colors
from mpl_toolkits.mplot3d import Axes3D
import cv2
import time
import math
import scipy

from useful_funSuper import *


# Data (outside, checkerboard)
loc_misc_data = 'Z:\PROJECTS\SUPER ALPHA\TestData\\2017-09-28 outside tests\\BUR_DATA_DRIVE'
loc_misc_data = 'C:\Users\\3dlabrat\Desktop\Lvenneri-python\Data\\testImages'


# Relevant Calibration Data
# Ramps
loc_ramps = 'Z:\PROJECTS\SUPER ALPHA\Calibration\\2017-09-26 calibration\calibration'
# Registration
loc_registration = 'Z:\PROJECTS\SUPER ALPHA\Calibration\\2017-09-24 calibration\\bodocal '


# view the frames

# list folders in the misc folder
misc_data_folders = [f for f in os.listdir(loc_misc_data) if f.startswith('2017')]
misc_data_folders = misc_data_folders[0:2]
for md_folder in misc_data_folders:
    md_folder = loc_misc_data + '\\'+ md_folder
    # REAL DATA
    # import  the rng and int image
    c1_loc = [f for f in os.listdir(md_folder) if
              (f.endswith('c1.bur') or f.endswith('cam1.dat')) and not f.startswith('.')]
    c2_loc = [f for f in os.listdir(md_folder) if
              (f.endswith('c2.bur') or f.endswith('cam2.dat')) and not f.startswith('.')]
    c1back_loc = [f for f in os.listdir(md_folder) if
                  (f.endswith('b1.bur') ) and not f.startswith('.')]
    c2back_loc = [f for f in os.listdir(md_folder) if
                  (f.endswith('b2.bur')) and not f.startswith('.')]
    int_loc = [f for f in os.listdir(md_folder) if
              (f.endswith('int.bur') ) and not f.startswith('.')]
    rng_loc = [f for f in os.listdir(md_folder) if
              (f.endswith('rng.bur')) and not f.startswith('.')]

plt.show()

# pipeline filtering



# pipeline verification



