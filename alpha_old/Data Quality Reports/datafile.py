import numpy as np
import re
import os
import struct

pat=re.compile(r'(.*)\_(int|rng|class)\.dat$')

class DataFile:
  def __init__(self):
    self.clear()
  
  def load(self, prefix):
    # Load intensity data, return on error
    if not self.loadIntensity(prefix+"_int.dat"):
      return False
    
    # Load range data, return on error
    if not self.loadRange(prefix+"_rng.dat"):
      return False
    
    # Load class data, return on error
    if not self.loadClass(prefix+"_class.dat"):
      return False
    
    return True
  
  def readArray(self, fname, dtype):
    data = None
    
    with open(fname, "rb") as f:
      # Read 2 4-byte integers in big-endian byte order
      header = np.fromfile(f, np.dtype('>i4'), 2)
    
      # If file has been cleared, load new width/height
      if self.width == 0 and self.height == 0:
        self.width = header[0]
        self.height = header[1]
        
      # If header does not match existing width/height, return error
      elif header[0] != self.width or header[1] != self.height:
        print "error: header size mismatch"
        return None
      
      # Read the data as a column-order matrix of 4-byte floats in big-endian byte order
      size = self.width * self.height
      shape = (self.height, self.width)
      data = np.fromfile(f, dtype, size)
      data = np.reshape(data, shape, 'F')
      data = np.flipud(data) # OpenCV wants the image data flipped?
      
    return data
  
  def loadIntensity(self, fname):
    data = self.readArray(fname, np.dtype('>f4'))
    
    if data != None:
      self.intensity = data
      return True
    
    return False
  
  def loadRange(self, fname):
    data = self.readArray(fname, np.dtype('>f4'))
    
    if data != None:
      self.range = data
      return True
    
    return False
  
  def loadClass(self, fname):
    data = self.readArray(fname, np.uint8)
    
    if data != None:
      self.klass = data
      return True
    
    return False
  
  def clear(self):
    self.width = 0
    self.height = 0
    self.intensity = np.empty((0, 0), np.float32)
    self.range = np.empty((0, 0), np.float32)
    self.klass = np.empty((0, 0), np.uint8)

  def getIntensity(self):
    return self.intensity
    #return self.intensity.copy()
  
  def getRange(self):
    return self.range
    #return self.range.copy()
  
  def getKlass(self):
    return self.klass
    #return self.klass.copy()
  
  def hasIntensity(self):
    return self.intensity.size != 0
  
  def hasRange(self):
    return self.range.size != 0
  
  def hasKlass(self):
    return self.klass.size != 0
  
  def filterRange(self):
    if self.range.size == 0:
      return
    
    if self.range.size != self.klass.size:
      print "error: data size mismatch in filterRange"
      return
    
    # In the C++ library, class 3 never gets set to anything; should be set to +inf?
    self.range[self.klass == 0] = np.NAN
    self.range[self.klass == 2] = -np.inf
    #self.range[self.klass == 3] = np.inf
  
  # Default values mean to autoscale
  def scaleTo8bitMinMax(self, minval=-float("inf"), maxval=float("inf")):
    im=self.getIntensity().copy()
    #valid = np.logical_and(np.isfinite(im), ~np.isnan(im))
    #maxval = im[valid].max() if not np.isfinite(maxval) else maxval
    #minval = im[valid].min() if not np.isfinite(minval) else minval
    ma = np.ma.masked_invalid(im, copy=False)
    if np.isinf(maxval):
      maxval = ma.max()
    if np.isinf(minval):
      minval = ma.min()
    im[im>maxval] = maxval
    im[im<minval] = minval
    im -= minval
    im /= (maxval - minval)
    return (255*im).astype('uint8')

  def histogramScaleTo8bit(self,qlo=0.1,qhi=0.9):
    im=self.getIntensity().copy()

    # get the histogram values (could subsample here)
    lo=np.percentile(im.flat,qlo*100.0)
    hi=np.percentile(im.flat,qhi*100.0)

    # now work out a linear mapping to take these to the position on
    # the [0,256) scale
    ylo=qlo*256.0
    yhi=qhi*256.0

    im=np.maximum(np.minimum((im-lo)*(yhi-ylo)/(hi-lo)+ylo,255.0),0.0)
    return im.astype('uint8')
    
  def scaleTo8bit(self):
    im=self.getIntensity().copy()
    # Saw im.max() == 0 in one data set, so...
    #mx = im.max()
    mx = np.nanmax(im)
    if (mx == 0):
      mx = 1;
    #end
    return (255.0*im/mx).astype('uint8')

  def scaleTo8bit2(self):
    im=self.getIntensity().copy()
    return np.maximum(np.minimum(im/100.0,255.0),0.0).astype('uint8')

  def gamma(self,g=0.75):
    im=self.getIntensity().copy()
    #im[np.isinf(im)] = 0
    im[np.isinf(im)] = np.NAN
    # Saw im.max() == 0 in one data set, so...
    #mx = im.max()
    #mn = im.min()
    mx = np.nanmax(im)
    mn = np.nanmin(im)
    #if (mx == 0) or np.isnan(mx):
    #  mx = 1;
    #end
    im-=mn
    #im/=mx
    im /= mx - mn
    return np.power(im,g)    

  def gammaTo8bit(self,g=0.75):
    return (self.gamma(g)*255.0).astype('uint8')

  def getMask(self):
    #return (self.getKlass()==ttv.KlassMode.valid)
    return (self.klass == 1)

  def loadSmart(self,f):
    if f.endswith('.dat'):
      m=pat.match(f)
      fn=m.group(1)
    else:
      fn=f
    #end

    ifn=fn+'_int.dat'
    rfn=fn+'_rng.dat'
    kfn=fn+'_class.dat'

    self.clear()
    ok=True
    loaded=False
    if ok and os.access(ifn,os.R_OK):
      ok=ok and self.loadIntensity(ifn)
      loaded=True
    #end
    if ok and os.access(rfn,os.R_OK):
      ok=ok and self.loadRange(rfn)
      loaded=True
    #end
    if ok and os.access(kfn,os.R_OK):
      ok=ok and self.loadClass(kfn)
      loaded=True
    #end
    return ok and loaded
    


if __name__=='__main__':
  import os,sys
  import argparse
  import matplotlib.pyplot as plt
  import cv2

  p = argparse.ArgumentParser(description='Load raw file and display.')
  p.add_argument('-f',dest='files',required=True,nargs='+',
                 help='File to load')
  p.add_argument('-o',dest='output',default=None,
                 help='Output path to conver to png')
  args = p.parse_args()

  if args.output!=None and not os.path.isdir(args.output):
    print "Making output directory:",args.output
    os.makedirs(args.output)
  #end
    
  df=DataFile()

  cv2.namedWindow('test')

  for f in args.files:
    df.loadSmart(f)

    im=df.getIntensity()
    
    if False:
      plt.clf()
      plt.imshow(df.gammaTo8bit())
      plt.set_cmap('gray')
      plt.tight_layout()
      plt.ion()
      plt.draw()
    else:
      cv2.imshow('test',df.gammaTo8bit())
      cv2.waitKey(0)
    #end
    if args.output!=None:
      fb=os.path.splitext(os.path.basename(f))[0]
      ofn=os.path.join(args.output,fb+'.png')
      print "output: ",ofn
      cv2.imwrite(ofn,im)      
    #end
  #end
  cv2.destroyAllWindows()