from useful_func import *
from reportlab.lib.enums import TA_JUSTIFY
from reportlab.lib.pagesizes import letter
from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer
from reportlab.platypus import Image as muchhate
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib.units import inch
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.platypus import SimpleDocTemplate, Table, TableStyle
import pylab
import matplotlib
import datetime
# matplotlib.style.use('dark_background')


matplotlib.style.use('ggplot')

def timeseries(data_loc,saveloc,range_image):

    placesx = np.arange(.2, .81, .1)
    placesy = np.arange(.2, .81, .1)
    placesx, placesy = np.meshgrid(placesx, placesy)
    placesx = np.reshape(placesx, placesx.size)
    placesy = np.reshape(placesy, placesy.size)
    symbols = ['o', 'v', '^', '<', '>', '8', 's', 'p', 'h', 'H']


    # WALL
    wallslices = 90
    rangesW = np.zeros((wallslices, placesx.size, 2))
    intensW = np.zeros((wallslices, placesx.size, 2))
    timing_offsetW = np.zeros((wallslices))
    time_capt = np.zeros((wallslices))

    wall_loc = data_loc

    # import rng images of ramp, and see
    rng_loc = [f for f in os.listdir(wall_loc) if
                  (f.endswith('rng.bur'))]
    int_loc = [f for f in os.listdir(wall_loc) if
              (f.endswith('int.bur'))]
    count = 0

    box = 15

    for f in range(min(len(rng_loc),wallslices)):
        if count == 0:
            placesx = placesx * range_im.shape[0]
            placesy = placesy * range_im.shape[1]
            range_image = np.mean(range_image[
                                  int(range_image.shape[0] / 2 - range_im.shape[0]):
                                  int(range_image.shape[0] / 2 + range_im.shape[0]),
                                  int(range_image.shape[1] / 2 - range_im.shape[1]):
                                  int(range_image.shape[1] / 2 + range_im.shape[1])])

        range_im = import_cams((wall_loc + '/' + rng_loc[f]))
        inten_im = import_cams((wall_loc + '/' + int_loc[f]))
        inten_im = np.divide(inten_im, np.power(range_image, 2.0))




        for i in range(placesx.size):
            temp_R = range_im[int(placesx[i]) - box:int(placesx[i]) + box,
                                      int(placesy[i]) - box:int(placesy[i]) + box]
            rangesW[count, i,:] = np.mean(temp_R),np.std(temp_R)
            temp_I = inten_im[int(placesx[i]) - box:int(placesx[i]) + box, int(placesy[i]) - box:int(placesy[i]) + box]
            intensW[count, i,:] = np.mean(temp_I),np.std(temp_I)

        separ = [pos for pos, char in enumerate(rng_loc[f]) if char == '_']
        timing_offsetW[count] = np.float32(rng_loc[f][separ[0]+1:separ[1]])
        time_capt[count] = np.float32(rng_loc[f][separ[1]+1:separ[2]])
        count += 1

    rangesW = pandas.DataFrame(rangesW-range_image)
    intensW = pandas.DataFrame(intensW)
    time_captHOURS = (time_capt-time_capt[0])/3600.0
    # plot moving average, noise, raw data, jitter over time for rng and intensity

    matplotlib.rcParams.update({'font.size': 6})
    fig = plt.figure(figsize=(6, 9))
    plt.subplot(321)
    plt.tight_layout(pad=1, w_pad=1, h_pad=2)
    plt.subplots_adjust(left=.1, bottom=.1, right=.9, top=.9, wspace=.3, hspace=.3)

    cm = pylab.get_cmap('gist_rainbow')
    movingWindowSize = 30
    plt.subplot(321)
    for i in range(placesx.size):
        color = cm(1. * i / placesx.size)
        plt.plot(time_captHOURS, pandas.rolling_std(rangesW[:,i,0], movingWindowSize), c=color, alpha=.8)
    plt.title('Range Jitter Over Time')
    plt.xlabel('Time (hours)')
    plt.ylabel('Range Jitter (m)')
    plt.grid('on')
    # plt.savefig(saveloc + '/time_v_rangeJit.png')


    plt.subplot(323)
    for i in range(placesx.size):
        color = cm(1. * i / placesx.size)
        plt.plot(time_captHOURS, (rangesW[:,i,0]), c=color, alpha=.5,label = 'Raw')
        plt.plot(time_captHOURS, pandas.rolling_mean(rangesW[:,i,0], movingWindowSize), c=color, alpha=.8,label = 'Rolling Mean (30)')
    plt.title('Range Error from True Over Time')
    plt.xlabel('Time (hours)')
    plt.ylabel('Range (m)')
    plt.legend()
    plt.grid('on')
    # plt.savefig(saveloc + '/time_v_range.png')


    plt.subplot(325)
    for i in range(placesx.size):
        color = cm(1. * i / placesx.size)
        plt.plot(time_captHOURS, (rangesW[:,i,1]), c=color, alpha=.5,label='Raw')
        plt.plot(time_captHOURS, pandas.rolling_mean(rangesW[:,i,1], movingWindowSize), c=color, alpha=.8,label = 'Rolling Mean (30)')
    plt.title('Range Noise Over Time')
    plt.xlabel('Time (hours)')
    plt.ylabel('Range Noise (m)')
    plt.legend()
    plt.grid('on')
    # plt.savefig(saveloc + '/time_v_rangeSTD.png')



    plt.subplot(322)
    fig = plt.figure(figsize=(6, 6))
    cm = pylab.get_cmap('gist_rainbow')
    movingWindowSize = 30
    for i in range(placesx.size):
        color = cm(1. * i / placesx.size)
        plt.plot(time_captHOURS, pandas.rolling_std(intensW[:, i, 0], movingWindowSize), c=color, alpha=.8)
    plt.title('Intensity Jitter Over Time')
    plt.xlabel('Time (hours)')
    plt.ylabel('Intensity Jitter (DN)')
    plt.grid('on')
    # plt.savefig(saveloc + '/time_v_IntensityJit.png')

    plt.subplot(324)
    for i in range(placesx.size):
        color = cm(1. * i / placesx.size)
        plt.plot(time_captHOURS, (intensW[:, i, 0]), c=color, alpha=.5,label = 'Raw')
        plt.plot(time_captHOURS, pandas.rolling_mean(intensW[:, i, 0], movingWindowSize), c=color, alpha=.8,label = 'Rolling Mean (30)')

    plt.title('Intensity Over Time (at 1m)')
    plt.xlabel('Time (hours)')
    plt.ylabel('Intensity (DN)')
    plt.legend()
    plt.grid('on')

    plt.subplot(326)
    for i in range(placesx.size):
        color = cm(1. * i / placesx.size)
        plt.plot(time_captHOURS, (intensW[:,i,1]), c=color, alpha=.5,label='Raw')
        plt.plot(time_captHOURS, pandas.rolling_mean(intensW[:,i,1], movingWindowSize), c=color, alpha=.8,label = 'Rolling Mean (30)')
    plt.title('Intensity Noise Over Time')
    plt.xlabel('Time (hours)')
    plt.ylabel('Intensity Noise (DN)')
    plt.legend()
    plt.grid('on')
    # plt.savefig(saveloc + '/time_v_rangeSTD.png')


    plt.savefig(saveloc + '/warmp_up_drift.png')
    plt.savefig(saveloc + '/fiducials.png')


