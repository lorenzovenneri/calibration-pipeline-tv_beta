from useful_func import *
from reportlab.lib.enums import TA_JUSTIFY
from reportlab.lib.pagesizes import letter
from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer
from reportlab.platypus import Image as muchhate
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib.units import inch
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.platypus import SimpleDocTemplate, Table, TableStyle
import pylab
import matplotlib
import datetime
# matplotlib.style.use('dark_background')
from numpy import genfromtxt


# matplotlib.style.use('ggplot')
#
#
# data_loc0 = '/Users/lvenneri/Downloads/BUR_DATA_DRIVE_MARCH15/test'
#
# data_locs = [f for f in os.listdir(data_loc0) if not f.startswith('.') and not f.startswith('ana') and not f.startswith('3mW')]
#
# # for each subfolder do:
# rangeJIT = np.zeros((len(data_locs),4))
# intenJIT = np.zeros((len(data_locs),4))
# countFOLD = 0
#
# # the data to look at folder is
#
# # the calibration folder is
# calibration_loc = '/Users/lvenneri/Desktop/development/ARRI2 (cal to use)'
# lens_cal = calibration_loc+'/lensCal1_out'
#
# # get range image from plane fit
#
# saveloc = data_loc0 + '/analysis_output'
# create_dir(saveloc)
# range_image = 0.0



def timeseries(data_loc0,saveloc,range_image, INTERNAL):

    symbols = ['o', 'v', '^', '<', '>', '8', 's', 'p', 'h', 'H']

    csv_head =  ['unix time', 'range roi', 'ac_internal', 'ac_internal', 'set_timing_offset', 'HV fiducial', 'Laser fiducial', 'ac_internal_timinginfo','rand1','rand2','rand3','rand4']
    # data = np.array((genfromtxt((data_loc), delimiter=',')))



    # import rng images of ramp, and see
    csv_loc = [f for f in os.listdir(data_loc0) if
                  (f.endswith('.csv'))]

    rangesW = []
    time_capt = []
    HV_fids = []
    laser_fids = []
    count = 0


    for f in range(len(csv_loc)):
        data_loc = data_loc0+'/'+csv_loc[f]
        data = pandas.read_csv(data_loc, sep=', ', names=csv_head,engine='python')
        rangesW.append(data['range roi']-range_image)
        # intensW[f, :] =
        time_captHOURS = (data['unix time'] - data['unix time'][0])/3600000.0
        time_capt.append(time_captHOURS)

        HV_fids.append(data['Laser fiducial'] - data['Laser fiducial'][0])
        laser_fids.append(data['HV fiducial'] - data['HV fiducial'][0])

        count += 1

    # plot moving average, noise, raw data, jitter over time for rng and intensity

    matplotlib.rcParams.update({'font.size': 6})
    plt.figure(figsize=(6, 9))
    # plt.tight_layout(pad=1, w_pad=1, h_pad=2)
    plt.subplots_adjust(left=.1, bottom=.1, right=.9, top=.9, wspace=.3, hspace=.3)

    cm = pylab.get_cmap('Vega10')
    movingWindowSize = 30
    if INTERNAL == 1:

        plt.subplot(311)

        for i in range(len(csv_loc)):
            color = cm(1. * i / len(csv_loc))
            jitoffset = (HV_fids[i] - pandas.rolling_mean(HV_fids[i], movingWindowSize))*.2998/2000.0
            # jitoffset = 0.0
            plt.plot(time_capt[i], pandas.rolling_std(rangesW[i], movingWindowSize), c=color, lw = .5,alpha=.5,label = csv_loc[i])
        plt.legend()
        plt.title('Range Jitter Over Time')
        plt.xlabel('Time (hours)')
        plt.ylabel('Range Jitter (m)')
        plt.grid('on')
        # plt.savefig(saveloc + '/time_v_rangeJit.png')
        plt.subplot(312)
    else:
        plt.subplot(111)

    for i in range(len(csv_loc)):
        color = cm(1. * i / len(csv_loc))
        plt.plot(time_capt[i], (rangesW[i]), c=color, alpha=.5,label = 'Raw')

        plt.plot(time_capt[i], pandas.rolling_mean(rangesW[i], movingWindowSize), c=color, lw = .5,alpha=.9,label = (csv_loc[i] + ' - Rolling Mean'))
    hor = np.asarray([0,1.5])
    vert = np.asarray([2.65,2.65])
    print hor,vert
    plt.plot(hor, vert, c='r', lw = 1,alpha=1,label = 'Ground Truth')

    plt.title('Range Error from True Over Time')
    plt.xlabel('Time (hours)')
    plt.ylabel('Range (m)')
    plt.legend()
    plt.grid('on')
    # plt.savefig(saveloc + '/time_v_range.png')


    # plt.subplot(313)
    # for i in range(placesx.size):
    #     color = cm(1. * i / placesx.size)
    #     plt.plot(time_captHOURS, (rangesW[:,i,1]), c=color, alpha=.5,label='Raw')
    #     plt.plot(time_captHOURS, pandas.rolling_mean(rangesW[:,i,1], movingWindowSize), c=color, alpha=.8,label = 'Rolling Mean (30)')
    # plt.title('Range Noise Over Time')
    # plt.xlabel('Time (hours)')
    # plt.ylabel('Range Noise (m)')
    # plt.legend()
    # plt.grid('on')
    # plt.savefig(saveloc + '/time_v_rangeSTD.png')
    if INTERNAL == 1:

        plt.subplot(313)
        for i in range(len(csv_loc)):
            color = cm(1. * i / len(csv_loc))
            plt.plot(time_capt[i],pandas.rolling_mean(laser_fids[i], movingWindowSize),lw=2, c=color,  alpha=.5,
                     label=csv_loc[i]+'-Diode')
            plt.plot(time_capt[i],pandas.rolling_mean(HV_fids[i], movingWindowSize), c=color, lw=.5, alpha=.5,
                     label=csv_loc[i]+'-HV')
        plt.legend()
        plt.title('Fiducial Changes Over Time')
        plt.xlabel('Time (hours)')
        plt.ylabel('Fiducial (change From start)')
        plt.grid('on')



    # plt.savefig(saveloc + '/time_v_rangeSTD.png')


    plt.savefig(saveloc + '/warmp_up_drift.png')

    matplotlib.rcParams.update({'font.size': 6})
    plt.figure(figsize=(6, 9))
    # plt.tight_layout(pad=1, w_pad=1, h_pad=2)
    # plt.subplots_adjust(left=.1, bottom=.1, right=.9, top=.9, wspace=.3, hspace=.3)

    plt.subplot(211)
    for i in range(len(csv_loc)):
        color = cm(1. * i / len(csv_loc))
        plt.scatter( HV_fids[i],rangesW[i]-rangesW[i][0], c=color, lw=0,s=5, alpha=.5, label=csv_loc[i])

    plt.legend()
    plt.title('HV Fiducial Changes Over Time')
    plt.ylabel(' Range (change from start - m)')
    plt.xlabel('HV Fiducial (change from start - tmu)')
    plt.grid('on')


    plt.subplot(212)
    for i in range(len(csv_loc)):
        color = cm(1. * i / len(csv_loc))
        plt.scatter(laser_fids[i],rangesW[i]-rangesW[i][0],  c=color, lw=0,s=5, alpha=.5, label=csv_loc[i])
    plt.legend()
    plt.title('Diode Fiducial Changes Over Time')
    plt.ylabel(' Range (change from start - m)')
    plt.xlabel('Diode Fiducial (change from start - tmu)')
    plt.grid('on')

    plt.savefig(saveloc + '/fiducials.png')




# timeseries(data_loc0+'/3mWarmUp', saveloc, range_image)
# plt.show()