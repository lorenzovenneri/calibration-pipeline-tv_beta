from useful_func import *
from reportlab.lib.enums import TA_JUSTIFY
from reportlab.lib.pagesizes import letter
from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer
from reportlab.platypus import Image as muchhate
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib.units import inch
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.platypus import SimpleDocTemplate, Table, TableStyle
import pylab
import matplotlib
import datetime
import subprocess
from reportlab.platypus import SimpleDocTemplate, Paragraph, Image, Spacer, PageBreak, Table, TableStyle
from warm_up import *
# matplotlib.style.use('dark_background')


def planefit(range_image,a_angleLUT,b_angleLUT):
    xcoord0 = np.multiply(range_image, a_angleLUT)
    ycoord0 = np.multiply(range_image, b_angleLUT)

    # plane fit the image, to get the right vector
    # FAST PLANE FIT on range data
    # Ax + By + Cz + D = 0, and write the equation as a matrix multiplication. P is your unknown 4x1 [A;B;C;D]
    # g = [x y z 1];  % represent a point as an augmented row vector
    # g*P = 0, G*P = E  % E is a Nx1 vector
    range_image0 = np.reshape(range_image, range_image.size)
    xcoord0 = np.reshape(xcoord0, range_image.size)
    ycoord0 = np.reshape(ycoord0, range_image.size)

    last = np.ones(range_image.size)
    g = np.column_stack((xcoord0, ycoord0, range_image0, last))
    g = g[g[:, 2] > 0, :]
    u, s, v = np.linalg.svd(g)
    P = v[3, :]
    CENTER = [0, 0, -P[3] / P[2]]  # CENTER
    norma = P[0:3]  # NORMAL
    return norma, CENTER

tim = datetime.datetime.now().strftime("%I:%M%p on %B %d, %Y")
print 'TIME: ', tim

matplotlib.style.use('ggplot')

INTERNAL = 1
data_loc0 = '/Users/lvenneri/Downloads/BUR_DATA_DRIVE_MARCH15/test'

data_locs = [f for f in os.listdir(data_loc0) if not f.startswith('.') and not f.startswith('ana') and not f.startswith('3mW')]

# for each subfolder do:
# the calibration folder is
calibration_loc = '/Users/lvenneri/Desktop/development/ARRI2 (cal to use)'
lens_cal3 = calibration_loc + '/lensCal1_out(binX3.0)'
lens_cal = calibration_loc + '/lensCal1_out(binX1.0)'
CMU_files_loc =''





saveloc = data_loc0 + '/analysis_output'
create_dir(saveloc)

rangeJIT = np.zeros((len(data_locs),4))
intenJIT = np.zeros((len(data_locs),4))
countFOLD = 0

for dd in data_locs:

    # the data to look at folder is
    data_loc = data_loc0 + '/'+dd

    # for the temporal data do:
    timeData = 1


    # ramp location
    ramp_loc0 = data_loc+'/rampRNG'
    wall_loc0 = data_loc+'/wallRNG'
    ramp_folders = [f for f in os.listdir(ramp_loc0) if not f.startswith('.') if f.startswith('rw')]
    wall_folders = [f for f in os.listdir(wall_loc0) if not f.startswith('.') if f.startswith('rw')]

    placesx = np.arange(.7, .81, .1)
    placesy = np.arange(.2, .81, .3)
    placesx, placesy = np.meshgrid(placesx, placesy)
    placesxW = np.reshape(placesx, placesx.size)
    placesyW = np.reshape(placesy, placesy.size)
    symbols = ['o', 'v', '^', '<', '>', '8', 's', 'p', 'h', 'H']

    # get range image from plane fit
    one_wall_fit_loc = wall_loc0+'/'+wall_folders[0]
    if 1 == 0:
        holder = [f for f in os.listdir(one_wall_fit_loc) if
                  not f.startswith('.') and (f.endswith('c2.bur') or f.endswith('int.bur'))]
        if len(holder) >= 4:
            print 'computing... CMU wall range ---------------------------------------------------------------------------------'
            if 1 == 1:
                # find wall data, and put in the right format for CMU
                # find lenscal data, and put in the right format for CMU
                filesWALL = [f for f in os.listdir(wall_loc) if
                             not f.startswith('.') and (f.endswith('int.bur') or f.endswith('2.bur'))]

                cmuformatfolder = one_wall_fit_loc + '/CMU_format'
                create_dir(cmuformatfolder)

                count = 0
                for f in range(2):
                    f = filesWALL[f]
                    image22 = import_cams(wall_loc + '/' + f)
                    writeCMUdats(image22, cmuformatfolder + '/' + f + 'int.dat')
                # wall
                doitWALL = ('C:\Python27\python.exe "' + str(CMU_files_loc + '\generate_flat_plane.py') +
                            '" -c "' + str(lens_cal.replace('/', '\\')) +
                            '" -f  "' + str(cmuformatfolder.replace('/', '\\')) + '"')
                subprocess.call(doitWALL)

            holder = [f for f in os.listdir(lens_cal + '/wall') if
                      not f.startswith('.')]
            range_image_CMU_loc = lens_cal + '/wall/' + holder[0] + '/range_image.dat.gz'
            # unzip the range file
            # convert files to a mode Arjun can use and save
            range_image = importCMUtxt(range_image_CMU_loc)

            # this image must be multiplied by the angle files
            anglesLUT = importLUTall(calibration_loc + '/angles.dat', 0)
            a_angleLUT = importLUTall(calibration_loc + '/a_angle.dat', 0)
            b_angleLUT = importLUTall(calibration_loc + '/b_angle.dat', 0)

            range_image = np.multiply(range_image, np.cos((anglesLUT)))  # cos((angles)) to take to rectilinear

            cmu_NORM, cmu_CENTER = planefit(range_image, a_angleLUT, b_angleLUT)

            print cmu_NORM, cmu_CENTER

        else:
            print 'ERROR - Missing wall data. (Need at least 2 wall images for c2).'
            sys.exit()
    else:
        range_image = 0.0

    # WALL
    wallslices = 24
    if 1==1:
        rangesW = np.zeros((len(wall_folders),wallslices, placesx.size, 2))
        intensW = np.zeros((len(wall_folders),wallslices, placesx.size, 2))
        timing_offsetW = np.zeros((len(wall_folders),wallslices))
        for rr in range(len(wall_folders)):
            wall_loc = wall_loc0+'/'+wall_folders[rr]

            # import rng images of ramp, and see
            rng_loc = [f for f in os.listdir(wall_loc) if
                          (f.endswith('rng.bur'))]
            int_loc = [f for f in os.listdir(wall_loc) if
                      (f.endswith('int.bur'))]
            count = 0

            box = 15

            if wallslices>len(rng_loc):
                print 'ERROR: insufficient slices for jitter measurement'
                sys.exit()
            for f in range(min(len(rng_loc),wallslices)):
                start = time.time()
                range_im = import_cams((wall_loc + '/' + rng_loc[f]))
                inten_imW = import_cams((wall_loc + '/' + int_loc[f]))
                if count == 0 and rr == 0:
                    placesxW = placesxW * range_im.shape[0]
                    placesyW = placesyW * range_im.shape[1]

                for i in range(placesxW.size):
                    temp_R = range_im[int(placesxW[i]) - box:int(placesxW[i]) + box,
                                              int(placesyW[i]) - box:int(placesyW[i]) + box]
                    rangesW[rr,count, i,:] = np.mean(temp_R),np.std(temp_R)
                    temp_I = inten_imW[int(placesxW[i]) - box:int(placesxW[i]) + box,
                                              int(placesyW[i]) - box:int(placesyW[i]) + box]
                    intensW[rr,count, i,:] = np.mean(temp_I),np.std(temp_I)
                separ = [pos for pos, char in enumerate(rng_loc[f]) if char == '_']
                timing_offsetW[rr,count] = np.float32(rng_loc[f][separ[0] + 1:separ[1]])
                # time_capt[count] = np.float32(rng_loc[f][separ[1] + 1:separ[2]])
                count += 1

    # RAMP
    placesx = np.arange(.2, .81, .3)
    placesy = np.arange(.2, .81, .3)

    placesx, placesy = np.meshgrid(placesx, placesy)
    placesx = np.reshape(placesx, placesx.size)
    placesy = np.reshape(placesy, placesy.size)
    ranges = np.zeros((len(ramp_folders),24, placesx.size, 2))
    intens = np.zeros((len(ramp_folders),24, placesx.size, 2))
    timing_offset = np.zeros((len(ramp_folders),24))
    for rr in range(len(ramp_folders)):
        ramp_loc = ramp_loc0+'/'+ramp_folders[rr]

        # import rng images of ramp, and see
        rng_loc = [f for f in os.listdir(ramp_loc) if
                      (f.endswith('rng.bur'))]
        int_loc = [f for f in os.listdir(ramp_loc) if
                  (f.endswith('int.bur'))]
        count = 0

        box = 15

        for f in range(min(len(rng_loc),24)):
            start = time.time()
            range_im = import_cams((ramp_loc + '/' + rng_loc[f]))
            inten_im0 = import_cams((ramp_loc + '/' + int_loc[f]))
            inten_im = np.divide(inten_im0,np.power(range_image,2.0))
            if count == 0 and rr == 0:
                placesx = placesx * range_im.shape[0]
                placesy = placesy * range_im.shape[1]

                tv_NORM, tv_CENTER = planefit(range_im, a_angleLUT, b_angleLUT)
                print tv_NORM, tv_CENTER

            for i in range(placesx.size):
                temp_R = range_im[int(placesx[i]) - box:int(placesx[i]) + box,
                                          int(placesy[i]) - box:int(placesy[i]) + box]
                ranges[rr,count, i,:] = np.mean(temp_R),np.std(temp_R)
                temp_I = inten_im[int(placesx[i]) - box:int(placesx[i]) + box, int(placesy[i]) - box:int(placesy[i]) + box]
                intens[rr,count, i,:] = np.mean(temp_I),np.std(temp_I)
            timing_offset[rr,count] = np.float32(rng_loc[f][7:13])

            count += 1




    matplotlib.rcParams.update({'font.size': 6})
    fig = plt.figure(figsize=(6, 9))
    plt.subplot(321)
    plt.tight_layout(pad=1, w_pad=1, h_pad=2)
    plt.subplots_adjust(left=.1, bottom=.1, right=.9, top=.9, wspace=.3, hspace=.3)

    cm = pylab.get_cmap('gist_rainbow')

    plt.imshow(np.vstack((inten_im0,inten_imW)),cmap='gray', interpolation='none')
    if 1==1:
        for i in range(placesx.size):
            color = cm(1. * i / placesx.size)
            plt.scatter(placesy[i],placesx[i],c=color,s=30,lw=0,alpha = 1)
            if i <placesxW.size:
                plt.scatter(placesyW[i],placesxW[i]+inten_im.shape[0],c=color,s=30,lw=0,alpha = 1)
    plt.title('SCENE: ' + dd,size=18)
    plt.axis('off')
    # plt.savefig(saveloc+'/intensity.png')



    # flatness over ramp
    plt.subplot(322)
    for rr in range(len(ramp_folders)):
        for i in range(placesx.size):
            color = cm(1. * i / placesx.size)
            plt.scatter(timing_offset[rr,:],ranges[rr,:,i,0],marker=symbols[rr],c=color,s=10,lw=0,alpha = .7)
        plt.xlabel('Timing Offset (ns)')
        plt.ylabel('Range (m)')
        plt.title('Range over rw')
        plt.grid('on')
        # plt.savefig(saveloc+'/range_over_rangewindow.png')

    if INTERNAL== 1:

        # range vs range noise
        plt.subplot(325)
        for rr in range(len(ramp_folders)):
            for i in range(placesx.size):
                color = cm(1. * i / placesx.size)
                plt.scatter(ranges[rr,:,i,0],ranges[rr,:,i,1],marker=symbols[rr],c=color,s=10,lw=0,alpha = .7)
            plt.xlabel('Range (m)')
            plt.ylabel('Range Noise (m)')
            plt.title('Range Noise in the Image')
            plt.grid('on')
            # plt.savefig(saveloc+'/range_v_range(std).png')

        # noise over ramp
        plt.subplot(324)
        for rr in range(len(ramp_folders)):
            for i in range(placesx.size):
                color = cm(1. * i / placesx.size)
                plt.scatter(timing_offset[rr,:],ranges[rr,:,i,1],marker=symbols[rr],c=color,s=10,lw=0,alpha = .7)
            plt.xlabel('Timing Offset (ns)')
            plt.ylabel('Range Noise (m)')
            plt.title('Range Noise (std) over rw')
            plt.grid('on')
            # plt.savefig(saveloc+'/rangeSTD_over_rangewindow.png')

        plt.subplot(323)

        for rr in range(len(ramp_folders)):

            for i in range(placesx.size):
                color = cm(1. * i / placesx.size)
                plt.scatter(timing_offset[rr, :], intens[rr, :, i, 0], marker=symbols[rr], c=color, s=10, lw=0, alpha=.7)
            plt.xlabel('Timing Offset (ns)')
            plt.ylabel('Intensity (DN)')
            plt.title('Intensity at 1 m over rw')
            plt.grid('on')


    # range Error over ramp
    plt.subplot(326)
    for rr in range(len(ramp_folders)):
        for i in range(placesx.size):
            color = cm(1. * i / placesx.size)
            plt.scatter(timing_offset[rr,:],ranges[rr,:,i,0]-range_image,marker=symbols[rr],c=color,s=10,lw=0,alpha = .7)
        plt.xlabel('Timing Offset (ns)')
        plt.ylabel('Range Error (m)')
        plt.title('Range Error (from vision plane fit)')
        plt.grid('on')
        # plt.savefig(saveloc+'/rangeSTD_over_rangewindow.png')

    plt.savefig(saveloc + '/' + dd + 'range_window_figs.png')

    # range

    # intensity -> scaled to 1m using the wall range measurement

    if timeData ==1:
        timeseries(data_loc0+'/3mWarmUp', saveloc, 0.0,INTERNAL)


    for rr in range(len(wall_folders)):
        rangeJIT[countFOLD,rr] = np.around(np.std(rangesW[rr, :, 0, 0]) * 100.0,4)
        intenJIT[countFOLD, rr] = np.around(np.std(intensW[rr, :, 0, 0]) / np.max(intensW[rr, :, 0, 0]),4)
        countFOLD+=1
# compare to a checkerboard to see

# checkerboard - 100s

# range error

# flatness

# jitter


# plt.xlabel('')
# plt.ylabel('')
# plt.title('')



# plot fiducials

import pandas as pd

from pandas.tools.plotting import scatter_matrix


doc = SimpleDocTemplate(saveloc+'/summary.pdf', pagesize=letter,
                        rightMargin=60, leftMargin=60,
                        topMargin=60, bottomMargin=60)
Story = []

formatted_time = str(time.time())
title = 'Alpha Prototype Data Quality Report (BOT)'
company = 'TetraVue'

styles = getSampleStyleSheet()
styleNormal = styles['Normal']
styleHeading = styles['Heading1']
styleHeading.alignment = 1



ptext = '<font name=HELVETICA size=20>%s</font>' % title
Story.append(Paragraph(ptext, styles["Heading1"]))

ptext = '<font name=HELVETICA size=10>%s</font>' % company
Story.append(Paragraph(ptext, styles["Heading1"]))

ptext = '<font name=HELVETICA size=10>%s</font>' % tim
Story.append(Paragraph(ptext, styles["Heading1"]))

ptext = '<font name=HELVETICA size=10>%s</font>' %  ('SN: '+data_loc0)
Story.append(Paragraph(ptext, styles["Normal"]))

ptext = '<font name=HELVETICA size=10>%s</font>' % ('Data location: '+data_loc0)
Story.append(Paragraph(ptext, styles["Normal"]))


Story.append(Spacer(1, 30))

if timeData ==1:
    ptext = '<font name=HELVETICA size=15>%s</font>' % ('Warm Up Data')
    Story.append(Paragraph(ptext, styles["Normal"]))
    Story.append(Spacer(1, 10))
    ptext = '<font name=HELVETICA size=10>%s</font>' % ('Location: '+data_loc0+'/3mWarmUp')
    Story.append(Paragraph(ptext, styles["Normal"]))
    Story.append(Spacer(1, 20))


if 1==1:
    ptext = '<font name=HELVETICA size=15>%s</font>' % ('Lens Calibration Summary')
    Story.append(Paragraph(ptext, styles["Normal"]))
    Story.append(Spacer(1, 10))
    summary = open(lens_cal3+'/summary.txt').read()

    FOV = summary[summary.find('fov')+4:summary.find('fov')+14]
    princip = summary[summary.find('principal')+18:summary.find('principal')+44]
    ptext = '<font name=HELVETICA size=10>%s</font>' % ('Measured FOV deg (x,y): ' +FOV)
    Story.append(Paragraph(ptext, styles["Normal"]))
    ptext = '<font name=HELVETICA size=10>%s</font>' % ('Measured Principal Point 3x3bin (pixels): '+princip)
    Story.append(Paragraph(ptext, styles["Normal"]))

    summary = open(lens_cal + '/summary.txt').read()
    princip = summary[summary.find('principal') + 18:summary.find('principal') + 44]
    ptext = '<font name=HELVETICA size=10>%s</font>' % ('Measured Principal Point HD (pixels): '+princip)
    Story.append(Paragraph(ptext, styles["Normal"]))
    Story.append(Spacer(1, 20))

    ptext = '<font name=HELVETICA size=15>%s</font>' % ('Performance Checks')
    Story.append(Paragraph(ptext, styles["Normal"]))
    Story.append(Spacer(1, 10))
    ptext = '<font name=HELVETICA size=10>%s</font>' % ('1. Software Trigger: |  |')
    Story.append(Paragraph(ptext, styles["Normal"]))
    ptext = '<font name=HELVETICA size=10>%s</font>' % ('2. Hardware Trigger: |  |')
    Story.append(Paragraph(ptext, styles["Normal"]))
    ptext = '<font name=HELVETICA size=10>%s</font>' % ('3. Max Frame Rate: |  |')
    Story.append(Paragraph(ptext, styles["Normal"]))
    ptext = '<font name=HELVETICA size=10>%s</font>' % ('4. Eye Safety: |  |')
    Story.append(Paragraph(ptext, styles["Normal"]))

    Story.append(Spacer(1, 20))

countFOLD = 0
for dd in data_locs:
    ptext = '<font name=HELVETICA size=15>%s</font>' % ('Range Measurements: ' + dd)
    Story.append(Paragraph(ptext, styles["Normal"]))
    Story.append(Spacer(1, 10))

    ptext = '<font name=HELVETICA size=10>%s</font>' % ('Range Jitter (cm) for each range window')
    Story.append(Paragraph(ptext, styles["Normal"]))
    for rr in range(len(wall_folders)):
        ptext = '<font name=HELVETICA size=10>%s</font>' % (wall_folders[rr] + ':  ' + str(rangeJIT[countFOLD,rr]))
        Story.append(Paragraph(ptext, styles["Normal"]))

    ptext = '<font name=HELVETICA size=10>%s</font>' % ('Intensity Jitter (Fraction of Max) for each range window')
    Story.append(Paragraph(ptext, styles["Normal"]))
    for rr in range(len(wall_folders)):
        ptext = '<font name=HELVETICA size=10>%s</font>' % (wall_folders[rr] + ':  ' + str(intenJIT[countFOLD,rr]))
        Story.append(Paragraph(ptext, styles["Normal"]))
    countFOLD+=1
    Story.append(Spacer(1, 20))

Story.append(PageBreak())
for dd in data_locs:
    im = muchhate((saveloc + '/'+dd+'range_window_figs.png'), 6 * inch, 9 * inch)
    Story.append(im)


if timeData ==1:
    Story.append(PageBreak())

    im = muchhate((saveloc + '/warmp_up_drift.png'), 6 * inch, 9 * inch)
    Story.append(im)
    Story.append(PageBreak())

    im = muchhate((saveloc + '/fiducials.png'), 6 * inch, 9 * inch)
    Story.append(im)




Story.append(Spacer(1, 10))
doc.build(Story)


plt.show()


