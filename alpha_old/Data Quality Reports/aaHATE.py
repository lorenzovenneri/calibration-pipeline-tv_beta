from useful_func import *
from reportlab.lib.enums import TA_JUSTIFY
from reportlab.lib.pagesizes import letter
from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer
from reportlab.platypus import Image as muchhate
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib.units import inch
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.platypus import SimpleDocTemplate, Table, TableStyle
import pylab
import matplotlib
import datetime
from reportlab.platypus import SimpleDocTemplate, Paragraph, Image, Spacer, PageBreak, Table, TableStyle
from warm_up import *
# matplotlib.style.use('dark_background')

tim = datetime.datetime.now().strftime("%I:%M%p on %B %d, %Y")
print 'TIME: ', tim

matplotlib.style.use('ggplot')

INTERNAL = 0
data_loc0 = '/Users/lvenneri/Downloads/BUR_DATA_DRIVE_MARCH15/test'
lens = '/Users/lvenneri/Downloads/BUR_DATA_DRIVE_MARCH15/test'

data_locs = [f for f in os.listdir(data_loc0) if not f.startswith('.') and not f.startswith('ana') and not f.startswith('3mW')]

# for each subfolder do:
# the calibration folder is
calibration_loc = '/Users/lvenneri/Desktop/development/ARRI2 (cal to use)'
lens_cal = calibration_loc + '/lensCal1_out(binX3.0)'

# get range image from plane fit

saveloc = data_loc0 + '/analysis_output'
create_dir(saveloc)

rangeJIT = np.zeros((len(data_locs),4))
intenJIT = np.zeros((len(data_locs),4))
countFOLD = 0

for dd in data_locs:

    # the data to look at folder is
    data_loc = data_loc0 + '/'+dd


    range_image = 1.0

    # for the temporal data do:
    timeData = 1
    if timeData ==1:
        timeseries(data_loc0+'/3mWarmUp', saveloc, 0.0,INTERNAL)



    # ramp location
    ramp_loc0 = data_loc+'/rampRNG'
    wall_loc0 = data_loc+'/wallRNG'
    ramp_folders = [f for f in os.listdir(ramp_loc0) if not f.startswith('.') if f.startswith('rw')]
    wall_folders = [f for f in os.listdir(wall_loc0) if not f.startswith('.') if f.startswith('rw')]

    placesx = np.arange(.7, .81, .1)
    placesy = np.arange(.2, .81, .3)
    placesx, placesy = np.meshgrid(placesx, placesy)
    placesxW = np.reshape(placesx, placesx.size)
    placesyW = np.reshape(placesy, placesy.size)
    symbols = ['o', 'v', '^', '<', '>', '8', 's', 'p', 'h', 'H']


    # WALL
    wallslices = 24
    if 1==1:
        csv_head = ['unix time', 'range roi', 'ac_internal', 'ac_internal', 'set_timing_offset', 'HV fiducial',
                    'Laser fiducial', 'ac_internal_timinginfo', 'rand1', 'rand2', 'rand3', 'rand4']
        # data = np.array((genfromtxt((data_loc), delimiter=',')))

        rangesW = []
        time_capt = []
        HV_fids = []
        laser_fids = []
        count = 0

        data_loc = '/Users/lvenneri/Downloads/BUR_DATA_DRIVE_MARCH15/zebraROI.csv'
        data = pandas.read_csv(data_loc, sep=', ', names=csv_head, engine='python')
        rangesW = data['unix time'][:-30]
        print rangesW

        count += 1

    # RAMP
    placesx = np.arange(.2, .81, .3)
    placesy = np.arange(.2, .81, .3)
    placesx = np.arange(.5, .61, .3)
    placesy = np.arange(.5, .61, .3)
    placesx, placesy = np.meshgrid(placesx, placesy)
    placesx = np.reshape(placesx, placesx.size)
    placesy = np.reshape(placesy, placesy.size)
    ranges = np.zeros((len(ramp_folders),24, placesx.size, 2))
    intens = np.zeros((len(ramp_folders),24, placesx.size, 2))
    timing_offset = np.zeros((len(ramp_folders),24))
    for rr in range(len(ramp_folders)):
        ramp_loc = ramp_loc0+'/'+ramp_folders[rr]

        # import rng images of ramp, and see
        rng_loc = [f for f in os.listdir(ramp_loc) if
                      (f.endswith('rng.bur'))]
        int_loc = [f for f in os.listdir(ramp_loc) if
                  (f.endswith('int.bur'))]
        count = 0

        box = 15

        for f in range(min(len(rng_loc),24)):
            start = time.time()
            range_im = import_cams((ramp_loc + '/' + rng_loc[f]))
            inten_im = import_cams((ramp_loc + '/' + int_loc[f]))
            inten_im = np.divide(inten_im,np.power(range_image,2.0))
            if count == 0 and rr == 0:
                placesx = placesx * range_im.shape[0]
                placesy = placesy * range_im.shape[1]
            for i in range(placesx.size):
                temp_R = range_im[int(placesx[i]) - box:int(placesx[i]) + box,
                                          int(placesy[i]) - box:int(placesy[i]) + box]
                ranges[rr,count, i,:] = np.mean(temp_R),np.std(temp_R)
                temp_I = inten_im[int(placesx[i]) - box:int(placesx[i]) + box, int(placesy[i]) - box:int(placesy[i]) + box]
                intens[rr,count, i,:] = np.mean(temp_I),np.std(temp_I)
            timing_offset[rr,count] = np.float32(rng_loc[f][7:13])

            count += 1




    matplotlib.rcParams.update({'font.size': 6})
    fig = plt.figure(figsize=(6, 9))
    # plt.tight_layout(pad=1, w_pad=1, h_pad=2)
    plt.subplot(321)

    plt.subplots_adjust(left=.1, bottom=.1, right=.9, top=.9, wspace=.3, hspace=.3)

    cm = pylab.get_cmap('gist_rainbow')

    plt.title('SCENE: ' + dd,size=18)
    plt.axis('off')
    # plt.savefig(saveloc+'/intensity.png')



    # flatness over ramp

    plt.subplot(311)

    for rr in range(len(ramp_folders)):
        for i in range(placesx.size):
            color = cm(1. * i / placesx.size)
            plt.scatter(timing_offset[rr,:],ranges[rr,:,i,0],marker=symbols[rr],c=color,s=10,lw=0,alpha = .7)
        plt.xlabel('Timing Offset (ns)')
        plt.ylabel('Range (m)')
        plt.title('Range over rw')
        plt.grid('on')
        # plt.savefig(saveloc+'/range_over_rangewindow.png')


    plt.subplot(312)

    plt.plot(rangesW,)
    plt.xlabel('Time [.1s]')
    plt.ylabel('Range (m)')
    plt.title('Range vs Time')

    plt.subplot(313)
    # plt.hist(pandas.rolling_std(rangesW, 30)[:], 50, normed=1, alpha=0.75)
    # plt.hist(pandas.rolling_std(rangesW, 30))
    # plt.xlabel('Time [.1s]')
    # plt.ylabel('Range Jitter (m)')
    boxy = 50
    pandas.Series(rangesW).plot.hist(alpha=1.0,bins=50)
    plt.title('Range Histogram (mean,std) = ('+str(np.mean(pandas.Series(rangesW)))+
              ','+str(np.std(pandas.Series(rangesW)))+')')
    plt.xlabel('Range [m]')
    plt.grid('on')

    if INTERNAL== 1:

        # range vs range noise
        plt.subplot(325)
        for rr in range(len(ramp_folders)):
            for i in range(placesx.size):
                color = cm(1. * i / placesx.size)
                plt.scatter(ranges[rr,:,i,0],ranges[rr,:,i,1],marker=symbols[rr],c=color,s=10,lw=0,alpha = .7)
            plt.xlabel('Range (m)')
            plt.ylabel('Range Noise (m)')
            plt.title('Range Noise in the Image')
            plt.grid('on')
            # plt.savefig(saveloc+'/range_v_range(std).png')

        # noise over ramp
        plt.subplot(324)
        for rr in range(len(ramp_folders)):
            for i in range(placesx.size):
                color = cm(1. * i / placesx.size)
                plt.scatter(timing_offset[rr,:],ranges[rr,:,i,1],marker=symbols[rr],c=color,s=10,lw=0,alpha = .7)
            plt.xlabel('Timing Offset (ns)')
            plt.ylabel('Range Noise (m)')
            plt.title('Range Noise (std) over rw')
            plt.grid('on')
            # plt.savefig(saveloc+'/rangeSTD_over_rangewindow.png')

        plt.subplot(323)

        for rr in range(len(ramp_folders)):

            for i in range(placesx.size):
                color = cm(1. * i / placesx.size)
                plt.scatter(timing_offset[rr, :], intens[rr, :, i, 0], marker=symbols[rr], c=color, s=10, lw=0, alpha=.7)
            plt.xlabel('Timing Offset (ns)')
            plt.ylabel('Intensity (DN)')
            plt.title('Intensity at 1 m over rw')
            plt.grid('on')


        # range Error over ramp
        plt.subplot(326)
        for rr in range(len(ramp_folders)):
            for i in range(placesx.size):
                color = cm(1. * i / placesx.size)
                plt.scatter(timing_offset[rr,:],ranges[rr,:,i,0]-range_image,marker=symbols[rr],c=color,s=10,lw=0,alpha = .7)
            plt.xlabel('Timing Offset (ns)')
            plt.ylabel('Range Error (m)')
            plt.title('Range Error (from vision plane fit)')
            plt.grid('on')
            # plt.savefig(saveloc+'/rangeSTD_over_rangewindow.png')

    plt.savefig(saveloc + '/' + dd + 'range_window_figs.png')

    # range

    # intensity -> scaled to 1m using the wall range measurement

    # noise over ramp

        # plt.savefig(saveloc+'/rangeSTD_over_rangewindow.png')


    for rr in range(len(wall_folders)):
        rangeJIT[countFOLD,rr] = np.around(np.std(rangesW[:]) * 100.0,4)
        print rangesW[:]
        # intenJIT[countFOLD, rr] = np.around(np.std(intensW[rr, :, 0, 0]) / np.max(intensW[rr, :, 0, 0]),4)
        countFOLD+=1
# compare to a checkerboard to see

# checkerboard - 100s

# range error

# flatness

# jitter


# plt.xlabel('')
# plt.ylabel('')
# plt.title('')



# plot fiducials

import pandas as pd

from pandas.tools.plotting import scatter_matrix


doc = SimpleDocTemplate(saveloc+'/summary.pdf', pagesize=letter,
                        rightMargin=60, leftMargin=60,
                        topMargin=60, bottomMargin=60)
Story = []

formatted_time = str(time.time())
title = 'Alpha Prototype Data Quality Report (BOT)'
company = 'TetraVue'

styles = getSampleStyleSheet()
styleNormal = styles['Normal']
styleHeading = styles['Heading1']
styleHeading.alignment = 1



ptext = '<font name=HELVETICA size=20>%s</font>' % title
Story.append(Paragraph(ptext, styles["Heading1"]))

ptext = '<font name=HELVETICA size=10>%s</font>' % company
Story.append(Paragraph(ptext, styles["Heading1"]))

# ptext = '<font name=HELVETICA size=10>%s</font>' % tim
# Story.append(Paragraph(ptext, styles["Heading1"]))

ptext = '<font name=HELVETICA size=10>%s</font>' %  ('SN: '+data_loc0)
Story.append(Paragraph(ptext, styles["Normal"]))

ptext = '<font name=HELVETICA size=10>%s</font>' % ('Data location: '+data_loc0)
Story.append(Paragraph(ptext, styles["Normal"]))


Story.append(Spacer(1, 30))

if timeData ==1:
    ptext = '<font name=HELVETICA size=15>%s</font>' % ('Warm Up Data')
    Story.append(Paragraph(ptext, styles["Normal"]))
    Story.append(Spacer(1, 10))
    ptext = '<font name=HELVETICA size=10>%s</font>' % ('Location: '+data_loc0+'/3mWarmUp')
    Story.append(Paragraph(ptext, styles["Normal"]))
    Story.append(Spacer(1, 20))


if 1==1:
    ptext = '<font name=HELVETICA size=15>%s</font>' % ('Lens Calibration Summary')
    Story.append(Paragraph(ptext, styles["Normal"]))
    Story.append(Spacer(1, 10))
    summary = open(lens_cal+'/summary.txt').read()

    FOV = summary[summary.find('fov')+4:summary.find('fov')+14]
    princip = summary[summary.find('principal')+18:summary.find('principal')+44]
    ptext = '<font name=HELVETICA size=10>%s</font>' % ('Measured FOV deg (x,y): ' +FOV)
    Story.append(Paragraph(ptext, styles["Normal"]))
    ptext = '<font name=HELVETICA size=10>%s</font>' % ('Measured Principal Point (pixels): '+princip)
    Story.append(Paragraph(ptext, styles["Normal"]))
    Story.append(Spacer(1, 20))

    ptext = '<font name=HELVETICA size=15>%s</font>' % ('Performance Checks')
    Story.append(Paragraph(ptext, styles["Normal"]))
    Story.append(Spacer(1, 10))
    ptext = '<font name=HELVETICA size=10>%s</font>' % ('1. Software Trigger: |  |')
    Story.append(Paragraph(ptext, styles["Normal"]))
    ptext = '<font name=HELVETICA size=10>%s</font>' % ('2. Hardware Trigger: |  |')
    Story.append(Paragraph(ptext, styles["Normal"]))
    ptext = '<font name=HELVETICA size=10>%s</font>' % ('3. Max Frame Rate: |  |')
    Story.append(Paragraph(ptext, styles["Normal"]))
    ptext = '<font name=HELVETICA size=10>%s</font>' % ('4. Eye Safety: |  |')
    Story.append(Paragraph(ptext, styles["Normal"]))

    Story.append(Spacer(1, 20))

countFOLD = 0
for dd in data_locs:
    ptext = '<font name=HELVETICA size=15>%s</font>' % ('Range Measurements: ' + dd)
    Story.append(Paragraph(ptext, styles["Normal"]))
    Story.append(Spacer(1, 10))

    ptext = '<font name=HELVETICA size=10>%s</font>' % ('Range Jitter (cm) for each range window')
    Story.append(Paragraph(ptext, styles["Normal"]))
    for rr in range(len(wall_folders)):
        ptext = '<font name=HELVETICA size=10>%s</font>' % (wall_folders[rr] + ':  ' + str(rangeJIT[countFOLD,rr]))
        Story.append(Paragraph(ptext, styles["Normal"]))

    # ptext = '<font name=HELVETICA size=10>%s</font>' % ('Intensity Jitter (Fraction of Max) for each range window')
    # Story.append(Paragraph(ptext, styles["Normal"]))
    # for rr in range(len(wall_folders)):
    #     ptext = '<font name=HELVETICA size=10>%s</font>' % (wall_folders[rr] + ':  ' + str(intenJIT[countFOLD,rr]))
    #     Story.append(Paragraph(ptext, styles["Normal"]))
    countFOLD+=1
    Story.append(Spacer(1, 20))

Story.append(PageBreak())
for dd in data_locs:
    im = muchhate((saveloc + '/'+dd+'range_window_figs.png'), 6 * inch, 9 * inch)
    Story.append(im)


if timeData ==1:
    Story.append(PageBreak())

    im = muchhate((saveloc + '/warmp_up_drift.png'), 6 * inch, 9 * inch)
    Story.append(im)
    Story.append(PageBreak())

    im = muchhate((saveloc + '/fiducials.png'), 6 * inch, 9 * inch)
    Story.append(im)




Story.append(Spacer(1, 10))
doc.build(Story)


plt.show()


