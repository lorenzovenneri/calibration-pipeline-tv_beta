#!/usr/bin/python

import os,sys
import matplotlib.pyplot as plt
import numpy as np

from datafile import *

import argparse
import fnmatch

import cv2

if __name__=='__main__':

  parser = argparse.ArgumentParser(description='Show raw data.')
  parser.add_argument('-f',metavar='files',dest='files',required=True,
                      nargs='+',help='Files to process')
  parser.add_argument('-o',metavar='path',dest='output',default=None,
                      help='Output path')
  #parser.add_argument('-c',metavar='path',dest='output',default=None,
  #                    help='Output path')

  args = parser.parse_args()

  df=DataFile()

  cv2.namedWindow('Intensity')
  
  for f in args.files:

    df.loadIntensity(f)
    fbase=os.path.basename(f)

    img=df.gammaTo8bit()
    for i in xrange(100):
      img[i,i+100]=255
    #end

    cv2.imshow('Intensity',img)

    if args.output:
      cv2.imwrite(os.path.join(args.output,fbase+'.marked.png'),img)
      cv2.waitKey()
    #end

  #end
