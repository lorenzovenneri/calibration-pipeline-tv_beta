import numpy as np

def hnorm(x):
  y=x.copy()
  y[:-1,:]/=np.tile(y[-1,:],(y.shape[0]-1,1))
  return y

def homog(x):
  return np.vstack((x,np.ones((1,x.shape[1]))))
