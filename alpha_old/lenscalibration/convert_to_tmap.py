#!/usr/bin/python

# # This converts data files that we receive from TetraVue into a format that we
# # can use with TMAP to produce surface models.

# # This is only the first step in running TMAP software.  After converting the
# # input files with this script, you need to run Visual Odometry on the
# # image+disparity pairs in order to get poses for the point clouds.  Then you
# # need to run ICP on the point clouds.

# # VO will produce a set of poses as .t3d files.  Copy these to the output
# # directory that is created here.

# # If VO provides reasonable starting poses, you are happy.  If it doesn't then
# # you need to manually fix the initial poses in ICPTestApp (and save those
# # corrections) before running ICP.

# # Note: the arguments you give to -f for the input files are all the *_int.dat
# # files.  Not all the files from the input directory.

import os,sys
import matplotlib.pyplot as plt
import numpy as np
from datafile import *
import argparse
import cv2
from range_to_ply import *
from range_to_disparity import *

if __name__=='__main__':

  # Set up command line options
  parser = argparse.ArgumentParser(description='Convert tetravue data to format TMAP code can read.')
  parser.add_argument('-f',metavar='files',dest='files',required=True,
                      nargs='+',help='Files to process')
  parser.add_argument('-c',metavar='PATH',dest='camera',required=True,
                      help='Path to camera configuration')
  parser.add_argument('-o',metavar='PATH',dest='output',required=True,
                      help='Output path to store ply files')
  parser.add_argument('-t',metavar='threshold',dest='threshold',default=0.05,
                      type=float,help='Filtering threshold')
  parser.add_argument('-maxr',metavar='range',dest='max_range',default=10.0,
                      type=float,help='Max. range')
  parser.add_argument('-minr',metavar='range',dest='min_range',default=0.0,
                      type=float,help='Min. range')
  parser.add_argument('-w',metavar='msec',dest='delay',default=0,
                      type=int,help='Wait time between frames')
  parser.add_argument('-a',dest='use_ascii',action='store_true',default=False,
                      help='Use ASCII output for ply point clouds [def=Binary]')
  parser.add_argument('-nocolor',dest='use_color',action='store_false',
                      default=True,help='Disable color output [def=use color]')
  parser.add_argument('-maxi',metavar='intensity',dest='max_intensity',
                      default=float("inf"), type=float, 
                      help='Max. raw intensity (inf means autoscale)')
  parser.add_argument('-mini',metavar='intensity',dest='min_intensity',
                      default=-float("inf"), type=float, 
                      help='Min. raw intensity (-inf means autoscale)')
  parser.add_argument('-median',dest='do_median_filter',action='store_true',default=False,
                      help='Use a 5x5 median filter -- before bilateral filtering. [def=no median filter')
  parser.add_argument('-bw',metavar='bilateral width',dest='bilateral_width',
                      default=8,type=int,
                      help='Window width for bilateral filtering.')
  parser.add_argument('-brs',metavar='bilateral range sigma',
                      dest='bilateral_rsigma', default=10,type=float,
                      help='Range sigma for bilateral filtering.')
  parser.add_argument('-bps',metavar='bilateral pixel sigma',
                      dest='bilateral_psigma', default=10,type=float,
                      help='Pixel sigma for bilateral filtering.')
  parser.add_argument('-nobilateral',dest='do_bilateral',action='store_false',
                      default=True,
                      help='Disable bilateral range filtering. [def=do filtering]')
  parser.add_argument('-cropl',metavar='cropping',dest='cropl', default=0, 
                      type=int, help='Number of rows to crop from the left.')
  parser.add_argument('-cropr',metavar='cropping',dest='cropr', default=0, 
                      type=int, help='Number of rows to crop from the right.')
  parser.add_argument('-cropt',metavar='cropping',dest='cropt', default=0, 
                      type=int, help='Number of rows to crop from the top.')
  parser.add_argument('-cropb',metavar='cropping',dest='cropb', default=0, 
                      type=int, help='Number of rows to crop from the bottom.')

  args = parser.parse_args()

  if not np.isfinite(args.min_intensity) or not np.isfinite(args.max_intensity):
    print "Warning image min and max values will be autoscaled.  Output images"
    print "will likely not have consistent brightness."
  #end

  # Create objects for processing
  df=DataFile()
  cam=PinHoleCamera.load(args.camera)
  rfilt=RangeFilter(args.threshold)

  # Make output directory if necessary
  if not os.path.isdir(args.output):
    print 'Creating output directory',args.output
    os.makedirs(args.output)
  #end

  # Process input files
  for f in args.files:
      
    print "Processing frame: ", f

    # Create filenames we need by parsing input filename.
    # We assume the frame number is the substring up to the first '_'.
    # We assume the timestamp is the number between the second and third '_'
    # characters.
    fbase=os.path.basename(f)
    parts = fbase.split('_')
    framenum = parts[0].zfill(6)
    timestamp = parts[2]
    imagebase = 'image'+framenum;
    ifn = os.path.join(args.output,imagebase+'.pgm')
    afn = os.path.join(args.output,imagebase+'.aux')
    dfn = os.path.join(args.output,imagebase+'-disparity.pgm')
    pfn = os.path.join(args.output,'pointCloud'+framenum+'.ply')
    rfn = os.path.join(args.output,'range'+framenum+'.dat');
    mfn = os.path.join(args.output,'mask'+framenum+'.pgm');

    # Load this frame of data, based on filename f, but multiple
    # files' worth of data will be loaded.
    df.loadSmart(f)

    df.filterRange()
    mask = df.getMask()

    # Camera images.  TBD: Rectify images?
    im = df.scaleTo8bitMinMax(args.min_intensity, args.max_intensity)
    cv2.imwrite(ifn, im)

    # Ranges are used for disparity and creating point clouds, so 
    # we process them up front.
    range = df.getRange()

    if args.do_median_filter:
      smooth = cv2.medianBlur(range,5)
    else:
      smooth = range
    #end

    if args.do_bilateral:
      smooth = rfilt.bilateralFilterSmoother(smooth, args.bilateral_width, 
                                             args.bilateral_rsigma,
                                             args.bilateral_psigma,
                                             args.min_range, args.max_range)
      mask,g = rfilt.simpleEdgeFilter(smooth, mask)
      mask = rfilt.updateMask(mask, smooth, minimum=args.min_range, 
                              maximum=args.max_range)
    else:
      # This doesn't to any filtering, except eliminating invalid 
      # values (nan, inf) and eliminating outliers by truncating the ranges.
      mask=rfilt.updateMask(mask, smooth, minimum=args.min_range, 
                            maximum = args.max_range)
    #end

    # Because of sensor characteristics, sometimes the pixels at the edges
    # of the range image are particularly noisy and biased, so the user 
    # has the option to crop the image.
    if args.cropl > 0: mask[:,:args.cropl] = 0 
    if args.cropr > 0: mask[:,-args.cropr:] = 0 
    if args.cropt > 0: mask[:args.cropt,:] = 0 
    if args.cropb > 0: mask[-args.cropb:,:] = 0 
    
    # Disparity images
    # For creating disparity images, we've sometimes found it best not to mask
    # as much.  Masking tends to eliminate data at exactly the places where 
    # we find image features.  We've also sometimes found it better not to 
    # smooth the ranges, but less often.
    focalLength = cam.K[0,0]
    dmask = df.getMask()
    dvals = rangeToDisparity2(smooth, focalLength)
    #dvals = rangeToDisparity2(range, focalLength)
    dvals[~dmask] = 0.0
    # For TMAP, disparity images are 16-bit images containing
    # (short)(disparity * 16)
    dvals = dvals * 16.0
    cv2.imwrite(dfn,dvals.astype('uint16'));
      

    # Camera .aux files
    
    #ofn = os.path.join(args.output, fbase+'.aux')
    auxf = open(afn, 'w')
    auxf.write('Auxiliary file for Stereo Images\n')
    auxf.write('Version: 0.5\n')
    auxf.write('Timestamp: %s\n' % timestamp)
    auxf.write('Exposure: 0.0080000\n'); # Bogus unused.
    auxf.write('Gain: 1.6000000\n'); # Bogus unused.
    auxf.close()

    # Range image and mask
    # These are used to efficiently compute the normals for the point cloud
    # using a c++ module.  We could integrate that code into the DataFile
    # class's c++ code.
    # We write a file like a PGM16 but as PGM32 where we write floats
    # instead of int.  There is no PGM32, of course, but if there were, 
    # this is what it would look like.
    f = open(rfn,'w')
    f.write('P8\n') # I made that up.  Netpbm goes P1-P7.
    f.write('%s %s\n' % (np.shape(smooth)[1], np.shape(smooth)[0]))
    f.write('4294967295\n')
    smooth.reshape(-1,1).tofile(f)
    f.close()
    cv2.imwrite(mfn,mask.astype('uint8'));
    

    # Point Cloud
    pts = mapToRays(cam, smooth)
    gim = im.reshape((-1,1))
    cols = np.hstack((gim, gim, gim))        
    m = mask.ravel()
    if args.use_color:
      plySavePoints(pfn,pts[m,:].astype('float32'),color=cols[m,:],
                    ascii=args.use_ascii)
    else:
      plySavePoints(pfn,pts[m,:].astype('float32'),ascii=args.use_ascii)
    #end



  #end
