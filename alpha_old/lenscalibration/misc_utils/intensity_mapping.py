import os,sys
import numpy as np


def gammaCorrect(img,g=0.75):
  return np.power(img/np.maximum(1.0,img.max()),g)

def gammaCorrectTo8bit(img,g=0.75):
  return (gammaCorrect(img,g)*256.0).astype('uint8')
