#!/usr/bin/python

import os,sys
import matplotlib.pyplot as plt
import numpy as np

from datafile import *

import argparse

import cv2

if __name__=='__main__':

  parser = argparse.ArgumentParser(description='Show raw data.')
  parser.add_argument('-f',metavar='files',dest='files',required=True,
                      nargs='+',help='Files to process')
  parser.add_argument('-w',metavar='msec',dest='delay',default=0,
                      type=int,help='Wait time between frames')
  

  args = parser.parse_args()

  df=DataFile()

  cv2.namedWindow('Intensity')
  
  for f in args.files:
      
    df.loadIntensity(f)

    fbase=os.path.basename(f)

    if False:
      plt.clf()
      plt.subplot(121)
      plt.imshow(df.getIntensity(),cmap='gray')
      #plt.title(fbase)
      plt.axis('equal')
      plt.axis('off')

      plt.subplot(122)
      plt.imshow(df.getRange(),vmin=2.0,vmax=8.0)
      #plt.title('Range')
      plt.axis('equal')
      plt.colorbar(shrink=0.4)
      plt.axis('off')
      plt.locator_params(tight=True, nbins=4)
    
      plt.ion()
      plt.draw()
    else:
      #cv2.imshow('Intensity',df.gammaTo8bit())
      cv2.imshow('Intensity',df.histogramScaleTo8bit())

      if cv2.waitKey(args.delay) & 0xFF ==27:
        print 'Exiting...'
        sys.exit(1)
      #end
      
    #end

  #end
