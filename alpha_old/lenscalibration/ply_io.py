import os,sys
import numpy as np


# simple and only for points for now
def plyWriteHeader(f,comment=None,ascii=True,nrvertex=0,vertex_t='float',vertex_color_t=None,
                   nrface=0,face_t='int'):

  f.write('ply\n')
  if ascii:
    f.write('format ascii 1.0\n')
  else:
    f.write('format binary_little_endian 1.0\n')
  #end

  if comment!=None:
    for l in comment.splitlines():
      f.write('comment '+l+'\n')
    #end
  #end
  
  f.write('element vertex %i\n'%nrvertex)
  f.write('property %s x\n'%vertex_t)
  f.write('property %s y\n'%vertex_t)
  f.write('property %s z\n'%vertex_t)

  if vertex_color_t!=None:
    f.write('property %s red\n'%vertex_color_t)
    f.write('property %s green\n'%vertex_color_t)
    f.write('property %s blue\n'%vertex_color_t)
  #end

  if nrface>0:
    f.write('element face %i\n'%nrface)
    f.write('property list uchar %s vertex_index\n'%face_t)
  #end
  f.write('end_header\n')

def plyType(a):
  if a==None:
    return None
  #end
  s=str(a.dtype)
  if s in ['float32']:
    return 'float'
  elif s in ['float64']:
    return 'double'
  elif s in ['uchar','uint8']:
    return 'uchar'
  elif s in ['uint','uint32','uint16']:
    return 'uint'
  elif s in ['char','int8']:
    return 'char'
  elif s in ['int','int32','int16']:
    return 'int'
  #end
  return None

def plySavePoints(fn,pts,color=None,comment=None,ascii=True):
  f=open(fn,'w')

  ctype=plyType(color)
  vtype=plyType(pts)
  
  plyWriteHeader(f,comment=comment,ascii=ascii,nrvertex=pts.shape[0],
                 vertex_t=vtype,vertex_color_t=ctype)

  # now write the points
  if ascii:
    if color==None:
      np.savetxt(f,pts)
    else:
      for i in xrange(pts.shape[0]):
        np.savetxt(f,pts[i,:],newline=' ',delimiter=' ')
        np.savetxt(f,color[i,:],fmt='%i',newline=' ',delimiter=' ')
        f.write('\n')
      #end
    #end
  else:
    if color==None:
      pts.tofile(f)
    else:
      for i in xrange(pts.shape[0]):
        pts[i,:].tofile(f)
        color[i,:].tofile(f)
      #end
    #end
  #end
  f.write('\n')
  f.close()
    


if __name__=='__main__':

  print 'testing PLY points output'

  N=1000*1000
  pts=np.random.rand(N,3).astype('float32')
  cols=(np.random.rand(N,3)*256.0).astype('uint8')
  plySavePoints('test_data/test.ply',pts,color=cols,ascii=False)
  
