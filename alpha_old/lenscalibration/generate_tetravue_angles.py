#!/usr/bin/python

import os,sys
import cv2
from camera_model import *

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D as ax3d

from misc_utils.mutils import *

  
class GenerateAngleMaps:
  def __init__(self,cam_model,opath,verbose=False):
    self.verbose=verbose
    self.cam=cam_model
    self.opath=opath

    # now build up an image of pixel locations and project to the wall
    vv,uu=np.mgrid[0:cam.h,0:cam.w]

    uv=np.vstack((uu.ravel(),vv.ravel()))
    uv=uv.T.reshape((1,uu.size,2)).astype(np.float32)

    # undistort the vectors -- should now be 3D rays in the camera frame
    xx=self.cam.undistortPoints(uv).squeeze().T

    print 'xx.shape',xx.shape

    x=xx[0,:]
    y=xx[1,:]
    z=np.ones((1,xx.shape[1]))

    # generate the angle images
    sz=(cam.h,cam.w)
    xz_angle = np.arctan(x/z).reshape(sz)

    n=np.sqrt(x**2+z**2)
    elev_angle=np.arctan(y/n).reshape(sz)

    # bodo's angles
    a_angle=np.arctan(x/z).reshape(sz)
    b_angle=np.arctan(y/z).reshape(sz)

    # save output
    testMakeDir(opath)

    print 'Saving to',opath

    np.savetxt(os.path.join(opath,'xz_angle_image.dat.gz'),xz_angle,delimiter=" ")
    np.savetxt(os.path.join(opath,'elev_angle_image.dat.gz'),elev_angle,delimiter=" ")

    np.savetxt(os.path.join(opath,'a_angle_image.dat.gz'),a_angle,delimiter=" ")
    np.savetxt(os.path.join(opath,'b_angle_image.dat.gz'),b_angle,delimiter=" ")

    self.showMap(np.rad2deg(xz_angle),'XZ Angle (deg)','xz_angle_image_plot.png')
    self.showMap(np.rad2deg(elev_angle),'Elev Angle (deg)','elev_angle_image_plot.png')
    self.showMap(np.rad2deg(a_angle),'A Angle (deg)','a_angle_image_plot.png')
    self.showMap(np.rad2deg(b_angle),'B Angle (deg)','b_angle_image_plot.png')

  def saveAndShow(self,fn):
    plt.savefig(os.path.join(self.opath,fn))

    if self.verbose:
      mng = plt.get_current_fig_manager()
      mng.resize(*mng.window.maxsize())      
      plt.show()
    #end

  def showMap(self,xim,title,fn):
    plt.clf()
    plt.imshow(xim,cmap='hsv')
    plt.tight_layout()
    plt.colorbar(shrink=0.6)
    plt.title(title)
    self.saveAndShow(fn)

if __name__=='__main__':
  import argparse
  from datafile import *

  p = argparse.ArgumentParser(description='Load raw file and display.')
  p.add_argument('-c',metavar='PATH',dest='campath',required=True,
                 help='Camera path')
  p.add_argument('-v',dest='verbose',default=False,action='store_true',
                 help='Verbose')

  args = p.parse_args()

  opath=os.path.join(args.campath,'angle_maps')

  cam=PinHoleCamera.load(args.campath)
  GenerateAngleMaps(cam,opath,verbose=args.verbose)


  


