#!/usr/bin/python

import os,sys
import numpy as np
import cv2
from scipy import ndimage

#from misc_utils import *
from datafile import *

def averageAndStddev(range, w):
  # integral pads with 0's on top and left.
  i,s = cv2.integral2(range)
  i = i[1:,1:]
  s = s[1:,1:]

  size = np.shape(range)
  paddedsize = size[0]+w,size[1]+w

  pi = np.zeros(paddedsize)
  pi[:-w,:-w] = i
  pww = pi[w:,w:]
  p0w = pi[:-w,w:] 
  pw0 = pi[w:,:-w] 
  p00 = pi[:-w,:-w]
  ave = pww - p0w - pw0 + p00
  # Pad rows have bogus values
  ave[-w:,:] = 0.0
  ave[:,-w:] = 0.0
  ave /= (w * w)

  ps = np.zeros(paddedsize)
  ps[:-w,:-w] = s
  pww = ps[w:,w:]
  p0w = ps[:-w,w:] 
  pw0 = ps[w:,:-w] 
  p00 = ps[:-w,:-w]
  std = pww - p0w - pw0 + p00
  # Pad rows have bogus values
  std[-w:,:] = 0.0
  std[:,-w:] = 0.0
  std /= (w * w)
  std -= (ave**2)
  # numerical precision...
  std[std<0.0] = 0.0
  std = np.sqrt(std)

  return ave,std,i,s

# Integral, top, bottom, left, right
def regionSumFromIntegral(i,t,b,l,r):
  itl = i[t,l]
  itr = i[t,r]
  ibl = i[b,l]
  ibr = i[b,r]
  return (ibr - ibl - itr - itl)


if __name__=='__main__':
  import argparse

  parser = argparse.ArgumentParser(description='Show 3D points.')
  parser.add_argument('-f',metavar='files',dest='files',required=True,
                      nargs='+',help='Files to process')

  args = parser.parse_args()

  df=DataFile()
  
  for f in args.files:
    
    #print "File: ", f

    df.loadSmart(f)
    fbase=os.path.basename(f)

    df.filterRange()
    mask=df.getMask()

    range = df.getRange()

    isnan = np.isnan(range);
    isinf = np.isinf(range);
    bigenough = range>0.0
    smallenough = range<10.0
    good = np.logical_and(np.logical_not(isnan), np.logical_not(isinf))
    if False: 
        good = np.logical_and(good, bigenough)
        good = np.logical_and(good, smallenough)
    #end
    
    bad = np.logical_not(good)
    range[bad] = 0.0

    # We want the images to be based on the truncated range or else 
    # outliers screw up the dynamic range
    bad = np.logical_or(bad, range<0.0)
    bad = np.logical_or(bad, range>10.0)
    trange = range.copy()
    trange[bad] = 0.0
    ave,stdev,i,s = averageAndStddev(trange,5)

    #print "Overall ave, stddev = %s, %s" % (range.mean(), range.std())

    # Show average and stddev of large central region
    rows = np.shape(range)[0]
    cols = np.shape(range)[1]
    midr = (rows/2)
    midc = (cols/2)
    width = 50
    top = midr - width
    bottom = midr + width
    left = midc - width
    right = midc + width

    region = range[top:bottom, left:right]
    rave = region.mean()
    rstd = region.std()

    #print "Region ave, stddev = %s, %s" % (rave, rstd)
    print rstd

    rim = (range * 255.0/10.0).astype('uint8')
    rim[top:bottom,left] = 255;
    rim[top:bottom,right] = 255;
    rim[top,left:right] = 255;
    rim[bottom,left:right] = 255;

    cv2.imshow('Range', rim)

    amax = np.max(ave)
    amin = np.min(ave)
    aim = ((ave-amin) * 255.0/(amax-amin)).astype('uint8')
    aim[top:bottom,left] = 255;
    aim[top:bottom,right] = 255;
    aim[top,left:right] = 255;
    aim[bottom,left:right] = 255;

    cv2.imshow('Ave', aim)

    smax = np.max(stdev)
    smin = np.min(stdev)
    sim = ((stdev-smin) * 255.0/(smax-smin)).astype('uint8')
    sim[top:bottom,left] = 255;
    sim[top:bottom,right] = 255;
    sim[top,left:right] = 255;
    sim[bottom,left:right] = 255;

    cv2.imshow('Stdev', sim)

    cv2.waitKey(0)
  #end
