#!/usr/bin/python

import cv2
import numpy as np
import argparse
import cv2
from datafile import *
import matplotlib.pyplot as plt
import matplotlib

from misc_utils.plot_utils import *
from misc_utils.mutils import *

from geom.plane import *
from camera_model import *
from range_filter import *

def getQuartileRange(im,qlo=0.1,qhi=0.9):
  lo=np.percentile(im[np.isfinite(im)].flat,qlo*100.0)
  hi=np.percentile(im[np.isfinite(im)].flat,qhi*100.0)
  return (lo,hi)

def quartileBound(im,qlo=0.1,qhi=0.9):
  a,b=getQuartileRange(im,qlo,qhi)
  delta=(b-a)/(qhi-qlo)
  minv=a-delta*qlo
  maxv=b+delta*(1.0-qhi)
  return np.maximum(np.minimum(im,maxv),minv)

def histogramBins(a,qlo=0.1,qhi=0.9,nbins=100):
  minv,maxv=getQuartileRange(a,qlo,qhi)
  delta=(maxv-minv)/(qhi-qlo)
  return np.linspace(minv-delta*qlo,maxv+delta*(1.0-qhi),nbins)
  

def plotHistogram(a,bins=None):
  if bins==None:
    [hist,bb]=np.histogram(a,density=True)
  else:
    [hist,bins]=np.histogram(a,bins=bins,density=True)
  #end
  #plt.plot(0.5*(bins[1:]+bins[:-1]),hist/np.sum(hist),'b-')
  plt.plot(0.5*(bins[1:]+bins[:-1]),hist,'b-')

def stdCentered(a,alpha=0.1):
  s=np.sort(a)
  k=np.ceil(a.shape[0]*alpha)
  return np.std(s[k:-k])

def meanCentered(a,alpha=0.1):
  s=np.sort(a)
  k=np.ceil(a.shape[0]*alpha)
  return np.average(s[k:-k])

def gaussian(x,mean,std):
  return np.exp(-0.5*((x-mean)/std)**2) / (std*np.sqrt(2.0*np.pi))

def plotSmoothGaussian(a,mean,std,col='-',n=100,**kwargs):
  x=np.linspace(a.min(),a.max(),n)
  y=gaussian(x,mean,std)
  plt.plot(x,y,col,*kwargs)

def binCenters(bins):
  return 0.5*(bins[1:]+bins[:-1])

# mouse callback function
class RangeDistribution:
  def __init__(self,cam_path=None,output=None,name='image',thresh=0.1,verbose=False):
    self.name=name
    self.dragging=False
    self.last=None
    self.df=DataFile()
    self.verbose=verbose

    self.output=output
    self.cam=PinHoleCamera.load(cam_path)

    self.fname=None

    self.ransac=RansacPlane3D(thresh=thresh)

    cv2.namedWindow(self.name)
    cv2.setMouseCallback(self.name,RangeDistribution.onMouse,param=self)

  def load(self,fn):
    self.fname=fn
    self.df.clear()
    self.df.loadSmart(fn)
    self.im = self.df.scaleTo8bit2()
    self.rng = self.df.getRange()
    self.mask = self.df.getMask()

    #rfilt=RangeFilter(0.05)
    #smooth = rfilt.bilateralFilterSmoother(self.df.getRange(), 20, 30, 15)
    #self.rng=smooth


    #self.rng[np.logical_not(self.mask)]=np.nan

    cv2.imshow(self.name,self.im)

  @staticmethod
  def onMouse(event,x,y,flags,param):
    param.doMouse(event,x,y,flags)

  def doMouse(self,event,x,y,flags):
    if event == cv2.EVENT_LBUTTONDOWN:
      if self.dragging:
        self.dragging=False
        self.estimateData(self.last,(x,y))
      else:
        self.last=(x,y)
        self.dragging=True
      #end
    elif event==cv2.EVENT_MOUSEMOVE and self.dragging:
      #self.im_copy=(256*np.minimum(8.0,self.rng.copy())/8.0).astype('int8') #self.im.copy()
      #self.im_copy[np.logical_not(self.mask)]=255
      #self.im_copy[self.mask]=0
      self.im_copy=self.im.copy()
      cv2.rectangle(self.im_copy,self.last,(x,y),(255,0,0))
      cv2.imshow(self.name,self.im_copy)
    #end

  def setBox(self,p1,p2):
    x1=min(p1[0],p2[0])
    y1=min(p1[1],p2[1])
    x2=max(p1[0],p2[0])
    y2=max(p1[1],p2[1])
    return (x1,y1,x2,y2)


  def plotPlanePoints(self,pts,plane,inliers):
    # make it a normalized normal vector plane    
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.plot(pts[0,:],pts[1,:],pts[2,:],'b.')
    plt.hold(True)
    
    xp=plane.projectToPlane(pts[:,inliers])
    ax.plot(xp[0,:],xp[1,:],xp[2,:],'go')

    plt.axis('equal')
    ax.set_xlabel('X')
    ax.set_ylabel('Y')
    ax.set_zlabel('Z')
    plt.show()

  def plotSoloHistogram(self,v,fbase=None,name='',title=''):
    fig=plt.figure()
    fig.clf()
    bins=histogramBins(v,nbins=32)
    bc=binCenters(bins)
    mu=meanCentered(v)
    ss=stdCentered(v)
    plotHistogram(v,bins=bins)
    plt.hold(True)
    #plt.plot(bc,gaussian(bc,0.0,s),'g-')
    plotSmoothGaussian(bc,mu,ss,'g:')
    plt.title('%s [std=%3.1fmm]'%(title,ss*1e3))
    plt.xlabel('Bounded Distance (m)')

    plot_maximize()
    if self.output!=None:
      fig.set_size_inches(18.,10.)
      #plt.subplots_adjust(wspace=0.5,hspace=0.5)
      plt.savefig(os.path.join(self.output,fbase+'_'+name+'_hist.png'),
                  transparent=True,dpi=100)
    #end
    if self.verbose:
      plt.show()
    #end
    

  def plotPlaneResults(self,rim,mask,ii,inliers,rexp,rerr,show_rim=False,fbase=None,name=None):
    fig=plt.figure()
    fig.clf()

    jj=ii[inliers]

    plt.subplot(2,2,1)
    plt.imshow(quartileBound(rim),cmap='hsv')
    plt.title('Range Patch')

    plt.subplot(2,2,2)
    bins=histogramBins(rerr.flat[ii],nbins=32)
    bc=binCenters(bins)
    mu=meanCentered(rerr.flat[ii])
    ss=stdCentered(rerr.flat[ii])
    plotHistogram(rerr.flat[ii],bins=bins)
    plt.hold(True)
    #plt.plot(bc,gaussian(bc,0.0,s),'g-')
    plotSmoothGaussian(bc,mu,ss,'g:')
    plt.title('Plane Distance [std=%3.1fmm]'%(ss*1e3))
    plt.xlabel('Bounded Distance to plane (m)')

    plt.subplot(2,2,3)
    plt.imshow(quartileBound(rexp),cmap='hsv')
    plt.title('Expected Range')

    plt.subplot(2,2,4)
    plt.imshow(quartileBound(rerr),cmap='hsv')
    plt.title('Range Error')

    #plt.subplot(2,3,5)
    #m=np.zeros(rim.size)
    #m[ii]=1
    #m[ii[inliers]]=2
    #plt.imshow(m.reshape(rim.shape),cmap='hsv')
    #plt.title('Inlier Mask')

    if False and show_rim:
      plt.subplot(2,2,1)
      bins=histogramBins(rim.flat[ii],nbins=32)
      bc=binCenters(bins)
      mu=meanCentered(rim.flat[ii])
      ss=stdCentered(rim.flat[ii])
      plotHistogram(rim.flat[ii],bins=bins)
      plt.hold(True)
      #plt.plot(bc,gaussian(bc,0.0,s),'g:')
      plotSmoothGaussian(bc,mu,ss,'g:')
      plt.title('Raw Range [std=%3.1fmm]'%(ss*1e3))
      plt.xlabel('Range (m)')
    #end

    plot_maximize()
    
    if self.output!=None:
      fig.set_size_inches(18.,10.)
      plt.subplots_adjust(wspace=0.5,hspace=0.5)
      plt.savefig(os.path.join(self.output,fbase+'_'+name+'_range_plot.png'),
                  transparent=True,dpi=100)
    #end
    if self.verbose:
      plt.show()
    #end
    
  
  def estimateOrthographicPlane(self,rim,mask,fbase):
    
    vv,uu=np.mgrid[0:rim.shape[0],0:rim.shape[1]].astype('float32')

    # get them to the same scale
    #uu/=uu.max()
    #vv/=vv.max()
    
    pts=np.vstack((uu.flat,vv.flat,rim.flat))

    ii=np.nonzero(mask.flat)[0]

    #plot3_quick(pts[:,ii])

    rv,plane,score,inliers=self.ransac.estimate(pts[:,ii])
    if not rv:
      print 'Cannot estimate orthographic plane'
      return 
    #end

    plane.normalizeVec()
    print 'plane',plane

    if False:
      self.plotPlanePoints(pts,plane,inliers)
    #end

    # build up predictive range
    N=pts.shape[1]
    rays=np.vstack((np.zeros((2,N)),np.ones((1,N))))

    print 'rays',rays.shape

    p0=pts.copy()
    p0[2,:]=0
    q=plane.rayIntersectDepth(rays,p0)

    rexp=q.reshape(rim.shape)
    rerr=rexp-rim

    self.plotPlaneResults(rim,mask,ii,inliers,rexp,rerr,fbase=fbase,name='ortho')
    self.plotSoloHistogram(rerr.flat[ii],fbase=fbase,name='ortho_error',title='Height Error')

    
  def estimateRangePlane(self,rim,mask,x1,y1,x2,y2,fbase):
    # get the rays and convert to ranges
    rays=self.cam.rays.reshape((self.rng.shape[0],self.rng.shape[1],3))

    pts=rays[y1:y2,x1:x2,:].copy()*np.dstack((rim,rim,rim))
    pts_flat=pts.reshape((-1,3)).T

    rrays=rays[y1:y2,x1:x2,:].reshape((-1,3))

    # for plotting... Need to check this is a copy not a view
    mm=np.logical_and(mask,np.isfinite(rim))
    ii=np.nonzero(mm.flat)[0]
    nn=np.sum(mm.flat)
 
    rv,plane,score,inliers=self.ransac.estimate(pts_flat[:,ii])
    if not rv:
      print 'Failed to estimate plane!'
      return 
    #end

    # normalize so distances are meaningful
    plane.normalizeVec()

    if False:
      self.plotPlanePoints(pts_flat[:,ii],plane,inliers)
    #end
   
    print 'Plane inlier ratio=%3.2f%%'%(np.sum(inliers)/float(inliers.size)*100.0)
    print 'Nr mask ratio=%3.2f%%'%(float(inliers.size)/float(rim.size)*100.0)
    print 'test=',np.sum(mask),mask.size
    #print "N=",N,mask.size

    # now compute residuals... normalize plane by vector to make
    # distances meaningful. This is our range distribution
    rerr=plane.distance(pts_flat).reshape(rim.shape)
    rexp=plane.originRayIntersectDepth(rrays.T).reshape(rim.shape)

    print "RMS err=",np.sqrt(np.sum(rerr.flat[ii]**2))
    print "RMS inlier err=",np.sqrt(np.sum(rerr.flat[ii[inliers]]**2))
    print "std range variation=",stdCentered(rim.flat[ii])
    print "std(rerr)=",stdCentered(rerr.flat[ii])
    print "std(rerr inlier)=",stdCentered(rerr.flat[ii[inliers]])
    

    self.plotPlaneResults(rim,mask,ii,inliers,rexp,rerr,show_rim=True,fbase=fbase,name='ray')
    self.plotSoloHistogram(rerr.flat[ii],fbase=fbase,name='plane_error',title='Plane Error')
    self.plotSoloHistogram(rim.flat[ii],fbase=fbase,name='range',title='Raw Range')


  def estimateData(self,p1,p2):
    x1,y1,x2,y2=self.setBox(p1,p2)

    rim=self.rng[y1:y2,x1:x2].copy()
    mask=self.mask[y1:y2,x1:x2].copy()

    # make bad points identifiable
    rim[np.logical_not(mask)]=np.inf

    fbase=os.path.basename(self.fname)

    self.estimateOrthographicPlane(rim,mask,fbase)
    self.estimateRangePlane(rim,mask,x1,y1,x2,y2,fbase)

    if self.output!=None:
      cv2.imwrite(os.path.join(self.output,fbase+'_label.png'),self.im_copy)
    #end
    if self.verbose:
      plt.show()
    #end



if __name__=='__main__':

  font = {'family' : 'normal',
          'weight' : 'bold',
          'size'   : 22}
  #matplotlib.rc('font', **font)
  
  matplotlib.rcParams.update({'font.size': 12})

  parser = argparse.ArgumentParser(description='Show raw data.')
  parser.add_argument('-f',metavar='files',dest='files',required=True,
                      nargs='+',help='Files to process')  
  parser.add_argument('-c',metavar='PATH',dest='campath',required=True,
                      help='Camera path')
  parser.add_argument('-t',metavar='FLOAT',dest='threshold',default=0.05,
                      help='Threshold for RANSAC')
  parser.add_argument('-o',metavar='PATH',dest='output',default=None,
                      help='Output path')
  parser.add_argument('-v',dest='verbose',default=False,action='store_true',
                      help='Verbose (show graphs)')

  args = parser.parse_args()

  if args.output!=None:
    testMakeDir(args.output)
  #end
    
  # load the camera calibration

  rd=RangeDistribution(cam_path=args.campath,thresh=args.threshold,output=args.output,
                       verbose=args.verbose)
  
  # shoul dmove this functionality into the class
  for f in args.files:
    print 'loading: ',f
    rd.load(f)
    
    if cv2.waitKey()&0xFF == 27:
      break
    #end
  #end

  cv2.destroyAllWindows()

