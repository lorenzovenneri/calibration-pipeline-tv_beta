#!/usr/bin/python

import os,sys
import numpy as np
import cv2

import argparse

from misc_utils import *
from datafile import *
  

if __name__=='__main__':

  parser = argparse.ArgumentParser(description='Show 3D points.')
  parser.add_argument('-f',metavar='files',dest='files',required=True,
                      nargs='+',help='Files to process')
  parser.add_argument('-o',metavar='PATH',dest='output',required=True,
                      help='Output path to store ply files')

  args = parser.parse_args()

  df=DataFile()
  
  if not os.path.isdir(args.output):
    print 'Creating output directory',args.output
    os.makedirs(args.output)
  #end

  for f in args.files:
    
    df.loadSmart(f)
    fbase=os.path.splitext(os.path.basename(f))[0]

    df.filterRange()

    # setup the colors
    gim=df.gammaTo8bit()

    ofn=os.path.join(args.output,fbase)

    print 'Saving to',ofn

    cv2.imwrite(ofn+'.png',gim)
    df.getRange().tofile(ofn+'.dat')
  #end
