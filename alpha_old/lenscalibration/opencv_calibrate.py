#!/usr/bin/env python
#
# TODO:
# * rewrite optimization to do our own camera models
# * provide uncertainty estimate of output
# * Enable scaling up of very large datasets with a data selection
#   stage and bootstrapped result
#
# CHANGES:
# * if a single argument is used with -f, it is used as a base for pattern '\*int.dat'

import copy

import os,sys
import glob
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d import art3d
import numpy as np
from multiprocessing import Pool
#from scipy.optimize import curve_fit
import cv2
#import cv
import time
import csv
import scipy.stats as stats
import math

from datafile import *
from camera_model import *

from misc_utils.mutils import *

#=== Utility Functions ======================================

# wraps the csv writer to write tuple to CSV files
def writeToCSV(fn,val):
  with open(fn,'wb') as csvfile:
    wr = csv.writer(csvfile,delimiter=',')
    wr.writerow(val)
  #end

def readCSV(fn,func=int):
  with open(fn,'rb') as csvfile:
    r = csv.reader(csvfile,delimiter=',')
    s=r.next()
    return tuple([func(x) for x in s])
  #end

def writeCSVList(fn,data):
  for i in xrange(len(data)):
    fname=fn+'.%06i.csv'%i
    np.savetxt(fname,data[i],delimiter=",")
  #end

def readCSVList(fprefix):
  i=0
  data=[]
  while True:
    try:
      fname=fprefix+'.%06i.csv'%i
      d=np.loadtxt(fname,delimiter=',',dtype='float32')
      if len(d.shape)==1:
        d=d.reshape(d.shape[0],1)
      #end
      data.append(d)
      i+=1
    except:
      return data
    #end
  #end
  return data


def im_MP(f, save_images, w, psize, chess_pts):
    df = DataFile()
    df.loadIntensity(f)
    img = df.scaleTo8bitMinMax()

    if w == 0:
        h, w = img.shape[:2]
    # end

    (rv, cnr) = cv2.findChessboardCorners(img, psize)
    if not rv:
        print "Failed to find chessboard corners."
        return False
    # end

    # subpix refinement
    term = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_COUNT, 30, 0.1)
    cv2.cornerSubPix(img, cnr, (5, 5), (-1, -1), term)

    print "+",

    sys.stdout.flush()
    return f, chess_pts, cnr.reshape(-1, 2),w,h


def estimateExtrinsics_out(x,X,K,g):
    # work out extrinsics for _all_ targets
    Ni=len(x)
    r=np.zeros((3,Ni))
    t=np.zeros((3,Ni))
    for i in xrange(Ni):
      (rv,rr,tt)=cv2.solvePnP(X[i],x[i],K,g)
      r[:,i]=rr.flat
      t[:,i]=tt.flat
    #end
    return (r,t)


def evalReprojection_out( imgpts, objpts, K, distort, rvecs, tvecs):
    Ni = len(imgpts)
    Np = imgpts[0].shape[0]

    err2 = np.zeros((Np, Ni))
    err = np.zeros((Ni, Np, 2))

    assert (Ni == rvecs.shape[1])
    assert (Ni == tvecs.shape[1])

    xpts = []
    xcov = []
    for i in xrange(Ni):
        x, J = cv2.projectPoints(objpts[i], rvecs[:, i], tvecs[:, i], K, distort)
        xx = x.reshape(x.shape[0], x.shape[2])
        xpts.append(xx)
        # print "J shape=",J.shape

        err2[:, i] = ((xx - imgpts[i]) ** 2).sum(1)
        err[i, :, :] = xx - imgpts[i]
    # end

    return (xpts, err2, err)

def caliter(X, x, w, h,ind):

    (rv, K, g, r, t) = cv2.calibrateCamera(X[ind], x[ind], (w, h))

    (r, t) = estimateExtrinsics_out(x, X, K, g)

    # eval error on the whole test set
    (xx, e2, ee) = evalReprojection_out(x, X, K, g, r, t)
    med = np.median(e2.max(axis=0))
    return med,e2,K,g


#=== Opencv Calibration =====================================

class OpenCVCalibration:
  def __init__(self,nr,nc,sq,debug=False,verbose=False):
    self.nrows=nr
    self.ncols=nc
    self.psize=(nc,nr)
    self.square=sq
    self.files=[]
    self.debug=debug
    
    self.imgpts=[]
    self.objpts=[]
    self.w=0
    self.h=0
    self.chess_pts=self.genObjPoints()
    self.verbose=verbose

  def genObjPoints(self):
    op = np.zeros( (np.prod(self.psize), 3), np.float32 )
    op[:,:2] = np.indices(self.psize).T.reshape(-1, 2)
    op *= self.square
    return op

  def addImage(self,fn,im,save_images=None):
    
    if self.w==0:
      self.h,self.w=im.shape[:2]
    #end  

    (rv,cnr)=cv2.findChessboardCorners(im,self.psize)
    if not rv:
      print "Failed to find chessboard corners."
      return False
    #end

    # subpix refinement
    term = ( cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_COUNT, 30, 0.1 )
    cv2.cornerSubPix(im, cnr, (5, 5), (-1, -1), term)

    #print "found corners...",cnr
    self.files.append(fn)
    self.objpts.append(self.chess_pts)
    self.imgpts.append(cnr.reshape(-1,2))

    if save_images!=None:
      cv2.namedWindow("corners")

      imc=cv2.cvtColor(im,cv2.COLOR_GRAY2BGR)
      cv2.drawChessboardCorners(imc,self.psize,cnr,rv)
      cv2.imshow("corners",imc)
      cv2.waitKey(1)

      fnb=os.path.basename(fn)

      fno=os.path.join(save_images,fnb+'.jpg')
      cv2.imwrite(fno,imc)
      
    #end
    return True



  def addImages(self,files,save_images=None,pat='int.dat'):

    if save_images!=None and not os.path.exists(save_images):
      print "Making path: ",save_images
      os.makedirs(save_images)
    #end

    if files[0].endswith('dat'):

      print "Loading datafiles"
      
      pool = Pool(processes=10)
      result_list = []

      for f in files: # TODO parallelize this
        result_list.append(pool.apply_async(im_MP, (f, save_images, self.w, self.psize, self.chess_pts)))

      for i, res in enumerate(result_list):
        rtn = res.get(timeout=None)
        self.files.append(rtn[0])
        self.objpts.append(rtn[1])
        self.imgpts.append(rtn[2])
        self.w = rtn[3]
        self.h = rtn[4]

    else:
      for f in files: # TODO parallelize this
        img=cv2.imread(f,0)
        if self.addImage(f,img,save_images):
          print "+",
        else:
          print ".",
        #end
        sys.stdout.flush()
      #end
    #end

    print "Processed: ",len(files),"files"
    print "Found corners in ",len(self.files),"of them"
    

  def nrImages(self):
    return len(self.files)
    

  def saveInput(self,path):
    if not os.path.exists(path):
      print "Making path: ",path
      os.makedirs(path)
    #end
    
    print "Saving detected corners to %s/(imgpts|objpts|imgsize).[0-9].csv"%path
    writeCSVList(os.path.join(path,'imgpts'),self.imgpts)
    writeCSVList(os.path.join(path,'objpts'),self.objpts)

    writeToCSV(os.path.join(path,'imsize.csv'),(self.w,self.h))
    return

  def loadInput(self,path):
    print "Loading detected corners from %s/"%path
    self.imgpts=readCSVList(os.path.join(path,'imgpts'))
    self.objpts=readCSVList(os.path.join(path,'objpts'))

    imsize=readSizeCSV(os.path.join(path,'imsize.csv'))
    self.w=imsize[0]
    self.h=imsize[1]
    return

  def calibrate(self,path):

    # make the output path
    testMakeDir(path)

    w=self.w
    h=self.h
    Ni=len(self.imgpts)
    Np=self.imgpts[0].shape[0]

    x=np.array(self.imgpts)
    X=np.array(self.objpts)
    
    # do a RANSAC-like small sample at first to select the points
    ebest=float('inf')

    pool = Pool(processes=10)
    result_list = []

    for j in xrange(20):
      ind = np.random.randint(Ni, size=15)
      result_list.append(pool.apply_async(caliter, (X,x,w,h,ind)))

    K_list = []
    e2_list = []
    g_list = []
    med_list = []
    for i, res in enumerate(result_list):
      print(i)
      rtn = res.get(timeout=None) #med,e2,K,g
      med_list.append(rtn[0])
      e2_list.append(rtn[1])
      K_list.append(rtn[2])
      g_list.append(rtn[3])

    for i in xrange(20):
        if med_list[i]<ebest:
            ebest=med_list[i]
            e2best=e2_list[i]
            Kbest=K_list[i]
            gbest=g_list[i]

    g= g_list[-1]
    K= K_list[-1]
    # From the best model, pick the inliers using MAD on per-frame error
    thresh=ebest*3.0

    #inl=(np.median(e2best,0)<thresh).nonzero()[0]
    inl=(e2best.max(axis=0)<thresh).nonzero()[0]

    #print "inliers=",inl.size
    #inl=inl[::15]

    # refit to the inliers
    print "Refitting to %i/%i inliers"%(inl.size,Ni)
    (r,t)=self.estimateExtrinsics(x[inl],X[inl],K,g)
    (rv,self.K,self.distort,r,t)=cv2.calibrateCamera(X[inl],x[inl],(w,h),
                                                     Kbest,gbest,r,t,
                                                     cv2.cv.CV_CALIB_USE_INTRINSIC_GUESS)

    # convert back to a set of matrices
    r=np.array(r).squeeze().T
    t=np.array(t).squeeze().T

    # estimate extrinsics for everything that is not an inlier
    (self.rvecs,self.tvecs)=self.estimateExtrinsics(x,X,self.K,self.distort)
    self.rvecs[:,inl]=r
    self.tvecs[:,inl]=t

    # save out yaml format
    print "Saving out result to ",path
    np.savetxt(os.path.join(path,'kmat.dat'),self.K,delimiter=" ")
    np.savetxt(os.path.join(path,'distort.dat'),self.distort,delimiter=" ")
    np.savetxt(os.path.join(path,'rvecs.dat'),self.rvecs,delimiter=" ")
    np.savetxt(os.path.join(path,'tvecs.dat'),self.tvecs,delimiter=" ")

    writeToCSV(os.path.join(path,'imsize.csv'),(self.w,self.h))
    writeToCSV(os.path.join(path,'psize.csv'),self.psize)
    np.savetxt(os.path.join(path,'square.dat'),np.array([self.square]))
    
    print "\nCalibration result"
    print "K=",self.K
    print "distort=",self.distort,"\n"

    print "Evaluating reprojection error"
    (xproj,err2,ee)=self.evalReprojection(x[inl],X[inl],
                                       self.K,self.distort,
                                       r,t)
    rms=np.sqrt(np.mean(err2.flat))
    med=np.median(err2.flat)
    
    s="Calibration performed\n"
    s+=' Square=%s, pattern size=%s\n'%(self.square,self.psize)

    #s+='CMD=%s'%sys.argv
    s+="RMS error=%7.6f pix\n"%rms
    s+= "Med error=%7.6f pix\n"%med
    s+= 'Used %s inliers from %s images\n'%(inl.size,Ni)
    s+= '  Threshold=%s from median %s\n'%(thresh,ebest)

    fovx=np.rad2deg(2.0*math.atan(float(self.w)/(self.K[0,0]*2.0)))
    fovy=np.rad2deg(2.0*math.atan(float(self.h)/(self.K[1,1]*2.0)))

    pp=self.K[0:2,2]
    f=self.K[0,0]

    s+= "fov=%4.1f %4.1f deg\n"%(fovx,fovy)
    s+= "flen=%4.3f pix\n"%f
    s+= "principal point=%s\n"%pp

    s+= "K=%s\n"%self.K
    s+= "distort=%s\n"%self.distort

    print 'Summary:'
    print s

    s+= "CMD=%s\n"%sys.argv

    with open(os.path.join(path,'summary.txt'),'w') as f:
      f.write(s)
    #end

    err_path=os.path.join(path,'error')
    testMakeDir(err_path)

    print "Plotting reprojected points..."
    self.plotReprojectedPoints(os.path.join(err_path,'reproj_error'),xproj,x[inl])
    self.plotReprojectionHeatMap(os.path.join(err_path,'reproj_error_heatmap.png'),x[inl],err2)

    self.histogramReprojectionErrorvsRadius(os.path.join(err_path,'error_vs_radius.png'),
                                            x[inl],err2,self.K)
    self.plotModelPoints(os.path.join(err_path,'3Dplots.png'),X[inl],r,t)
    
    #print "rectified imagery"
    #self.rectifyImages(os.path.join(path,'rectified'),False)
    return

  def estimateExtrinsics(self,x,X,K,g):
    # work out extrinsics for _all_ targets
    Ni=len(x)
    r=np.zeros((3,Ni))
    t=np.zeros((3,Ni))
    for i in xrange(Ni):
      (rv,rr,tt)=cv2.solvePnP(X[i],x[i],K,g)
      r[:,i]=rr.flat
      t[:,i]=tt.flat
    #end
    return (r,t)

  def loadCalResults(self,path):
    print "Saving out result to ",path
    self.K=np.loadtxt(os.path.join(path,'kmat.dat'),delimiter=" ")
    self.distort=np.loadtxt(os.path.join(path,'distort.dat'),delimiter=" ")
    self.rvecs=np.loadtxt(os.path.join(path,'rvecs.dat'),delimiter=" ")
    self.tvecs=np.loadtxt(os.path.join(path,'tvecs.dat'),delimiter=" ")

    # only needed for some things
    imsize=readCSV(os.path.join(path,'imsize.csv'),int)
    self.w=imsize[0]
    self.h=imsize[1]
    
    self.psize=readCSV(os.path.join(path,'psize.csv'),int)
    self.square=np.loadtxt(os.path.join(path,'square.dat'))
    

  def evalReprojection(self,imgpts,objpts,K,distort,rvecs,tvecs):

    Ni=len(imgpts)
    Np=imgpts[0].shape[0]

    err2=np.zeros((Np,Ni))
    err=np.zeros((Ni,Np,2))
    
    assert(Ni==rvecs.shape[1])
    assert(Ni==tvecs.shape[1])

    xpts=[]
    xcov=[]
    for i in xrange(Ni):
      x,J=cv2.projectPoints(objpts[i],rvecs[:,i],tvecs[:,i],K,distort)
      xx=x.reshape(x.shape[0],x.shape[2])
      xpts.append(xx)
      #print "J shape=",J.shape

      err2[:,i]=((xx-imgpts[i])**2).sum(1)
      err[i,:,:]=xx-imgpts[i]
    #end
   
    return (xpts,err2,err)
  

  def plotReprojectedPoints(self,fn,yproj,x):
    # show reprojection error
    Ni=len(yproj)

    fig=plt.figure()
    ax=fig.gca()
    for i in xrange(Ni):
      xx=x[i]
      yy=yproj[i]

      #print "x size=",x.shape
      #print "y size=",y.shape
      plt.plot(xx[:,0],xx[:,1],'r+',markersize=2)
      plt.hold(True)
      plt.plot(yy[:,0],yy[:,1],'go',markersize=2)

      for k in xrange(xx.shape[0]):
        plt.plot(np.hstack((xx[k,0],yy[k,0])),np.hstack((xx[k,1],yy[k,1])),'g-',linewidth=3)
      #end
    #end
    plt.title('Reprojected Points',fontsize=19)
    ax.set_xlim([0,self.w])
    ax.set_ylim([0,self.h])
    
    print "saving to ",fn
    plt.savefig(fn)

    if self.verbose:
      plt.ioff()
      plt.show() #block=False)
    #end
    return

  def histogramReprojectionErrorvsRadius(self,fn,x,err2,K):
    Ni=len(x)
    Np=x[0].shape[0]

    r=np.zeros((Np,Ni))
    e=np.sqrt(err2)

    cp=np.reshape(K[0:2,2],(1,2)) #np.array([K[0,2],K[1,2]])

    #print "K=",self.K
    #print "cp=",cp
    #print "Np=",Np
    #print "size imgpts=",self.imgpts[0].shape
      
    for i in xrange(Ni):
      xx=x[i]-np.tile(cp,(Np,1))
      r[:,i]=np.sqrt((xx**2).sum(1))
    #end

    plt.figure()
    plt.plot(r.ravel(),e.ravel(),'b.',markersize=3)
    plt.xlabel('Radius (pix)')
    plt.ylabel('Reproj. Error (pix)')
    plt.title('Error as a function of radius',fontsize=19)
    print "Saving to ",fn
    plt.savefig(fn)
    if self.verbose:
      plt.ioff()
      plt.show()#block=False)
    #end
    return
    

  def plotReprojectionHeatMap(self,fn,x,err2):

    # show reprojection error
    Ni=len(x)

    # build the big point, error map
    hm=np.zeros((self.h,self.w))

    # change to distance not square distance
    e=np.sqrt(err2)

    for i in xrange(Ni):
      xx=x[i].astype(np.int)
      ind=xx[:,0]+xx[:,1]*self.w
      hm.flat[ind]+=e[:,i]
    #end

    # could apply some smoothing to the image....
    s=20
    h=(s*3)*2+1
    hms=cv2.GaussianBlur(hm,(h,h),s)

    # plot image point error histogram
    plt.figure()
    plt.imshow(hms)
    plt.title('Reprojection Error Heatmap')
    plt.savefig(fn)
    print "saved to ",fn


  def plotReprojectionScatter(self,fn,err):
    # show reprojection error
    Ni=len(x)

    # plot scatter matrix 
    plt.figure()
    plt.scatter(err[:,0],err[:,1],'.',markersize=2)
    plt.title('Reprojection Error')
    plt.savefig(fn)
    print "saved to ",fn

  def plotModelPoints(self,fn,X,r,t):
    # transform to camera coordinate frame

    Np=X[0].shape[0]
    Ni=len(X)
    
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    cols=np.random.rand(3,Ni)

    for k in xrange(Ni):
      R,J=cv2.Rodrigues(r[:,k])

      #print "R.shape:",R.shape
      #print "obj.shape:",self.objpts[k].T.shape
      #print "tvecs:",np.tile(self.tvecs[k].reshape((3,1)),(1,Np)).shape
      #print "Np:",Np
      
      Y=np.dot(R,X[k].T)+np.tile(t[:,k].reshape((3,1)),(1,Np))
      #ax.plot(X[0,:],X[1,:],X[2,:],'+-',color=cols[:,k],markersize=5,linewidth=2)
      #ax.scatter(X[0,:],X[1,:],X[2,:],c='b')

      XX=Y[0,:].reshape((self.psize[1],self.psize[0]))
      YY=Y[1,:].reshape((self.psize[1],self.psize[0]))
      ZZ=Y[2,:].reshape((self.psize[1],self.psize[0]))

      c=cols[:,k]
      ax.plot_wireframe(XX,YY,ZZ,colors=c)
      ax.hold(True)
    #end

    ax.scatter([0],[0],[0],'.')

    self.drawFrustrum(ax)
    
    plt.xlabel('X')
    plt.ylabel('Y')
    ax.set_aspect('equal','box')
    plt.title('Camera Target Points')

    print "saving figure to",fn
    plt.savefig(fn)
    if self.verbose:
      plt.ioff()
      plt.show()#block=False)
    #end
    
  def drawFrustrum(self,ax,s=0.01):

    vtx=[np.array([[0,0,0],
                 [s,s,s],
                 [-s,s,s]]),
       np.array([[0,0,0],
                 [s,s,s],
                 [s,-s,s]]),
       np.array([[0,0,0],
                 [-s,s,s],
                 [-s,-s,s]]),
       np.array([[0,0,0],
                [s,-s,s],
                 [-s,-s,s]])]

    c=['r','g','b','y']
    for i in xrange(len(vtx)):
      tri = art3d.Poly3DCollection([vtx[i]])
      tri.set_color(c[i])
      tri.set_edgecolor('k')
      ax.add_collection3d(tri)
    #end

  def rectifyImages(self,path,show=False):
    cm=PinHoleCamera(self.K,self.distort,(w,h))

    cv2.namedWindow("Undistort")
    for f in self.files:
      fb=os.path.basename(f)
      of=os.path.join(path,fb+'_rect.png')
      im=cv2.imread(f)

      rim=cm.undistort(im)

      cv2.imshow("Undistort",np.hstack(im,rim))
      cv2.waitKey(10)

      if path!=None:
        print f,"->",of
        cv2.imwrite(of,rim)
      #end
    #end

  def genPointHeatMap(self,fn):

    # generate one big list of points
    nr=self.imgpts[0].shape[0]
    pts=np.zeros([2,len(self.imgpts)*nr])
    for i in xrange(len(self.imgpts)):
      pts[:,i*nr:(i+1)*nr]=self.imgpts[i].T
    #end

    print "Set up GMM..."
    kernel = stats.gaussian_kde(pts)

    # generate plot
    print "Generate plot..."
    xmax=self.w
    ymax=self.h
    
    x, y = np.mgrid[0:xmax:10,0:ymax:10]
    xy = np.vstack([x.ravel(), y.ravel()])
    z = np.reshape(kernel(xy).T, x.shape)

    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.imshow(np.rot90(z), cmap=plt.cm.gist_earth_r,
              extent=[0,xmax,0,ymax])
    ax.plot(pts[0,:],pts[1,:], 'r.', markersize=2)
    ax.set_xlim([0,xmax])
    ax.set_ylim([0,ymax])
    plt.title('Image Point Distribution')
    print "Saving to",fn
    plt.savefig(fn)
    if self.verbose:
      plt.ioff()
      plt.show()#block=False)
    #end


  

if __name__=='__main__':
  import argparse
  from datafile import *

  p = argparse.ArgumentParser(description='Load raw file and display.')
  p.add_argument('-f',dest='files',nargs='*',default=None,metavar='FILES',
                 help='Path to load')
  p.add_argument('-c',dest='base',default=None,required=True,metavar='PATH',
                 help='Base path for calibration results/working')
  
  p.add_argument('-i',dest='iname',default=False,action='store_true',
                 help='Load corners')
  p.add_argument('-wc',dest='write_input',default=False,action='store_true',
                 help='Write corner data out')
  
  #p.add_argument('-wo',dest='write_output',default=None,metavar='PATH',
  #               help='Write result out')
  
  p.add_argument('-nc',dest='calibrate',default=True,action='store_false',
                 help='Do not run calibration')
  
  p.add_argument('-ci',dest='load_calibrate',default=False,action='store_true',
                 help='Load prior calibration')
  
  p.add_argument('-nhm',dest='no_heatmap',default=False,action='store_true',
                 help='Do not generate heatmap')
  
  p.add_argument('-si',dest='save_images',default=False,action='store_true',
                 help='Generate debug output for calibration target')
  
  p.add_argument('-s',dest='square',required=True,type=float,
                 help='Square size [def=105.0]')
  p.add_argument('-p',dest='psize',required=True,nargs=2,type=int,
                 help='pattern size')
  #p.add_argument('-ne',dest='no_error',default=False,action='store_true',
  #               help='Do not generate error analysis output')
  p.add_argument('-v',dest='verbose',default=False,action='store_true',
                 help='Show output plots')

  args = p.parse_args()

  if args.files==None and args.iname==None:
    p.print_usage()
    sys.exit(1)
  #end

  
  cal=OpenCVCalibration(args.psize[0],args.psize[1],args.square,
                        verbose=args.verbose)

  print "calibrating with psize=",cal.psize," squarelen=",cal.square
  print 'Calibration directory:',args.base

  # make the output path
  testMakeDir(args.base)

  if args.files!=None:
    print "Processing image input"
    opath=None
    if args.save_images:
      opath=testMakeDir(os.path.join(args.base,'corner_images'))
    #end
    if len(args.files) == 1:
      files = glob.glob(args.files[0]) # + "\\*int.dat")
      cal.addImages(files, opath)
    else:
      cal.addImages(args.files,opath)

    if args.write_input:
      print 'Saving corner data'
      cal.saveInput(testMakeDir(os.path.join(args.base,'corners')))
    #end
  else:
    print 'Loading input corners...'
    cal.loadInput(os.path.join(args.base,'corners'))
  #end

  if not args.no_heatmap:
    print "Generating heatmap"
    cal.genPointHeatMap(os.path.join(args.base,'point_heatmap.png'))
  #end

  if args.calibrate:
    print "Now calibrating..."
    cal.calibrate(args.base)
  elif args.load_calibrate:
    print "Loading prior calibration"
    cal.loadCalResults(args.base)
  #end


  
