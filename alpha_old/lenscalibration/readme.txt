To install:
First, run "python-2.7.7.msi" install Python 2.7
Then, run each of the following to install libraries:
"matplotlib-1.3.1.win32-py2.7.exe"
"numpy-1.8.1-win32-superpack-python2.7.exe"
"pyparsing-2.0.2.win32-py2.7.exe"
"python-dateutil-2.2.win32-py2.7.exe"
"scipy-0.14.0-win32-superpack-python2.7.exe"
"six-1.7.2.win32-py2.7.exe"
Last, to install OpenCV, navigate to C:\\Python27\Lib\site-packages, and copy "cv2.pyd" to here.

To run:
In the command prompt, cd to the directory that contains the python scripts.

Type "python opencv_calibrate.py -f [input directory] -c [output directory] -s 105.1 -p 5 3 -wc"
The input and output paths can be relative to the current directory, for example "..\wave" would be the wave folder in the parent directory.
The input directory should contain files named by the convention "*_int.dat".

Type "python generate_tetravue_angles.py -c [output directory]"
Where [output directory] is the output directory from the opencv_calibrate step.

Type "python generate_flat_plane.py" -c [output directory] -f [input directory]"
Where [output directory] is the output directory from the opencv_calibrate step.
The input directory should contain files named by the convention "*_int.dat".