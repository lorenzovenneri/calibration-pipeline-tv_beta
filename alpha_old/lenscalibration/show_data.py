#!/usr/bin/python

import os,sys
import matplotlib.pyplot as plt
import numpy as np
from range_filter import *
from datafile import *

import argparse

import cv2

if __name__=='__main__':

  parser = argparse.ArgumentParser(description='Show raw data.')
  parser.add_argument('-f',metavar='files',dest='files',required=True,
                      nargs='+',help='Files to process')
  parser.add_argument('-maxr',metavar='range',dest='max_range',default=8.0,
                      type=float, help='Max. range (0 means autoscale)')
  parser.add_argument('-w',metavar='msec',dest='delay',default=0,
                      type=int,help='Wait time between frames')
  parser.add_argument('-maxi',metavar='intensity',dest='max_intensity',
                      default=float("inf"), type=float, 
                      help='Max. raw intensity (inf means autoscale)')
  parser.add_argument('-mini',metavar='intensity',dest='min_intensity',
                      default=-float("inf"), type=float, 
                      help='Min. raw intensity (-inf means autoscale)')
  parser.add_argument('-print-intensity',dest='print_intensity',
                      action='store_true',default=False,
                      help='Print the min and max intensity for each image.')

  args = parser.parse_args()

  df=DataFile()
  rfilt=RangeFilter(0.05)

  cv2.namedWindow('Intensity')
  cv2.namedWindow('Range')
  cv2.namedWindow('Klass')
  
  for f in args.files:
      
    #print "Processing frame: ", f

    df.loadSmart(f)

    fbase=os.path.basename(f)

    df.filterRange()

    if args.print_intensity:
      im = df.getIntensity()
      valid = np.logical_and(~np.isnan(im), np.isfinite(im))
      print "Min, max intensity: %s, %s" % (im[valid].min(), im[valid].max())
    #end

    
    rng = rfilt.bilateralFilterSmoother(df.getRange(), 20, 30, 15)

    if False:
      plt.clf()
      plt.subplot(121)
      plt.imshow(df.getIntensity(),cmap='gray')
      #plt.title(fbase)
      plt.axis('equal')
      plt.axis('off')

      plt.subplot(122)
      plt.imshow(rng,vmin=2.0,vmax=8.0)
      #plt.title('Range')
      plt.axis('equal')
      plt.colorbar(shrink=0.4)
      plt.axis('off')
      plt.locator_params(tight=True, nbins=4)
    
      plt.ion()
      plt.draw()
    else:

      im = df.scaleTo8bitMinMax(args.min_intensity, args.max_intensity)
      cv2.imshow('Intensity', im)

      range=df.getRange()
      if args.max_range<1e-6:
        max_range=range[np.isfinite(range)].max()
      else:
        max_range=args.max_range
      #end
      rim=(range*255.0/max_range).astype('uint8')
      rim[np.isnan(range)]=0
      rim[np.isinf(range)]=255
      cv2.imshow('Range',rim)

      cv2.imshow('Klass',df.getKlass()*(255/2))

      cv2.waitKey(args.delay)
    #end

  #end
