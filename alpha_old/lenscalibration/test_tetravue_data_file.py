import libtetravue as ttv


import os,sys
import matplotlib.pyplot as plt
import numpy as np

print 'testing load'
df=ttv.DataFile()
df.load('test_data/test')

print 'size (w=%s h=%s)'%(df.width,df.height)
print 'intensity type:',df.getIntensity().dtype
print 'range type:',df.getRange().dtype
print 'klass type:',df.getKlass().dtype

for i in xrange(3):
  #print i," ",df.getIntensity()[i,:4]
  print i," ",df.getRange()[i,:4]
#end

if False:
  b=df.getIntensity().copy()
  for i in xrange(300):
    b[i,i]=float(i)*1000.0
  #end
#end

#print "shape: ",df.getIntensity().T.shape
#print "stride: ",df.getIntensity().T.strides


range=df.getRange().copy()
range[df.getKlass()==ttv.KlassMode.invalid]=0
range[df.getKlass()==ttv.KlassMode.infront]=0 #np.inf

plt.clf()
plt.subplot(221)
plt.imshow(df.getIntensity(),origin='lower')
plt.title('Intensity')

plt.subplot(222)
plt.imshow(range,origin='lower')
plt.title('Range')

plt.subplot(223)
plt.imshow(df.getKlass(),origin='lower')
plt.title('Klass')

plt.ioff()
plt.show()

print 'done'
